��          D      l       �      �   f   �   $   �   N     1  k     �  f   �  $     N   0                          CentOS Groonga supports AlmaLinux. So you can continue using Groonga by switching from CentOS 7 to AlmaLinux. See also :doc:`/install/almalinux` . Support for Groonga has ended from version 14.0.5 because of CentOS 7’s EOL. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 CentOS Groonga supports AlmaLinux. So you can continue using Groonga by switching from CentOS 7 to AlmaLinux. See also :doc:`/install/almalinux` . Support for Groonga has ended from version 14.0.5 because of CentOS 7’s EOL. 