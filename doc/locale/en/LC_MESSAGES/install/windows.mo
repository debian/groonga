��          �      <      �     �  $   �  K   �  K   4  Z   �     �     �  n       v     �  1   �  E   �  Y   
  J   d  Y   �  J   	     T  1  X     �  $   �  K   �  K     Z   Y     �     �  n   �    O	     c
  1   k
  E   �
  Y   �
  J   =  Y   �  J   �     -               
                                     	                                                 Build from source Build from source is for developers. For 32-bit environment, download x86 zip archive from packages.groonga.org: For 64-bit environment, download x64 zip archive from packages.groonga.org: If we don't need Microsoft Visual C++ Runtime Library, we download from the following URL: See :doc:`/install/cmake`. Then extract it. This section describes how to install Groonga on Windows. You can install Groonga by extracting a zip package. We distribute both 32-bit and 64-bit packages but we strongly recommend a 64-bit package for server. You should use a 32-bit package just only for tests or development. You will encounter an out of memory error with a 32-bit package even if you just process medium size data. Windows You can build Groonga from the source with CMake. You can find :doc:`/reference/executables/groonga` in ``bin`` folder. https://packages.groonga.org/windows/groonga/groonga-latest-x64-vs2019-with-vcruntime.zip https://packages.groonga.org/windows/groonga/groonga-latest-x64-vs2019.zip https://packages.groonga.org/windows/groonga/groonga-latest-x86-vs2019-with-vcruntime.zip https://packages.groonga.org/windows/groonga/groonga-latest-x86-vs2019.zip zip Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Build from source Build from source is for developers. For 32-bit environment, download x86 zip archive from packages.groonga.org: For 64-bit environment, download x64 zip archive from packages.groonga.org: If we don't need Microsoft Visual C++ Runtime Library, we download from the following URL: See :doc:`/install/cmake`. Then extract it. This section describes how to install Groonga on Windows. You can install Groonga by extracting a zip package. We distribute both 32-bit and 64-bit packages but we strongly recommend a 64-bit package for server. You should use a 32-bit package just only for tests or development. You will encounter an out of memory error with a 32-bit package even if you just process medium size data. Windows You can build Groonga from the source with CMake. You can find :doc:`/reference/executables/groonga` in ``bin`` folder. https://packages.groonga.org/windows/groonga/groonga-latest-x64-vs2019-with-vcruntime.zip https://packages.groonga.org/windows/groonga/groonga-latest-x64-vs2019.zip https://packages.groonga.org/windows/groonga/groonga-latest-x86-vs2019-with-vcruntime.zip https://packages.groonga.org/windows/groonga/groonga-latest-x86-vs2019.zip zip 