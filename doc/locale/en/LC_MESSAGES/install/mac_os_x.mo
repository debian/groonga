��          t      �              $   #     H  p   Q  	   �     �     �  ,   �  �        �  1  �       $        <  p   E  	   �     �     �  ,   �  �        �        	                       
                             Build from source Build from source is for developers. Homebrew If you want to use `MeCab <https://taku910.github.io/mecab/>`_ as a tokenizer, specify ``--with-mecab`` option:: Install:: MacPorts See :doc:`/install/cmake` . Then install and configure MeCab dictionary. This section describes how to install Groonga on macOS. You can install Groonga by `MacPorts <http://www.macports.org/>`__ or `Homebrew <http://mxcl.github.com/homebrew/>`__. macOS Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Build from source Build from source is for developers. Homebrew If you want to use `MeCab <https://taku910.github.io/mecab/>`_ as a tokenizer, specify ``--with-mecab`` option:: Install:: MacPorts See :doc:`/install/cmake` . Then install and configure MeCab dictionary. This section describes how to install Groonga on macOS. You can install Groonga by `MacPorts <http://www.macports.org/>`__ or `Homebrew <http://mxcl.github.com/homebrew/>`__. macOS 