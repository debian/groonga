��          <      \       p      q     y     |  1  �     �    �    �                   Install This section describes how to install Groonga on each environment. There are packages for major platforms. It's recommended that you use package instead of building Groonga by yourself. But don't warry. There is a document about building Groonga from source. We distribute both 32-bit and 64-bit packages but we strongly recommend a 64-bit package for server. You should use a 32-bit package just only for tests or development. You will encounter an out of memory error with a 32-bit package even if you just process medium size data. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Install This section describes how to install Groonga on each environment. There are packages for major platforms. It's recommended that you use package instead of building Groonga by yourself. But don't warry. There is a document about building Groonga from source. We distribute both 32-bit and 64-bit packages but we strongly recommend a 64-bit package for server. You should use a 32-bit package just only for tests or development. You will encounter an out of memory error with a 32-bit package even if you just process medium size data. 