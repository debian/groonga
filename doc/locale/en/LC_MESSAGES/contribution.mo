��    	      d      �       �   9   �        
   +     6  G   S  8   �  #   �  2  �  1  +  9   ]     �  
   �     �  G   �  8     #   P  2  t           	                                   As a developer: Bug report, development and documentation As a spokesman: As a user: How to contribute to groonga If you are interested in groonga, please read this document and try it. Please introduce groonga to your friends and colleagues. This section describes the details. We welcome your contributions to the groonga project. There are many ways to contribute, such as using groonga, introduction to others, etc. For example, if you find a bug when using groonga, you are welcome to report the bug. Coding and documentation are also welcome for groonga and its related projects. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 As a developer: Bug report, development and documentation As a spokesman: As a user: How to contribute to groonga If you are interested in groonga, please read this document and try it. Please introduce groonga to your friends and colleagues. This section describes the details. We welcome your contributions to the groonga project. There are many ways to contribute, such as using groonga, introduction to others, etc. For example, if you find a bug when using groonga, you are welcome to report the bug. Coding and documentation are also welcome for groonga and its related projects. 