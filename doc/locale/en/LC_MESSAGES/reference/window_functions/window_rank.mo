��          �      <      �     �     �  ;   �  5     1   S  .   �     �  
   �     �     �     �     �     �      5        U     [  1  k     �     �  ;   �  5   	  1   ?  .   q     �  
   �     �     �     �     �     �    �  5   
     A
     G
                  
                                   	                                                :doc:`window_record_number` Execution example:: Here are a schema definition and sample data to show usage. Here is an example that computes ranks for each game: Here is an example that specifies only sort keys: Here is an example that uses descending order: Nothing. Parameters Return value See also Summary Syntax The rank as ``UInt32`` value. This window function computes the rank of each record with gaps. This is similar to :doc:`window_record_number`. :doc:`window_record_number` computes the number of each record. The number is always incremented but the rank isn't incremented when multiple records that are the same order. The next rank after multiple records that are the same order has gap. If values of sort keys are ``100, 100, 200`` then the ranks of them are ``1, 1, 3``. The rank of the last record is ``3`` not ``2`` because there are two ``1`` rank records. This window function doesn't require any parameters:: Usage ``window_rank`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`window_record_number` Execution example:: Here are a schema definition and sample data to show usage. Here is an example that computes ranks for each game: Here is an example that specifies only sort keys: Here is an example that uses descending order: Nothing. Parameters Return value See also Summary Syntax The rank as ``UInt32`` value. This window function computes the rank of each record with gaps. This is similar to :doc:`window_record_number`. :doc:`window_record_number` computes the number of each record. The number is always incremented but the rank isn't incremented when multiple records that are the same order. The next rank after multiple records that are the same order has gap. If values of sort keys are ``100, 100, 200`` then the ranks of them are ``1, 1, 3``. The rank of the last record is ``3`` not ``2`` because there are two ``1`` rank records. This window function doesn't require any parameters:: Usage ``window_rank`` 