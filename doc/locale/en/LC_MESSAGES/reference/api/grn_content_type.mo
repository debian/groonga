��          �      ,      �  h   �     �       �   #     �  %   �  m   �  ]   [  	   �     �     �     �     �               +  1  @  h   r     �     �  �        �  %   �  m   �  ]   D  	   �     �     �     �     �     �                                  	                
                                      :c:type:`grn_content_type` shows input type and output type. Currently, it is used only for output type. Here are available values: It means JSON format. It means MessagePack format. You need MessagePack library on building Groonga. If you don't have MessagePack library, you can't use this type. It means XML format. It means tab separated values format. It means that outputting nothing or using the original format. :doc:`/reference/commands/dump` uses the type. Normally, you don't need to use this type. It is used internally in :c:func:`grn_ctx_send()`. Reference Summary `GRN_CONTENT_JSON` `GRN_CONTENT_MSGPACK` `GRN_CONTENT_NONE` `GRN_CONTENT_TSV` `GRN_CONTENT_XML` ``grn_content_type`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :c:type:`grn_content_type` shows input type and output type. Currently, it is used only for output type. Here are available values: It means JSON format. It means MessagePack format. You need MessagePack library on building Groonga. If you don't have MessagePack library, you can't use this type. It means XML format. It means tab separated values format. It means that outputting nothing or using the original format. :doc:`/reference/commands/dump` uses the type. Normally, you don't need to use this type. It is used internally in :c:func:`grn_ctx_send()`. Reference Summary `GRN_CONTENT_JSON` `GRN_CONTENT_MSGPACK` `GRN_CONTENT_NONE` `GRN_CONTENT_TSV` `GRN_CONTENT_XML` ``grn_content_type`` 