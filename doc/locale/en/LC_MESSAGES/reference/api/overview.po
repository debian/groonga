# -*- po -*-
# English translations for Groonga package.
# Copyright (C) 2009-2025 Groonga Project
# This file is distributed under the same license as the Groonga package.
# Automatically generated, 2025.
#
msgid ""
msgstr ""
"Project-Id-Version: Groonga 15.0.2\n"
"Report-Msgid-Bugs-To: \n"
"PO-Revision-Date: 2025-02-21 09:46+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "Overview"
msgstr "Overview"

msgid "Summary"
msgstr "Summary"

msgid "You can use Groonga as a library. You need to use the following APIs to initialize and finalize Groonga."
msgstr "You can use Groonga as a library. You need to use the following APIs to initialize and finalize Groonga."

msgid ":c:func:`grn_init()` initializes Groonga. In contrast, :c:func:`grn_fin()` finalizes Groonga."
msgstr ":c:func:`grn_init()` initializes Groonga. In contrast, :c:func:`grn_fin()` finalizes Groonga."

msgid "You must call :c:func:`grn_init()` only once before you use APIs which are provided by Groonga. You must call :c:func:`grn_fin()` only once after you finish to use APIs which are provided by Groonga."
msgstr "You must call :c:func:`grn_init()` only once before you use APIs which are provided by Groonga. You must call :c:func:`grn_fin()` only once after you finish to use APIs which are provided by Groonga."

msgid "Example"
msgstr "Example"

msgid "Here is an example that uses Groonga as a full-text search library."
msgstr "Here is an example that uses Groonga as a full-text search library."

msgid "Reference"
msgstr "Reference"

msgid "``grn_init()`` initializes resources that are used by Groonga. You must call it just once before you call other Groonga APIs."
msgstr "``grn_init()`` initializes resources that are used by Groonga. You must call it just once before you call other Groonga APIs."

msgid "Returns"
msgstr "Returns"

msgid "``GRN_SUCCESS`` on success, not ``GRN_SUCCESS`` on error."
msgstr "``GRN_SUCCESS`` on success, not ``GRN_SUCCESS`` on error."

msgid "``grn_fin()`` releases resources that are used by Groonga. You can't call other Groonga APIs after you call ``grn_fin()``."
msgstr "``grn_fin()`` releases resources that are used by Groonga. You can't call other Groonga APIs after you call ``grn_fin()``."
