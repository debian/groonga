��    
      l      �       �   '   �   �        �  D   �  Z        z     �  -   �  W   �  1    '   C  �   k       D   ,  Z   q     �     �  -   �  W                                	         
       :doc:`/reference/commands/table_create` A table can have zero or more token filters. You can attach token filters to a table by :ref:`table-create-token-filters` option in :doc:`/reference/commands/table_create`. Execution example:: Groonga has token filter module that some processes tokenized token. Here is an example ``table_create`` that uses ``TokenFilterStopWord`` token filter module: See also Summary Token filter module can be added as a plugin. You can customize tokenized token by registering your token filters plugins to Groonga. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/commands/table_create` A table can have zero or more token filters. You can attach token filters to a table by :ref:`table-create-token-filters` option in :doc:`/reference/commands/table_create`. Execution example:: Groonga has token filter module that some processes tokenized token. Here is an example ``table_create`` that uses ``TokenFilterStopWord`` token filter module: See also Summary Token filter module can be added as a plugin. You can customize tokenized token by registering your token filters plugins to Groonga. 