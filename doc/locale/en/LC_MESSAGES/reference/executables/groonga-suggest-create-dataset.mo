��          �   %   �      @  U   A  
   �     �     �     �  �   �     �     �     �     �  	   �     �  M   �                  
   #     .  H   M  �   �     ?     L     Q     ^  1  o  U   �  
   �          
       �        �     �     �     �  	          M        `     g     u  
   �     �  H   �  �   �     �	     �	     �	     �	                                                                                       
                                   	    :doc:`/reference/suggest` :doc:`groonga-suggest-httpd` :doc:`groonga-suggest-learner` DESCTIPION EXAMPLE EXIT STATUS FILES Here is the list of such tables. If you specify 'query' as dataset name, following '_DATASET' suffix are replaced. Thus, 'item_query', 'pair_query', 'sequence_query', 'event_query' tables are generated. NAME None. OPTIONS SEE ALSO SYNOPSTIS TODO This command generates some tables and columns for :doc:`/reference/suggest`. bigram configuration event_DATASET event_type groonga-suggest-create-dataset groonga-suggest-create-dataset - Defines schema for a suggestion dataset groonga-suggest-create-dataset creates a dataset for :doc:`/reference/suggest`. A database has many datasets. This command just defines schema for a suggestion dataset. item_DATASET kana pair_DATASET sequence_DATASET Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/suggest` :doc:`groonga-suggest-httpd` :doc:`groonga-suggest-learner` DESCTIPION EXAMPLE EXIT STATUS FILES Here is the list of such tables. If you specify 'query' as dataset name, following '_DATASET' suffix are replaced. Thus, 'item_query', 'pair_query', 'sequence_query', 'event_query' tables are generated. NAME None. OPTIONS SEE ALSO SYNOPSTIS TODO This command generates some tables and columns for :doc:`/reference/suggest`. bigram configuration event_DATASET event_type groonga-suggest-create-dataset groonga-suggest-create-dataset - Defines schema for a suggestion dataset groonga-suggest-create-dataset creates a dataset for :doc:`/reference/suggest`. A database has many datasets. This command just defines schema for a suggestion dataset. item_DATASET kana pair_DATASET sequence_DATASET 