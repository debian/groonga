��          �   %   �      P  3   Q  "   �  �   �  >   [  (   �     �     �     �  
              /     >  )   P     z     �  %   �     �  -   �  +   �  d        v     �  �   �     A    Y  1  i  3   �  "   �  �   �  >   �	  (   �	     
     *
     2
  
   M
      X
     y
     �
  )   �
     �
     �
  %   �
     �
  -     +   /  d   [     �     �  �   �     �    �                                            	                   
                                                              Execute groonga-suggest-learner with ``-l`` option. Execute groonga-suggest-learner.:: Here is the list of table which learned data is stored. If you specify ``query`` as dataset name, following ``_DATASET`` suffix are replaced. Thus, ``event_query`` table is used. Here is the sample to load log data under ``logs`` directory:: Learning data from groonga-suggest-httpd Learning data from log files Options Outputs log to ``<path>``. Parameters Reads logs from ``<directory>``. Related tables Runs as a daemon. Specifies the path to a groonga database. Summary Synopsis There is only one required parameter. Usage Uses ``<endpoint>`` as the receiver endpoint. Uses ``<endpoint>`` as the sender endpoint. Uses ``<level>`` for log level. ``<level>`` must be between 1 and 9. Larger level outputs more logs. ``database_path`` event_DATASET groonga-suggest-leaner supports the two way of learning data. One is learning data from groonga-suggest-httpd, the other is learning data from already existing log files. groonga-suggest-learner groonga-suggest-learner is a program to learn suggest result from data which derived from groonga-suggest-httpd. Usually, it is used with groonga-suggest-httpd, but It is allowed to launch standalone. In such a case, groonga-suggest-learner loads data from log directory. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execute groonga-suggest-learner with ``-l`` option. Execute groonga-suggest-learner.:: Here is the list of table which learned data is stored. If you specify ``query`` as dataset name, following ``_DATASET`` suffix are replaced. Thus, ``event_query`` table is used. Here is the sample to load log data under ``logs`` directory:: Learning data from groonga-suggest-httpd Learning data from log files Options Outputs log to ``<path>``. Parameters Reads logs from ``<directory>``. Related tables Runs as a daemon. Specifies the path to a groonga database. Summary Synopsis There is only one required parameter. Usage Uses ``<endpoint>`` as the receiver endpoint. Uses ``<endpoint>`` as the sender endpoint. Uses ``<level>`` for log level. ``<level>`` must be between 1 and 9. Larger level outputs more logs. ``database_path`` event_DATASET groonga-suggest-leaner supports the two way of learning data. One is learning data from groonga-suggest-httpd, the other is learning data from already existing log files. groonga-suggest-learner groonga-suggest-learner is a program to learn suggest result from data which derived from groonga-suggest-httpd. Usually, it is used with groonga-suggest-httpd, but It is allowed to launch standalone. In such a case, groonga-suggest-learner loads data from log directory. 