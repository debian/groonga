��          �      L      �     �  F   �     $  7  8  �   p  #     u   0  8   �     �     �     �       9        E  S   K     �  (   �  �   �  1  r     �  F   �       7    �   S	  #   �	  u   
  8   �
     �
     �
     �
     �
  9   �
     (  S   .     �  (   �  �   �                                                                	          
                          :doc:`../commands/tokenize` As you expected, the above query matches the ``Raspberry Pie`` record. Execution example:: For example, query contains a known keyword and a unknown keyword, only a known keyword is used in search query (unknown keyword will be ignored). In other words, you can search contents with only known keywords. Because of this characteristic, you need to maintain keyword table for a new keyword continuously. Here is an example of ``TokenTable``. For example, let's search ``Raspberry Pie`` from ``Pies`` table. The table which is used for keyword is ``Keywords``. Here is the sample schema and data: In above example, as the keyword ``Stargazy`` is not registered in ``Keywords`` table yet, it doesn't match anything. Next, search ``Stargazy Pie`` with ``--query Stargazy``. See also Specify the table:: Summary Syntax Then search ``Raspberry Pie`` with ``--query Raspberry``. Usage ``TABLE`` must be created with ``--default_tokenizer 'TokenTable("table", TABLE)``. ``TokenTable`` ``TokenTable`` has a required parameter. ``TokenTable`` is a tokenizer which treats only known keywords as a token. The known keywords must be registered as a key in the table for ``TokenTable``. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`../commands/tokenize` As you expected, the above query matches the ``Raspberry Pie`` record. Execution example:: For example, query contains a known keyword and a unknown keyword, only a known keyword is used in search query (unknown keyword will be ignored). In other words, you can search contents with only known keywords. Because of this characteristic, you need to maintain keyword table for a new keyword continuously. Here is an example of ``TokenTable``. For example, let's search ``Raspberry Pie`` from ``Pies`` table. The table which is used for keyword is ``Keywords``. Here is the sample schema and data: In above example, as the keyword ``Stargazy`` is not registered in ``Keywords`` table yet, it doesn't match anything. Next, search ``Stargazy Pie`` with ``--query Stargazy``. See also Specify the table:: Summary Syntax Then search ``Raspberry Pie`` with ``--query Raspberry``. Usage ``TABLE`` must be created with ``--default_tokenizer 'TokenTable("table", TABLE)``. ``TokenTable`` ``TokenTable`` has a required parameter. ``TokenTable`` is a tokenizer which treats only known keywords as a token. The known keywords must be registered as a key in the table for ``TokenTable``. 