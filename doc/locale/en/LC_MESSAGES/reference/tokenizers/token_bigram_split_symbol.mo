��    	      d      �       �      �      �      �           
  -   %  m   S  G   �  1  	     ;     O     W     ^     d  -     m   �  G        	                                         Execution example:: Summary Syntax Usage ``TokenBigramSplitSymbol`` ``TokenBigramSplitSymbol`` hasn't parameter:: ``TokenBigramSplitSymbol`` is similar to :ref:`token-bigram`. The difference between them is symbol handling. ``TokenBigramSplitSymbol`` tokenizes symbols by bigram tokenize method: Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Summary Syntax Usage ``TokenBigramSplitSymbol`` ``TokenBigramSplitSymbol`` hasn't parameter:: ``TokenBigramSplitSymbol`` is similar to :ref:`token-bigram`. The difference between them is symbol handling. ``TokenBigramSplitSymbol`` tokenizes symbols by bigram tokenize method: 