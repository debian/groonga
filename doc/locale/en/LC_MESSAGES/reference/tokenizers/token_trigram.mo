��    
      l      �       �      �   p     �   v     +     3     :     @  #   Q  _   u  1  �       p     �   �     A     I     P     V  #   g  _   �                        	       
              Execution example:: If ``TokenTrigram`` tokenize non-ASCII charactors, ``TokenTrigram`` uses 3 character per token as below example. If normalizer is used, ``TokenTrigram`` uses white-space-separate like tokenize method for ASCII characters. ``TokenTrigram`` uses trigram tokenize method for non-ASCII characters. Summary Syntax Usage ``TokenTrigram`` ``TokenTrigram`` hasn't parameter:: ``TokenTrigram`` is similar to :ref:`token-bigram`. The differences between them is token unit. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: If ``TokenTrigram`` tokenize non-ASCII charactors, ``TokenTrigram`` uses 3 character per token as below example. If normalizer is used, ``TokenTrigram`` uses white-space-separate like tokenize method for ASCII characters. ``TokenTrigram`` uses trigram tokenize method for non-ASCII characters. Summary Syntax Usage ``TokenTrigram`` ``TokenTrigram`` hasn't parameter:: ``TokenTrigram`` is similar to :ref:`token-bigram`. The differences between them is token unit. 