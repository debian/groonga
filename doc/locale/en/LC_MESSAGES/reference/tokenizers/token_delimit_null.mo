��    
      l      �       �      �   +        1     9     @     F  '   [  3   �  �   �  1  �     �  +   �                      '   1  3   Y  �   �                         
      	              Execution example:: Here is an example of ``TokenDelimitNull``: Summary Syntax Usage ``TokenDelimitNull`` ``TokenDelimitNull`` hasn't parameter:: ``TokenDelimitNull`` is also suitable for tag text. ``TokenDelimitNull`` is similar to :ref:`token-delimit`. The difference between them is separator character. :ref:`token-delimit` uses space character (``U+0020``) but ``TokenDelimitNull`` uses NUL character (``U+0000``). Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here is an example of ``TokenDelimitNull``: Summary Syntax Usage ``TokenDelimitNull`` ``TokenDelimitNull`` hasn't parameter:: ``TokenDelimitNull`` is also suitable for tag text. ``TokenDelimitNull`` is similar to :ref:`token-delimit`. The difference between them is separator character. :ref:`token-delimit` uses space character (``U+0020``) but ``TokenDelimitNull`` uses NUL character (``U+0000``). 