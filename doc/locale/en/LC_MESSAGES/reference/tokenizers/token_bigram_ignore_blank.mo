��          t      �              )   %  /   O          �     �  n   �       -     �   L  1       K  )   _  /   �     �     �     �  n   �     =  -   X  �   �                                            	         
       Execution example:: Here is a result by :ref:`token-bigram` : Here is a result by ``TokenBigramIgnoreBlank``: Summary Syntax Usage You can find difference of them by ``日 本 語 ! ! !`` text because it has symbols and non-ASCII characters. ``TokenBigramIgnoreBlank`` ``TokenBigramIgnoreBlank`` hasn't parameter:: ``TokenBigramIgnoreBlank`` is similar to :ref:`token-bigram`. The difference between them is blank handling. ``TokenBigramIgnoreBlank`` ignores white-spaces in continuous symbols and non-ASCII characters. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here is a result by :ref:`token-bigram` : Here is a result by ``TokenBigramIgnoreBlank``: Summary Syntax Usage You can find difference of them by ``日 本 語 ! ! !`` text because it has symbols and non-ASCII characters. ``TokenBigramIgnoreBlank`` ``TokenBigramIgnoreBlank`` hasn't parameter:: ``TokenBigramIgnoreBlank`` is similar to :ref:`token-bigram`. The difference between them is blank handling. ``TokenBigramIgnoreBlank`` ignores white-spaces in continuous symbols and non-ASCII characters. 