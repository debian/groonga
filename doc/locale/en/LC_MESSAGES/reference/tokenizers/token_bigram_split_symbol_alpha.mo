��    	      d      �       �      �      �      �           
  2   *     ]  Z   �  1  8     j     ~     �     �     �  2   �     �  Z   f                             	                 Execution example:: Summary Syntax Usage ``TokenBigramSplitSymbolAlpha`` ``TokenBigramSplitSymbolAlpha`` hasn't parameter:: ``TokenBigramSplitSymbolAlpha`` is similar to :ref:`token-bigram`. The difference between them is symbol and alphabet handling. ``TokenBigramSplitSymbolAlpha`` tokenizes symbols and alphabets by bigram tokenize method: Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Summary Syntax Usage ``TokenBigramSplitSymbolAlpha`` ``TokenBigramSplitSymbolAlpha`` hasn't parameter:: ``TokenBigramSplitSymbolAlpha`` is similar to :ref:`token-bigram`. The difference between them is symbol and alphabet handling. ``TokenBigramSplitSymbolAlpha`` tokenizes symbols and alphabets by bigram tokenize method: 