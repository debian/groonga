��          �            h     i     x  )   �  :   �     �     �     	       n     %   �  8   �  j   �  y   O  R   �  1       N     ]  )   q  :   �     �     �     �     �  n   �  %   j  8   �  j   �  y   4  R   �     	                                               
                            Blank handling Execution example:: Here is a result by :ref:`token-bigram` : Here is a result by ``TokenBigramIgnoreBlankSplitSymbol``: Summary Symbol handling Syntax Usage You can find difference of them by ``日 本 語 ! ! !`` text because it has symbols and non-ASCII characters. ``TokenBigramIgnoreBlankSplitSymbol`` ``TokenBigramIgnoreBlankSplitSymbol`` hasn't parameter:: ``TokenBigramIgnoreBlankSplitSymbol`` ignores white-spaces in continuous symbols and non-ASCII characters. ``TokenBigramIgnoreBlankSplitSymbol`` is similar to :ref:`token-bigram`. The differences between them are the followings: ``TokenBigramIgnoreBlankSplitSymbol`` tokenizes symbols by bigram tokenize method. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Blank handling Execution example:: Here is a result by :ref:`token-bigram` : Here is a result by ``TokenBigramIgnoreBlankSplitSymbol``: Summary Symbol handling Syntax Usage You can find difference of them by ``日 本 語 ! ! !`` text because it has symbols and non-ASCII characters. ``TokenBigramIgnoreBlankSplitSymbol`` ``TokenBigramIgnoreBlankSplitSymbol`` hasn't parameter:: ``TokenBigramIgnoreBlankSplitSymbol`` ignores white-spaces in continuous symbols and non-ASCII characters. ``TokenBigramIgnoreBlankSplitSymbol`` is similar to :ref:`token-bigram`. The differences between them are the followings: ``TokenBigramIgnoreBlankSplitSymbol`` tokenizes symbols by bigram tokenize method. 