��          <      \       p      q   m   y   ;   �   1  #     U  m   ]  ;   �                   Command Command is the most important processing unit in query API. You request a processing to groonga by a command. This section describes about command and built-in commands. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Command Command is the most important processing unit in query API. You request a processing to groonga by a command. This section describes about command and built-in commands. 