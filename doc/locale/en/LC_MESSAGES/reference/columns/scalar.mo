��    &      L  5   |      P      Q     r     �     �  B  �  [   �     E     Z     r  %   �  <   �  6   �          #     +  $   0  .   U     �  9   �     �     �     �  	   �  	   �  	   �  	                  $     2     ;     D  
   V  
   a  
   l  	   w     �  1  �      �     �     �     	  B  	  [   ]
     �
     �
     �
  %   �
  <     6   W     �     �     �  $   �  .   �     �  9   �     8     E     N  	   Z  	   d  	   n  	   x     �     �     �     �     �     �  
   �  
   �  
   �  	   �     �                                    
      #                                      	   $                          "                                  !                      %      &       :doc:`../commands/column_create` :doc:`vector` Generated scalar column How to create If you want to store zero or more values per record, you can use multiple scalar columns or one :doc:`vector`. If these values are related and use the same types such as tags (zero or more strings), :doc:`vector` is suitable. If these values aren't related such as title and location, multiple scalar columns are suitable. It can store one scalar value per record. Scalar value is one of the following type values: Normal scalar column Reference scalar column Scalar column Scalar column is a data store object. See :doc:`../commands/column_create` how to create a column. See :ref:`column-create-generated-column` for details. See also Summary TODO There are three scalar column types: This section describes how to use these types. Usage You can use a scalar column as a :ref:`generated-column`. ``BFloat16`` ``Bool`` ``Float32`` ``Float`` ``Int16`` ``Int32`` ``Int64`` ``Int8`` ``LongText`` ``ShortText`` ``Text`` ``Time`` ``TokyoGeoPoint`` ``UInt16`` ``UInt32`` ``UInt64`` ``UInt8`` ``WGS84GeoPoint`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`../commands/column_create` :doc:`vector` Generated scalar column How to create If you want to store zero or more values per record, you can use multiple scalar columns or one :doc:`vector`. If these values are related and use the same types such as tags (zero or more strings), :doc:`vector` is suitable. If these values aren't related such as title and location, multiple scalar columns are suitable. It can store one scalar value per record. Scalar value is one of the following type values: Normal scalar column Reference scalar column Scalar column Scalar column is a data store object. See :doc:`../commands/column_create` how to create a column. See :ref:`column-create-generated-column` for details. See also Summary TODO There are three scalar column types: This section describes how to use these types. Usage You can use a scalar column as a :ref:`generated-column`. ``BFloat16`` ``Bool`` ``Float32`` ``Float`` ``Int16`` ``Int32`` ``Int64`` ``Int8`` ``LongText`` ``ShortText`` ``Text`` ``Time`` ``TokyoGeoPoint`` ``UInt16`` ``UInt32`` ``UInt64`` ``UInt8`` ``WGS84GeoPoint`` 