��    &      L  5   |      P  
   Q     \  	   i  	   s  b   }  Q   �  b   2  �   �  �     Y   �  W     
   m  t   x  
   �  �   �  3   z  6   �  (   �               )  
   2  �   =  ?   �  �   4	     
     
     
     
     '
     =
     K
     T
     \
     j
     q
  
   w
  1  �
  
   �     �  	   �  	   �  b   �  Q   C  b   �  �   �  �   }  Y     W   x  
   �  t   �  
   P  �   [  3   �  6     (   H     q          �  
   �  �   �  ?   W  �   �     f     s     z     �     �     �     �     �     �     �     �  
   �                "                       #       $                                  	   
                                                                 !      %            &            "complete" "correction" "groonga" "suggest" An user inputs "MySQL" and groonga returns nothing because "MySQL" isn't in keyword column values. An user inputs "ab" and groonga returns nothing because no word starts with "ab". An user inputs "co" and groonga returns "complete" and "correction" because they starts with "co". An user inputs "gronga" and groonga returns "groonga" because "gronga" is in wrong word and corresponding correct word is "groonga". An user inputs "groonga" and groonga returns "groonga search engine" because "groonga" is in keyword column and related query column is "groonga search engine". An user inputs "roonga" and groonga returns nothing because "roonga" isn't in wrong word. An user inputs "sug" and groonga returns "suggest" because "suggest" starts with "sug". Completion Completion helps user input. If user inputs a partial word, Groonga can return complete words from registered words. Correction Correction also helps user input. If user inputs a wrong word, groonga can return correct words from registered correction pairs. For example, there are registered correction pairs: For example, there are registered related query pairs: For example, there are registered words: Google search Introduction Learning Suggestion Suggestion helps that user filters many found documents. If user inputs a query, groonga can return new queries that has more additional keywords from registered related query pairs. The suggest feature in Groonga provides the following features: The suggest feature requires registered data before using the feature. Those data can be registered from user inputs. Gronnga-suggest-httpd and groonga-suggest-learner commands are provided for the propose. correct word gronga gronnga groonga groonga search engine groonga speed grroonga keyword related query search speed wrong word Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 "complete" "correction" "groonga" "suggest" An user inputs "MySQL" and groonga returns nothing because "MySQL" isn't in keyword column values. An user inputs "ab" and groonga returns nothing because no word starts with "ab". An user inputs "co" and groonga returns "complete" and "correction" because they starts with "co". An user inputs "gronga" and groonga returns "groonga" because "gronga" is in wrong word and corresponding correct word is "groonga". An user inputs "groonga" and groonga returns "groonga search engine" because "groonga" is in keyword column and related query column is "groonga search engine". An user inputs "roonga" and groonga returns nothing because "roonga" isn't in wrong word. An user inputs "sug" and groonga returns "suggest" because "suggest" starts with "sug". Completion Completion helps user input. If user inputs a partial word, Groonga can return complete words from registered words. Correction Correction also helps user input. If user inputs a wrong word, groonga can return correct words from registered correction pairs. For example, there are registered correction pairs: For example, there are registered related query pairs: For example, there are registered words: Google search Introduction Learning Suggestion Suggestion helps that user filters many found documents. If user inputs a query, groonga can return new queries that has more additional keywords from registered related query pairs. The suggest feature in Groonga provides the following features: The suggest feature requires registered data before using the feature. Those data can be registered from user inputs. Gronnga-suggest-httpd and groonga-suggest-learner commands are provided for the propose. correct word gronga gronnga groonga groonga search engine groonga speed grroonga keyword related query search speed wrong word 