��          �   %   �      0     1     E  B   �  A     K   J  
   �     �     �  =   �  &         '     /  -   6  0   d  2   �     �  
   �     �     �  l   �     k  b   x  E   �  1  !     S     g  B   �  A   *  K   l  
   �     �     �  =   �  &   "	     I	     Q	  -   X	  0   �	  2   �	     �	  
   �	     �	     
  l    
     �
  b   �
  E   �
            
                                                                                 	                                  Execution example:: For backward compatibility, ``truncate`` command accepts ``table`` parameter. But it should not be used for newly written code. Here is a simple example of ``truncate`` command against a column. Here is a simple example of ``truncate`` command against a table. If command succeeded, it returns true, otherwise it returns false on error. Parameters Required parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. Specifies the name of table or column. Summary Syntax There is required parameter, ``target_name``. This command takes only one required parameter:: This section describes parameters of ``truncate``. Usage ``HEADER`` ``SUCCEEDED_OR_NOT`` ``target_name`` ``target_name`` parameter can be used since 4.0.9. You need to use ``table`` parameter for 4.0.8 or earlier. ``truncate`` ``truncate`` command deletes all records from specified table or all values from specified column. ``truncate`` command returns whether truncation is succeeded or not:: Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: For backward compatibility, ``truncate`` command accepts ``table`` parameter. But it should not be used for newly written code. Here is a simple example of ``truncate`` command against a column. Here is a simple example of ``truncate`` command against a table. If command succeeded, it returns true, otherwise it returns false on error. Parameters Required parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. Specifies the name of table or column. Summary Syntax There is required parameter, ``target_name``. This command takes only one required parameter:: This section describes parameters of ``truncate``. Usage ``HEADER`` ``SUCCEEDED_OR_NOT`` ``target_name`` ``target_name`` parameter can be used since 4.0.9. You need to use ``table`` parameter for 4.0.8 or earlier. ``truncate`` ``truncate`` command deletes all records from specified table or all values from specified column. ``truncate`` command returns whether truncation is succeeded or not:: 