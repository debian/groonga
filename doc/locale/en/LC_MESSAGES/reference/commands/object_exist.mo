��          �   %   �      `     a  e   u  }   �     Y  
   m     x     �  ;   �  (   �     �       O     M   ]  1   �  c   �     A  %   a  0   �  &   �     �  ;   �  N   !     p     y  i   �  Z   �  1  O     �  e   �  }   �     y	  
   �	     �	     �	  ;   �	  (   �	     
     &
  O   -
  M   }
  1   �
  c   �
     a  %   �  0   �  &   �     �  ;     N   A     �     �  i   �  Z                                        	   
                                                                                    Execution example:: If you want to check existence of a column, use ``TABLE_NAME.COLUMN_NAME`` format like the following: It's a light operation. It just checks existence of the name in the database. It doesn't load the specified object from disk. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. Specifies the object name to be checked. Summary Syntax The ``object_exist Users`` returns ``false`` before you create ``Users`` table. The ``object_exist Users`` returns ``true`` after you create ``Users`` table. The command returns ``false`` otherwise such as:: The command returns ``true`` as body if object with the specified name exists in database such as:: There is no optional parameter. There is only one required parameter. This command takes only one required parameter:: This section describes all parameters. Usage You can check whether the name is already used in database: ``Logs`` is table name and ``timestamp`` is column name in ``Logs.timestamp``. ``name`` ``object_exist`` ``object_exist`` doesn't check object type. The existing object may be table, column, function and so on. ``object_exist`` returns whether object with the specified name exists or not in database. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: If you want to check existence of a column, use ``TABLE_NAME.COLUMN_NAME`` format like the following: It's a light operation. It just checks existence of the name in the database. It doesn't load the specified object from disk. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. Specifies the object name to be checked. Summary Syntax The ``object_exist Users`` returns ``false`` before you create ``Users`` table. The ``object_exist Users`` returns ``true`` after you create ``Users`` table. The command returns ``false`` otherwise such as:: The command returns ``true`` as body if object with the specified name exists in database such as:: There is no optional parameter. There is only one required parameter. This command takes only one required parameter:: This section describes all parameters. Usage You can check whether the name is already used in database: ``Logs`` is table name and ``timestamp`` is column name in ``Logs.timestamp``. ``name`` ``object_exist`` ``object_exist`` doesn't check object type. The existing object may be table, column, function and so on. ``object_exist`` returns whether object with the specified name exists or not in database. 