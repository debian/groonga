��            )         �     �     �     �     �  ?     [   L     �  
   �     �     �  =   �     &  /   /     _     g     n     �  %   �  0   �  &   �     &  %   ,  
   R  	   ]  U   g     �  M   �  N     N   i     �  1  �     �          '     9  ?   M  [   �     �  
   �     	     	  =   )	     g	  /   p	     �	     �	     �	     �	  %   �	  0   
  &   @
     g
  %   m
  
   �
  	   �
  U   �
     �
  M     N   [  N   �     �                                
                           	                                                                             :doc:`/reference/configuration` :doc:`config_delete` :doc:`config_set` Execution example:: Here is an example to get nonexistent configuration item value: Here is an example to set a value to ``alias.column`` configuration item and get the value: Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the key of target configuration item. Summary Syntax The max key size is 4KiB. There is no optional parameter. There is only one required parameter. This command takes only one required parameter:: This section describes all parameters. Usage You can't use an empty string as key. ``HEADER`` ``VALUE`` ``VALUE`` is the value of the configuration item specified by ``key``. It's a string. ``config_get`` ``config_get`` command returns the value of the specified configuration item. ``config_get`` command returns the value of the specified configuration item:: ``config_get`` returns an empty string for nonexistent configuration item key. ``key`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/configuration` :doc:`config_delete` :doc:`config_set` Execution example:: Here is an example to get nonexistent configuration item value: Here is an example to set a value to ``alias.column`` configuration item and get the value: Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the key of target configuration item. Summary Syntax The max key size is 4KiB. There is no optional parameter. There is only one required parameter. This command takes only one required parameter:: This section describes all parameters. Usage You can't use an empty string as key. ``HEADER`` ``VALUE`` ``VALUE`` is the value of the configuration item specified by ``key``. It's a string. ``config_get`` ``config_get`` command returns the value of the specified configuration item. ``config_get`` command returns the value of the specified configuration item:: ``config_get`` returns an empty string for nonexistent configuration item key. ``key`` 