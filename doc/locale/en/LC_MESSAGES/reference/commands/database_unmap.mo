��          �      |      �     �  6     L   <  �   �     u  
   �     �     �  ;   �     �     �  9         :     Z  "   z  &   �     �  K   �  �        �  �   �  1  S     �  6   �  L   �  �        		  
   	     (	     <	  ;   I	     �	     �	  9   �	     �	     �	  "   
  &   1
     X
  K   ^
  �   �
     -  �   @                 	                                                                            
    Execution example:: If the command fails, error details are in ``HEADER``. If the max number of threads is larger than ``1``, ``database_unmap`` fails: Normally, you don't need to use ``database_unmap`` because OS manages memory cleverly. If remained system memory is reduced, OS moves memory used by Groonga to disk until Groonga needs the memory. OS moves unused memory preferentially. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. Summary Syntax The command returns ``true`` as body on success such as:: There is no optional parameter. There is no required parameter. This command takes no parameters:: This section describes all parameters. Usage You can unmap database after you change the max number of threads to ``1``: You can use this command only when :doc:`thread_limit` returns ``1``. It means that this command doesn't work with multithreading. ``database_unmap`` ``database_unmap`` unmaps already mapped tables and columns in the database. "Map" means that loading from disk to memory. "Unmap" means that releasing mapped memory. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: If the command fails, error details are in ``HEADER``. If the max number of threads is larger than ``1``, ``database_unmap`` fails: Normally, you don't need to use ``database_unmap`` because OS manages memory cleverly. If remained system memory is reduced, OS moves memory used by Groonga to disk until Groonga needs the memory. OS moves unused memory preferentially. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. Summary Syntax The command returns ``true`` as body on success such as:: There is no optional parameter. There is no required parameter. This command takes no parameters:: This section describes all parameters. Usage You can unmap database after you change the max number of threads to ``1``: You can use this command only when :doc:`thread_limit` returns ``1``. It means that this command doesn't work with multithreading. ``database_unmap`` ``database_unmap`` unmaps already mapped tables and columns in the database. "Map" means that loading from disk to memory. "Unmap" means that releasing mapped memory. 