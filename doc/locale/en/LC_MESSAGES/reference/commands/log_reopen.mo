��          �      \      �     �     �  �     "   �  h   �  
   $  _   /     �     �     �     �  1   �  F   �     -  �   M  "   .     Q     W  3   f  1  �     �     �  �     "   �  h   �  
     _   *     �     �     �     �  1   �  F   �     (	  �   H	  "   )
     L
     R
  3   a
                 	                                                      
                            :doc:`log_level` :doc:`log_put` Execute ``log_reopen`` command. It is used to reload log files such as groonga log or query log which are specified by ``--log-path`` or ``--query-log-path`` options. Lotate log files with `log_reopen` New log file is created as same as existing log file name. newer log content is written to new log file. Parameters Rename target log files such as mv command. (Log content is still written into moved log files) Return value See also Summary Syntax The command returns ``false`` otherwise such as:: The command returns ``true`` as body if the command succeeds such as:: There is no required parameter. This command only works when the number of worker processes is equal to 1.  Thus, it means that if you use :doc:`/reference/executables/groonga-httpd` with 2 or more workers, you must use ``groonga-httpd -s reopen`` instead. This command takes no parameters:: Usage ``log_reopen`` ``log_reopen`` is a command that reloads log files. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`log_level` :doc:`log_put` Execute ``log_reopen`` command. It is used to reload log files such as groonga log or query log which are specified by ``--log-path`` or ``--query-log-path`` options. Lotate log files with `log_reopen` New log file is created as same as existing log file name. newer log content is written to new log file. Parameters Rename target log files such as mv command. (Log content is still written into moved log files) Return value See also Summary Syntax The command returns ``false`` otherwise such as:: The command returns ``true`` as body if the command succeeds such as:: There is no required parameter. This command only works when the number of worker processes is equal to 1.  Thus, it means that if you use :doc:`/reference/executables/groonga-httpd` with 2 or more workers, you must use ``groonga-httpd -s reopen`` instead. This command takes no parameters:: Usage ``log_reopen`` ``log_reopen`` is a command that reloads log files. 