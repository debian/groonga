��          �   %   �      p     q     �     �  <   �  A   �  7   *  Q   b  K   �  @      
   A     L  =   Y     �  &   �     �     �  A   �  0     �   I  &   �     !  
   '     2     G  x   X  O   �     !  1  1     c     w     �  <   �  A   �  7   	  Q   T	  K   �	  @   �	  
   3
     >
  =   K
     �
  &   �
     �
     �
  A   �
  0   
  �   ;  &   �       
        $     9  x   J  O   �                                                                   	                                                         
    :doc:`lock_acquire` :doc:`lock_clear` Execution example:: Here is an example to release the lock of ``Entries`` table: Here is an example to release the lock of ``Sites.title`` column: Here is an example to release the lock of the database: If ``target_name`` parameters is omitted, database is used for the target object. If command succeeded, it returns true, otherwise it returns false on error. If you don't specify it, database is used for the target object. Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the name of table or column. Summary Syntax The default is none. It means that the target object is database. This command takes only one optional parameter:: This is a dangerous command. You must only release locks that you acquire by :doc:`lock_acquire`. If you release locks without :doc:`lock_acquire`, your database may be broken. This section describes all parameters. Usage ``HEADER`` ``SUCCEEDED_OR_NOT`` ``lock_release`` ``lock_release`` command releases the lock of the target object. The target object is one of database, table and column. ``lock_release`` command returns whether lock is released successfully or not:: ``target_name`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`lock_acquire` :doc:`lock_clear` Execution example:: Here is an example to release the lock of ``Entries`` table: Here is an example to release the lock of ``Sites.title`` column: Here is an example to release the lock of the database: If ``target_name`` parameters is omitted, database is used for the target object. If command succeeded, it returns true, otherwise it returns false on error. If you don't specify it, database is used for the target object. Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the name of table or column. Summary Syntax The default is none. It means that the target object is database. This command takes only one optional parameter:: This is a dangerous command. You must only release locks that you acquire by :doc:`lock_acquire`. If you release locks without :doc:`lock_acquire`, your database may be broken. This section describes all parameters. Usage ``HEADER`` ``SUCCEEDED_OR_NOT`` ``lock_release`` ``lock_release`` command releases the lock of the target object. The target object is one of database, table and column. ``lock_release`` command returns whether lock is released successfully or not:: ``target_name`` 