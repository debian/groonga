��          �      |      �  .   �        @   4  6   u     �  
   �     �     �  ;   �     (     0  7   7     o     �      �  &   �     �  m   �     k  -   {  �   �  1  *  .   \     �  @   �  6   �       
   +     6     J  ;   W     �     �  7   �     �     �        &   ;     b  m   h     �  -   �  �   	                 	                                                                            
    Currently, this command works only on Windows. Execution example:: For example, ``thread_dump`` puts a backtrace in a log as below. If ``thread_dump`` fails, error details are in HEADER. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. Summary Syntax The command returns `true` as body on success such as:: There is no optional parameter. There is no required parameter. This command has not parameter:: This section describes all parameters. Usage We can get a backtrace of all threads into a log as logs of NOTICE level at the time of running this command. ``thread_dump`` ``thread_dump`` has the following a features: ``thread_dump`` puts a backtrace of all threads into a log as logs of NOTICE level at the time of running this command as below. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Currently, this command works only on Windows. Execution example:: For example, ``thread_dump`` puts a backtrace in a log as below. If ``thread_dump`` fails, error details are in HEADER. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. Summary Syntax The command returns `true` as body on success such as:: There is no optional parameter. There is no required parameter. This command has not parameter:: This section describes all parameters. Usage We can get a backtrace of all threads into a log as logs of NOTICE level at the time of running this command. ``thread_dump`` ``thread_dump`` has the following a features: ``thread_dump`` puts a backtrace of all threads into a log as logs of NOTICE level at the time of running this command as below. 