��             +         �     �     �     �       c   $  K   �     �  
   �     �       =        R  /   [  J   �     �     �     �  *   �     *     I  ,   i  &   �     �  %   �  
   �     �     	  _     H   x     �  	   �  1  �          %     :     L  c   `  K   �     	  
   $	     /	     C	  =   P	     �	  /   �	  J   �	     
     
     !
  *   ;
     f
     �
  ,   �
  &   �
     �
  %   �
  
   %     0     E  _   T  H   �     �  	                 
       	                                                                                                                           :doc:`/reference/configuration` :doc:`config_delete` :doc:`config_get` Execution example:: Here is an example to set a value to ``alias.column`` configuration item and confirm the set value: If command succeeded, it returns true, otherwise it returns false on error. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the key of target configuration item. Specifies the value of the target configuration item specified by ``key``. Summary Syntax The max key size is 4KiB. The max value size is 4091B (= 4KiB - 5B). There are required parameters. There is no optional parameter. This command takes two required parameters:: This section describes all parameters. Usage You can't use an empty string as key. ``HEADER`` ``SUCCEEDED_OR_NOT`` ``config_set`` ``config_set`` command returns whether setting a configuration item value is succeeded or not:: ``config_set`` command sets a value to the specified configuration item. ``key`` ``value`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/configuration` :doc:`config_delete` :doc:`config_get` Execution example:: Here is an example to set a value to ``alias.column`` configuration item and confirm the set value: If command succeeded, it returns true, otherwise it returns false on error. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the key of target configuration item. Specifies the value of the target configuration item specified by ``key``. Summary Syntax The max key size is 4KiB. The max value size is 4091B (= 4KiB - 5B). There are required parameters. There is no optional parameter. This command takes two required parameters:: This section describes all parameters. Usage You can't use an empty string as key. ``HEADER`` ``SUCCEEDED_OR_NOT`` ``config_set`` ``config_set`` command returns whether setting a configuration item value is succeeded or not:: ``config_set`` command sets a value to the specified configuration item. ``key`` ``value`` 