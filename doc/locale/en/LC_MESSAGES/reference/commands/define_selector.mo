��    -      �  =   �      �     �    �  
             ,     5     =     D     J     ^     r     �     �     �  
   �  	   �     �     �  
   �       	     
     
   *  	   5  "   ?  -   b  l   �  c   �  r   a  >   �  0     b   D  c   �  k   	  c   w	  \   �	  Y   8
  X   �
  `   �
  Y   L  a   �  X     Y   a  Y   �  1       G    b  
   z     �     �     �     �     �     �     �     �     �     
       
   -  	   8     B     T  
   ]     h  	   {  
   �  
   �  	   �  "   �  -   �  l   �  c   c  r   �  >   :  0   y  b   �  c     k   q  c   �  \   A  Y   �  X   �  `   Q  Y   �  a     X   n  Y   �  Y   !        *                   
   "            +                                 	   !       $                  &   ,                 -   )                                 (      #          '          %                  :doc:`/reference/grn_expr` Groonga組込コマンドの一つであるdefine_selectorについて説明します。組込コマンドは、groonga実行ファイルの引数、標準入力、またはソケット経由でgroongaサーバにリクエストを送信することによって実行します。 Parameters Return value See also Summary Syntax Usage ``define_selector`` ``drilldown_limit`` ``drilldown_offset`` ``drilldown_output_columns`` ``drilldown_sortby`` ``drilldown`` ``filter`` ``limit`` ``match_columns`` ``name`` ``offset`` ``output_columns`` ``query`` ``scorer`` ``sortby`` ``table`` ``成功かどうかのフラグ`` define_selector - 検索コマンドを定義 define_selectorは、検索条件をカスタマイズした新たな検索コマンドを定義します。 エラーが生じなかった場合にはtrue、エラーが生じた場合にはfalseを返す。 テーブルEntryの全レコード・全カラムの値を出力するselectorコマンドを定義します。:: 定義するselectorコマンドの名前を指定します。 検索対象のテーブルを指定します。 追加するselectorコマンドのdrilldown_limit引数のデフォルト値を指定します。 追加するselectorコマンドのdrilldown_offset引数のデフォルト値を指定します。 追加するselectorコマンドのdrilldown_output_columns引数のデフォルト値を指定します。 追加するselectorコマンドのdrilldown_sortby引数のデフォルト値を指定します。 追加するselectorコマンドのdrilldown引数のデフォルト値を指定します。 追加するselectorコマンドのfilter引数のデフォルト値を指定します。 追加するselectorコマンドのlimit引数のデフォルト値を指定します。 追加するselectorコマンドのmatch_columns引数のデフォルト値を指定します。 追加するselectorコマンドのoffset引数のデフォルト値を指定します。 追加するselectorコマンドのoutput_columns引数のデフォルト値を指定します。 追加するselectorコマンドのquery引数のデフォルト値を指定します。 追加するselectorコマンドのscorer引数のデフォルト値を指定します。 追加するselectorコマンドのsortby引数のデフォルト値を指定します。 Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/grn_expr` Groonga組込コマンドの一つであるdefine_selectorについて説明します。組込コマンドは、groonga実行ファイルの引数、標準入力、またはソケット経由でgroongaサーバにリクエストを送信することによって実行します。 Parameters Return value See also Summary Syntax Usage ``define_selector`` ``drilldown_limit`` ``drilldown_offset`` ``drilldown_output_columns`` ``drilldown_sortby`` ``drilldown`` ``filter`` ``limit`` ``match_columns`` ``name`` ``offset`` ``output_columns`` ``query`` ``scorer`` ``sortby`` ``table`` ``成功かどうかのフラグ`` define_selector - 検索コマンドを定義 define_selectorは、検索条件をカスタマイズした新たな検索コマンドを定義します。 エラーが生じなかった場合にはtrue、エラーが生じた場合にはfalseを返す。 テーブルEntryの全レコード・全カラムの値を出力するselectorコマンドを定義します。:: 定義するselectorコマンドの名前を指定します。 検索対象のテーブルを指定します。 追加するselectorコマンドのdrilldown_limit引数のデフォルト値を指定します。 追加するselectorコマンドのdrilldown_offset引数のデフォルト値を指定します。 追加するselectorコマンドのdrilldown_output_columns引数のデフォルト値を指定します。 追加するselectorコマンドのdrilldown_sortby引数のデフォルト値を指定します。 追加するselectorコマンドのdrilldown引数のデフォルト値を指定します。 追加するselectorコマンドのfilter引数のデフォルト値を指定します。 追加するselectorコマンドのlimit引数のデフォルト値を指定します。 追加するselectorコマンドのmatch_columns引数のデフォルト値を指定します。 追加するselectorコマンドのoffset引数のデフォルト値を指定します。 追加するselectorコマンドのoutput_columns引数のデフォルト値を指定します。 追加するselectorコマンドのquery引数のデフォルト値を指定します。 追加するselectorコマンドのscorer引数のデフォルト値を指定します。 追加するselectorコマンドのsortby引数のデフォルト値を指定します。 