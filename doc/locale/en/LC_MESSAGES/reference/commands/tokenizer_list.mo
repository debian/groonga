��          �      �        #   	     -     J     V     j  $   �     �     �  =   �     �          
  "        4     D  
   J     U     ^  :   q  �   �     E  b   T  1  �  #   �          *     6     J  $   d     �     �  =   �     �     �     �  "   �          $  
   *     5     >  :   Q  �   �     %  b   4                             
                                                       	                    :doc:`/reference/commands/tokenize` :doc:`/reference/tokenizers` Description Execution example:: Here is a simple example. It returns tokenizers in a database. Name Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Summary Syntax This command takes no parameters:: Tokenizer name. Usage ``HEADER`` ``name`` ``tokenizer_list`` ``tokenizer_list`` command lists tokenizers in a database. ``tokenizer_list`` command returns tokenizers. Each tokenizers has an attribute that contains the name. The attribute will be increased in the feature:: ``tokenizers`` ``tokenizers`` is an array of tokenizer. Tokenizer is an object that has the following attributes. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/commands/tokenize` :doc:`/reference/tokenizers` Description Execution example:: Here is a simple example. It returns tokenizers in a database. Name Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Summary Syntax This command takes no parameters:: Tokenizer name. Usage ``HEADER`` ``name`` ``tokenizer_list`` ``tokenizer_list`` command lists tokenizers in a database. ``tokenizer_list`` command returns tokenizers. Each tokenizers has an attribute that contains the name. The attribute will be increased in the feature:: ``tokenizers`` ``tokenizers`` is an array of tokenizer. Tokenizer is an object that has the following attributes. 