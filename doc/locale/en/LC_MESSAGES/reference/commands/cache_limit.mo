��          �   %   �      P     Q     _  G   s  r   �  �   .  �   �  
   {     �  =   �     �  <   �            0   &  &   W     ~  c   �  `   �  
   I     T  O   b     �  v   �  G   9     �  1  �     �     �  G   �  r   %	  �   �	  �   G
  
   �
     �
  =   �
     ;  <   D     �     �  0   �  &   �     �  c   �  `   R  
   �     �  O   �       v   ,  G   �     �                  
                                                                 	                                          :doc:`select` Execution example:: Here is an example that sets ``10`` as the max number of cache entries. If ``max`` parameter is used, the return value is the max number of cache entries before ``max`` parameter is set. If ``max`` parameter isn't specified, the current max number of query cache entries isn't changed. ``cache_limit`` just returns the current max number of query cache entries. If the max number of query cache entries is 100, the recent 100 ``select`` commands are only cached. The cache expire algorithm is LRU (least recently used). Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the max number of query cache entries as a number. Summary Syntax This command takes only one optional parameter:: This section describes all parameters. Usage You can get the current max number of cache entries by executing ``cache_limit`` without parameter. You can set the max number of cache entries by executing ``cache_limit`` with ``max`` parameter. ``HEADER`` ``N_ENTRIES`` ``N_ENTRIES`` is the current max number of query cache entries. It is a number. ``cache_limit`` ``cache_limit`` gets or sets the max number of query cache entries. Query cache is used only by :doc:`select` command. ``cache_limit`` returns the current max number of query cache entries:: ``max`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`select` Execution example:: Here is an example that sets ``10`` as the max number of cache entries. If ``max`` parameter is used, the return value is the max number of cache entries before ``max`` parameter is set. If ``max`` parameter isn't specified, the current max number of query cache entries isn't changed. ``cache_limit`` just returns the current max number of query cache entries. If the max number of query cache entries is 100, the recent 100 ``select`` commands are only cached. The cache expire algorithm is LRU (least recently used). Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the max number of query cache entries as a number. Summary Syntax This command takes only one optional parameter:: This section describes all parameters. Usage You can get the current max number of cache entries by executing ``cache_limit`` without parameter. You can set the max number of cache entries by executing ``cache_limit`` with ``max`` parameter. ``HEADER`` ``N_ENTRIES`` ``N_ENTRIES`` is the current max number of query cache entries. It is a number. ``cache_limit`` ``cache_limit`` gets or sets the max number of query cache entries. Query cache is used only by :doc:`select` command. ``cache_limit`` returns the current max number of query cache entries:: ``max`` 