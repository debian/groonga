��          �   %   �      @     A  6   U  U   �  ?   �  �   "  K   �  @   �  
   6     A  =   N  &   �     �     �  A   �  0     �   5  &        4  
   :     E     Z     i  L   �     6  1  F     x  6   �  U   �  ?   	  �   Y	  K   �	  @   ,
  
   m
     x
  =   �
  &   �
     �
     �
  A   �
  0   ;  �   l  &   D     k  
   q     |     �     �  L         m                                                                                    	         
                                 Execution example:: Here is an example to clear all locks in the database: Here is an example to clear locks of ``Entries`` table and ``Entries`` table columns: Here is an example to clear the lock of ``Sites.title`` column: If ``target_name`` parameters is omitted, database is used for the target object. It means that all locks in the database are cleared. If command succeeded, it returns true, otherwise it returns false on error. If you don't specify it, database is used for the target object. Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. Specifies the name of table or column. Summary Syntax The default is none. It means that the target object is database. This command takes only one optional parameter:: This is a dangerous command. You must not use this command while other process or thread is doing a write operation to the target object. If you do it, your database may be broken and/or your process may be crashed. This section describes all parameters. Usage ``HEADER`` ``SUCCEEDED_OR_NOT`` ``lock_clear`` ``lock_clear`` command clear the lock of the target object recursively. The target object is one of database, table and column. ``lock_clear`` command returns whether lock is cleared successfully or not:: ``target_name`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here is an example to clear all locks in the database: Here is an example to clear locks of ``Entries`` table and ``Entries`` table columns: Here is an example to clear the lock of ``Sites.title`` column: If ``target_name`` parameters is omitted, database is used for the target object. It means that all locks in the database are cleared. If command succeeded, it returns true, otherwise it returns false on error. If you don't specify it, database is used for the target object. Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. Specifies the name of table or column. Summary Syntax The default is none. It means that the target object is database. This command takes only one optional parameter:: This is a dangerous command. You must not use this command while other process or thread is doing a write operation to the target object. If you do it, your database may be broken and/or your process may be crashed. This section describes all parameters. Usage ``HEADER`` ``SUCCEEDED_OR_NOT`` ``lock_clear`` ``lock_clear`` command clear the lock of the target object recursively. The target object is one of database, table and column. ``lock_clear`` command returns whether lock is cleared successfully or not:: ``target_name`` 