��          �      �        $   	     .     L     X     l  %   �     �     �     �  =   �                 "   %     H  
   N     Y     b  <   v  �   �     O  e   _  1  �  $   �          :     F     Z  %   t     �     �     �  =   �     �            "        6  
   <     G     P  <   d  �   �     =  e   M                                                                                  
           	           :doc:`/reference/commands/normalize` :doc:`/reference/normalizers` Description Execution example:: Here is a simple example. It returns normalizers in a database. Name Normalizer name. Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Summary Syntax This command takes no parameters:: Usage ``HEADER`` ``name`` ``normalizer_list`` ``normalizer_list`` command lists normalizers in a database. ``normalizer_list`` command returns normalizers. Each normalizers has an attribute that contains the name. The attribute will be increased in the feature:: ``normalizers`` ``normalizers`` is an array of normalizer. Normalizer is an object that has the following attributes. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/commands/normalize` :doc:`/reference/normalizers` Description Execution example:: Here is a simple example. It returns normalizers in a database. Name Normalizer name. Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Summary Syntax This command takes no parameters:: Usage ``HEADER`` ``name`` ``normalizer_list`` ``normalizer_list`` command lists normalizers in a database. ``normalizer_list`` command returns normalizers. Each normalizers has an attribute that contains the name. The attribute will be increased in the feature:: ``normalizers`` ``normalizers`` is an array of normalizer. Normalizer is an object that has the following attributes. 