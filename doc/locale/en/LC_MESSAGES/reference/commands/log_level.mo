��    &      L  5   |      P      Q     r  	   x  K   �  
   �     �  =   �     $  C   -     q     y     �     �     �     �     �     �  
   �     �  	   �     �     �  	   �     �             	   "     ,     2  	   ;     E  S   S  0   �     �  
   �     �     �  1        9     Z  	   `  K   j  
   �     �  =   �       C        Y     a     h     n     t     z     �     �  
   �     �  	   �     �     �  	   �     �     �     �  	   
	     	     	  	   #	     -	  S   ;	  0   �	     �	  
   �	     �	     �	     &                                 "                       #                                       	                 
                               %      $       !                 :doc:`log_put` :doc:`log_reopen` Alias Example:: If command succeeded, it returns true, otherwise it returns false on error. Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specify log level with a character or string which means log level. Summary Syntax Usage Value ``-`` ``A`` ``C`` ``E`` ``HEADER`` ``SUCCEEDED_OR_NOT`` ``alert`` ``crit`` or ``critical`` ``d`` ``debug`` ``dump`` ``e`` ``emerge`` or ``emergency`` ``error`` ``i`` ``info`` ``level`` ``log_level`` ``log_level`` command returns whether log level configuration is succeeded or not:: ``log_level`` command sets log level of Groonga. ``n`` ``notice`` ``w`` ``warn`` or ``warning`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`log_put` :doc:`log_reopen` Alias Example:: If command succeeded, it returns true, otherwise it returns false on error. Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specify log level with a character or string which means log level. Summary Syntax Usage Value ``-`` ``A`` ``C`` ``E`` ``HEADER`` ``SUCCEEDED_OR_NOT`` ``alert`` ``crit`` or ``critical`` ``d`` ``debug`` ``dump`` ``e`` ``emerge`` or ``emergency`` ``error`` ``i`` ``info`` ``level`` ``log_level`` ``log_level`` command returns whether log level configuration is succeeded or not:: ``log_level`` command sets log level of Groonga. ``n`` ``notice`` ``w`` ``warn`` or ``warning`` 