��            )         �  V   �            <   #     `  ;   t  *   �  K   �  �   '  
   �     �  =   �     �  �     n   �  �     2   �     �     �  @   �  �        �  
   �       
     5   '  
   ]     h     o  	   w  1  �  V   �	     

     
  <   %
     b
  ;   v
  *   �
  K   �
  �   )  
   �     �  =   �        �   	  n   �  �     2   �     �     �  @   �  �        �  
   �     	  
     5   )  
   _     j     q  	   y                         
                                          	                                                                     "Cascaded delete" removes the records which matches specified key and refers that key. :doc:`load` Cascade delete Delete the record from Entry table which has "2" as the key. Execution example:: Here are a schema definition and sample data to show usage. Here is the example about cascaded delete. If command succeeded, it returns true, otherwise it returns false on error. Note that the type of other table's column is COLUMN_VECTOR, only the value of referencing key is removed from the vector value. Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the expression of grn_expr to identify the record. If you specify ``filter`` parameter, you must not specify ``key`` and ``id`` parameter. Specifies the id of record to delete. If you specify ``id`` parameter, you must not specify ``key`` parameter. Specifies the key of record to delete. If you use the table with TABLE_NO_KEY, the key is just ignored. (Use ``id`` parameter in such a case) Specifies the name of table to delete the records. Summary Syntax The country column of Users table associates with Country table. There is a case that multiple table is associated. For example, the key of one table are referenced by other table's records. In such a case, if you delete the key of one table, other table's records are also removed. Usage ``HEADER`` ``SUCCEEDED_OR_NOT`` ``delete`` ``delete`` command deletes specified record of table. ``filter`` ``id`` ``key`` ``table`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 "Cascaded delete" removes the records which matches specified key and refers that key. :doc:`load` Cascade delete Delete the record from Entry table which has "2" as the key. Execution example:: Here are a schema definition and sample data to show usage. Here is the example about cascaded delete. If command succeeded, it returns true, otherwise it returns false on error. Note that the type of other table's column is COLUMN_VECTOR, only the value of referencing key is removed from the vector value. Parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the expression of grn_expr to identify the record. If you specify ``filter`` parameter, you must not specify ``key`` and ``id`` parameter. Specifies the id of record to delete. If you specify ``id`` parameter, you must not specify ``key`` parameter. Specifies the key of record to delete. If you use the table with TABLE_NO_KEY, the key is just ignored. (Use ``id`` parameter in such a case) Specifies the name of table to delete the records. Summary Syntax The country column of Users table associates with Country table. There is a case that multiple table is associated. For example, the key of one table are referenced by other table's records. In such a case, if you delete the key of one table, other table's records are also removed. Usage ``HEADER`` ``SUCCEEDED_OR_NOT`` ``delete`` ``delete`` command deletes specified record of table. ``filter`` ``id`` ``key`` ``table`` 