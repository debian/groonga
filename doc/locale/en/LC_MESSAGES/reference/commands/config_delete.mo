��            )         �     �     �     �     �  A   	  <   K  K   �     �  
   �     �       =        R  /   [     �     �     �     �  %   �  0   �  &   +     R  %   X  
   ~     �     �  C   �  ]   �  Y   R     �  1  �     �               *  A   >  <   �  K   �     		  
   	     (	     <	  =   I	     �	  /   �	     �	     �	     �	     �	  %   	
  0   /
  &   `
     �
  %   �
  
   �
     �
     �
  C   �
  ]   )  Y   �     �                                                   	      
                                                                               :doc:`/reference/configuration` :doc:`config_get` :doc:`config_set` Execution example:: Here is an example to delete ``alias.column`` configuration item: Here is an example to delete nonexistent configuration item: If command succeeded, it returns true, otherwise it returns false on error. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the key of target configuration item. Summary Syntax The max key size is 4KiB. There is no optional parameter. There is only one required parameter. This command takes only one required parameter:: This section describes all parameters. Usage You can't use an empty string as key. ``HEADER`` ``SUCCEEDED_OR_NOT`` ``config_delete`` ``config_delete`` command deletes the specified configuration item. ``config_delete`` command returns whether deleting a configuration item is succeeded or not:: ``config_delete`` returns an error when you try to delete nonexistent configuration item. ``key`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/configuration` :doc:`config_get` :doc:`config_set` Execution example:: Here is an example to delete ``alias.column`` configuration item: Here is an example to delete nonexistent configuration item: If command succeeded, it returns true, otherwise it returns false on error. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the key of target configuration item. Summary Syntax The max key size is 4KiB. There is no optional parameter. There is only one required parameter. This command takes only one required parameter:: This section describes all parameters. Usage You can't use an empty string as key. ``HEADER`` ``SUCCEEDED_OR_NOT`` ``config_delete`` ``config_delete`` command deletes the specified configuration item. ``config_delete`` command returns whether deleting a configuration item is succeeded or not:: ``config_delete`` returns an error when you try to delete nonexistent configuration item. ``key`` 