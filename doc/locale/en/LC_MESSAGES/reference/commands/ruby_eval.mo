��          �   %   �      0     1  @   E  u   �  
   �  F        N  =   [     �  5   �     �     �  0   �  &        ?  J   E     �  >   �  
   �     �  C   �  �   @  r   �  
   7  1  B     t  @   �  u   �  
   ?  F   J     �  =   �     �  5   �     	     #	  0   *	  &   [	     �	  J   �	     �	  >   �	  
   &
     1
  C   ?
  �   �
  r     
   z                              	                                               
                                                  Execution example:: Here is an example that just calculate ``1 + 2`` as Ruby script. Note that ``ruby_eval`` is implemented as an experimental plugin, and the specification may be changed in the future. Parameters Register ``ruby/eval`` plugin to use ``ruby_eval`` command in advance. Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the Ruby script which you want to evaluate. Summary Syntax This command takes only one required parameter:: This section describes all parameters. Usage You can execute any scripts which mruby supports by calling ``ruby_eval``. ``EVALUATED_VALUE`` ``EVALUATED_VALUE`` is the evaluated value of ``ruby_script``. ``HEADER`` ``ruby_eval`` ``ruby_eval`` command evaluates Ruby script and returns the result. ``ruby_eval`` returns the evaluated result with metadata such as exception information (Including metadata isn't implemented yet):: ``ruby_eval`` supports only a number for evaluated value for now. Supported types will be increased in the future. ``script`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here is an example that just calculate ``1 + 2`` as Ruby script. Note that ``ruby_eval`` is implemented as an experimental plugin, and the specification may be changed in the future. Parameters Register ``ruby/eval`` plugin to use ``ruby_eval`` command in advance. Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the Ruby script which you want to evaluate. Summary Syntax This command takes only one required parameter:: This section describes all parameters. Usage You can execute any scripts which mruby supports by calling ``ruby_eval``. ``EVALUATED_VALUE`` ``EVALUATED_VALUE`` is the evaluated value of ``ruby_script``. ``HEADER`` ``ruby_eval`` ``ruby_eval`` command evaluates Ruby script and returns the result. ``ruby_eval`` returns the evaluated result with metadata such as exception information (Including metadata isn't implemented yet):: ``ruby_eval`` supports only a number for evaluated value for now. Supported types will be increased in the future. ``script`` 