��          �   %   �      @     A     \     p     �  
   �     �     �  ;   �       k        y     �  >   �  4   �     �  %     0   B  "   s     �  d   �  =        ?  a   V     �  1  �     �          +     C  
   W     b     v  ;   �     �  k   �     4     <  >   C  4   �     �  %   �  0   �  "   .	     Q	  d   W	  =   �	     �	  a   
     s
                             
         	                                                                                        :doc:`/reference/sharding` Execution example:: Here are sample shards: Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. See also Specifies the logical table name. ``logical_shard_list`` returns a list of shard name of the logical table: Summary Syntax The command returns a list of shard names in ascending order:: The list is sorted by shard name in ascending order. There is no optional parameter. There is only one required parameter. This command takes only one required parameter:: This section describes parameters. Usage You can get the all shard names in ascending order by specifying ``Logs`` as the logical table name: You need to register ``sharding`` plugin to use this command: ``logical_shard_list`` ``logical_shard_list`` returns all existing shard names against the specified logical table name. ``logical_table`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/sharding` Execution example:: Here are sample shards: Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. See also Specifies the logical table name. ``logical_shard_list`` returns a list of shard name of the logical table: Summary Syntax The command returns a list of shard names in ascending order:: The list is sorted by shard name in ascending order. There is no optional parameter. There is only one required parameter. This command takes only one required parameter:: This section describes parameters. Usage You can get the all shard names in ascending order by specifying ``Logs`` as the logical table name: You need to register ``sharding`` plugin to use this command: ``logical_shard_list`` ``logical_shard_list`` returns all existing shard names against the specified logical table name. ``logical_table`` 