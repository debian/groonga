��          �   %   �      0     1  ;   E  ,   �  0   �  3   �  
             2     ?     L     [     t     |     �  %   �  &   �  q   �  V   a     �  8   �  
   �       0     1  I     {  ;   �  ,   �  0   �  3   )  
   ]     h     |     �     �     �     �     �     �  %   �  &     q   9  V   �     	  8   	  
   A	     L	  0   b	                                                                                      	                  
                       Execution example:: Here are a schema definition and sample data to show usage. Here is a schema definition and sample data. Here is a simple usage of ``time_classify_day``: It returns a value that rounded time to a day unit. Parameters Required parameters Return value Sample data: Sample schema: Specify a target column. Summary Syntax The return value is UNIX time. There is only one required parameter. This function has only one parameter:: This rounded ``2020-01-30 11:50:11.000000`` and ``2020-01-30 22:50:11.000000`` to ``2020-01-30 00:00:00.000000``. To enable this function, register ``functions/time`` plugin by the following command:: Usage You need to register ``functions/time`` plugin at first: ``column`` ``time_classify_day`` ``time_classify_day`` rounds time to a day unit. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here are a schema definition and sample data to show usage. Here is a schema definition and sample data. Here is a simple usage of ``time_classify_day``: It returns a value that rounded time to a day unit. Parameters Required parameters Return value Sample data: Sample schema: Specify a target column. Summary Syntax The return value is UNIX time. There is only one required parameter. This function has only one parameter:: This rounded ``2020-01-30 11:50:11.000000`` and ``2020-01-30 22:50:11.000000`` to ``2020-01-30 00:00:00.000000``. To enable this function, register ``functions/time`` plugin by the following command:: Usage You need to register ``functions/time`` plugin at first: ``column`` ``time_classify_day`` ``time_classify_day`` rounds time to a day unit. 