��          �      �           	  ;     b   Y  
   �     �     �     �     �  #   �     !     )  %   0     V  �   \     
  Y     M   s  /   �  :   �  M   ,  A   z     �  1  �     �  ;     b   G  
   �     �     �     �     �  #   �            %        D  �   J     �  Y   	  M   a	  /   �	  :   �	  M   
  A   h
     �
                                            	                           
                                 Execution example:: Here are a schema definition and sample data to show usage. Here is the simple usage of ``html_untag`` function which strips HTML tags from content of column. Parameters Requirements Return value Sample data: Sample schema: Specifies HTML text to be untagged. Summary Syntax There is only one required parameter. Usage When executing the above query, you can see "span" tag with "class" attribute is stripped. Note that you must specify ``--command_version 2`` to use ``html_untag`` function. ``html_untag`` ``html_untag`` is used in ``--output_columns`` described at :ref:`select-output-columns`. ``html_untag`` requires :doc:`/reference/command/command_version` 2 or later. ``html_untag`` requires Groonga 3.0.5 or later. ``html_untag`` requires only one argument. It is ``html``. ``html_untag`` returns plain text which is stripped HTML tags from HTML text. ``html_untag`` strips HTML tags from HTML and outputs plain text. ``html`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here are a schema definition and sample data to show usage. Here is the simple usage of ``html_untag`` function which strips HTML tags from content of column. Parameters Requirements Return value Sample data: Sample schema: Specifies HTML text to be untagged. Summary Syntax There is only one required parameter. Usage When executing the above query, you can see "span" tag with "class" attribute is stripped. Note that you must specify ``--command_version 2`` to use ``html_untag`` function. ``html_untag`` ``html_untag`` is used in ``--output_columns`` described at :ref:`select-output-columns`. ``html_untag`` requires :doc:`/reference/command/command_version` 2 or later. ``html_untag`` requires Groonga 3.0.5 or later. ``html_untag`` requires only one argument. It is ``html``. ``html_untag`` returns plain text which is stripped HTML tags from HTML text. ``html_untag`` strips HTML tags from HTML and outputs plain text. ``html`` 