��          �      l      �  Y   �     ;  ,   O  Z   |  
   �     �     �     �  S        _     g  %   n  �   �  V   A     �     �  0   �  ?   �  1     
   N  1  Y  Y   �     �  ,   �  Z   &  
   �     �     �     �  S   �     	       %     �   >  V   �     B	     H	  0   U	  ?   �	  1   �	  
   �	                                                                           
          	             By specifying ``--sort_keys from_office``, you can show nearest shops by ascending order. Execution example:: Here is a schema definition and sample data. Here is the simple usage of ``math_abs`` function which returns nearest shops from office. Parameters Return value Sample data: Sample schema: Specifies a column of table that is specified by ``table`` parameter in ``select``. Summary Syntax There is only one required parameter. To detect nearest shops, we need to calculate distance. If the distance of your office from station is 250 meters, you can calculate it by ``math_abs(250 - from_station)``. To enable this function, register ``functions/math`` plugin by following the command:: Usage ``math_abs`` ``math_abs`` requires one argument - ``target``. ``math_abs`` returns the absolute value of target column value. ``math_abs`` returns the absolute value of value. ``target`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 By specifying ``--sort_keys from_office``, you can show nearest shops by ascending order. Execution example:: Here is a schema definition and sample data. Here is the simple usage of ``math_abs`` function which returns nearest shops from office. Parameters Return value Sample data: Sample schema: Specifies a column of table that is specified by ``table`` parameter in ``select``. Summary Syntax There is only one required parameter. To detect nearest shops, we need to calculate distance. If the distance of your office from station is 250 meters, you can calculate it by ``math_abs(250 - from_station)``. To enable this function, register ``functions/math`` plugin by following the command:: Usage ``math_abs`` ``math_abs`` requires one argument - ``target``. ``math_abs`` returns the absolute value of target column value. ``math_abs`` returns the absolute value of value. ``target`` 