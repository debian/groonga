# -*- po -*-
# English translations for Groonga package.
# Copyright (C) 2009-2025 Groonga Project
# This file is distributed under the same license as the Groonga package.
# Automatically generated, 2025.
#
msgid ""
msgstr ""
"Project-Id-Version: Groonga 15.0.2\n"
"Report-Msgid-Bugs-To: \n"
"PO-Revision-Date: 2025-02-21 09:46+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "``time_classify_month``"
msgstr "``time_classify_month``"

msgid "Summary"
msgstr "Summary"

msgid "Syntax"
msgstr "Syntax"

msgid "Usage"
msgstr "Usage"

msgid "Return value"
msgstr "Return value"
