��    #      4  /   L           	  ;        Y  X   s  F   �  7     D   K     �  
   �     �     �     �     �  [   �  7   H  1   �  l   �          :     B     I  X   i     �  1   �     �  
               L   +     x  B   �  5   �  b     
   i  1  t     �	  ;   �	     �	  X   
  F   i
  7   �
  D   �
     -  
   A     L     `     m     z  [   �  7   �  1     l   O     �     �     �     �  X        _  1   e     �  
   �     �     �  L   �       B   *  5   m  b   �  
                 #          
                          	                             !                          "                                                    Execution example:: Here are a schema definition and sample data to show usage. Here is a simple example. If you omit or specify a negative value, this function extracts from ``nth`` to the end. If you specify a negative value, it counts from the end of ``target``. In the following example, specifying the default value. In the following example, specifying the negative value for ``nth``. Optional parameters Parameters Required parameters Return value Sample data: Sample schema: Specify a 0-based index number of charactors where to start the extraction from ``target``. Specify a number of characters to extract from ``nth``. Specify a string literal or a string type column. Specify a string to be returned when a substring is an empty string except when specifying 0 for ``length``. Specify the following key. Summary Syntax The default is an empty string. To enable this function, register ``functions/string`` plugin by following the command:: Usage You can specify string literal instead of column. ``default_value`` ``length`` ``nth`` ``options`` ``options`` uses the following format. All of key-value pairs are optional:: ``string_substring`` ``string_substring`` extracts a substring of a string by position. ``string_substring`` requires two to four parameters. ``string_substring`` returns a substring extracted under the specified conditions from ``target``. ``target`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here are a schema definition and sample data to show usage. Here is a simple example. If you omit or specify a negative value, this function extracts from ``nth`` to the end. If you specify a negative value, it counts from the end of ``target``. In the following example, specifying the default value. In the following example, specifying the negative value for ``nth``. Optional parameters Parameters Required parameters Return value Sample data: Sample schema: Specify a 0-based index number of charactors where to start the extraction from ``target``. Specify a number of characters to extract from ``nth``. Specify a string literal or a string type column. Specify a string to be returned when a substring is an empty string except when specifying 0 for ``length``. Specify the following key. Summary Syntax The default is an empty string. To enable this function, register ``functions/string`` plugin by following the command:: Usage You can specify string literal instead of column. ``default_value`` ``length`` ``nth`` ``options`` ``options`` uses the following format. All of key-value pairs are optional:: ``string_substring`` ``string_substring`` extracts a substring of a string by position. ``string_substring`` requires two to four parameters. ``string_substring`` returns a substring extracted under the specified conditions from ``target``. ``target`` 