��          �      L      �     �  ,   �       
   �     �     �     �  Z   �            %      X   F     �  
   �     �  3   �  ?   �  8   4  1  m     �  ,   �     �  
   `     k     x     �  Z   �     �     �  %   �  X   $     }  
   �     �  3   �  ?   �  8                  
                                                                                 	    Execution example:: Here is a schema definition and sample data. Here is the simple usage of ``vector_size`` function which returns tags and size - the value of ``tags`` column and size of it. Parameters Return value Sample data: Sample schema: Specifies a vector column of table that is specified by ``table`` parameter in ``select``. Summary Syntax There is only one required parameter. To enable this function, register ``functions/vector`` plugin by the following command:: Usage ``target`` ``vector_size`` ``vector_size`` requires one argument - ``target``. ``vector_size`` returns the value of target vector column size. ``vector_size`` returns the value of vector column size. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here is a schema definition and sample data. Here is the simple usage of ``vector_size`` function which returns tags and size - the value of ``tags`` column and size of it. Parameters Return value Sample data: Sample schema: Specifies a vector column of table that is specified by ``table`` parameter in ``select``. Summary Syntax There is only one required parameter. To enable this function, register ``functions/vector`` plugin by the following command:: Usage ``target`` ``vector_size`` ``vector_size`` requires one argument - ``target``. ``vector_size`` returns the value of target vector column size. ``vector_size`` returns the value of vector column size. 