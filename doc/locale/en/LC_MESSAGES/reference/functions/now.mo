��          t      �         �        �     �  I   �                  I   '     q     x  1    �   �     K     O  I   k     �     �     �  I   �                                                
           	           Groonga組込関数の一つであるnowについて説明します。組込関数は、script形式のgrn_expr中で呼び出すことができます。 now now - 現在時刻を返す now() 関数は現在時刻に対応するTime型の値を返します。 例 名前 書式 現在時刻に対応するTime型のオブジェクトを返します。 説明 返値 Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Groonga組込関数の一つであるnowについて説明します。組込関数は、script形式のgrn_expr中で呼び出すことができます。 now now - 現在時刻を返す now() 関数は現在時刻に対応するTime型の値を返します。 例 名前 書式 現在時刻に対応するTime型のオブジェクトを返します。 説明 返値 