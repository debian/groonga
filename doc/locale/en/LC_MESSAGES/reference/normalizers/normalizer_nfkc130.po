# -*- po -*-
# English translations for Groonga package.
# Copyright (C) 2009-2025 Groonga Project
# This file is distributed under the same license as the Groonga package.
# Automatically generated, 2025.
#
msgid ""
msgstr ""
"Project-Id-Version: Groonga 15.0.2\n"
"Report-Msgid-Bugs-To: \n"
"PO-Revision-Date: 2025-02-21 09:46+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "``NormalizerNFKC130``"
msgstr "``NormalizerNFKC130``"

msgid "Use :doc:`./normalizer_nfkc` instead."
msgstr "Use :doc:`./normalizer_nfkc` instead."

msgid "``NormalizerNFKC130`` and ``NormalizerNFKC(\"version\", \"13.0.0\")`` are equal."
msgstr "``NormalizerNFKC130`` and ``NormalizerNFKC(\"version\", \"13.0.0\")`` are equal."

msgid "Summary"
msgstr "Summary"

msgid "``NormalizerNFKC130`` normalizes text by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 13.0."
msgstr "``NormalizerNFKC130`` normalizes text by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 13.0."

msgid "This normalizer can change behavior by specifying options."
msgstr "This normalizer can change behavior by specifying options."

msgid "Syntax"
msgstr "Syntax"

msgid "``NormalizerNFKC130`` has optional parameter."
msgstr "``NormalizerNFKC130`` has optional parameter."

msgid "No options::"
msgstr "No options::"

msgid "Specify option::"
msgstr "Specify option::"

msgid ":ref:`normalizer-nfkc130-unify-katakana-gu-small-sounds` is added."
msgstr ":ref:`normalizer-nfkc130-unify-katakana-gu-small-sounds` is added."

msgid ":ref:`normalizer-nfkc130-unify-katakana-di-sound` is added."
msgstr ":ref:`normalizer-nfkc130-unify-katakana-di-sound` is added."

msgid ":ref:`normalizer-nfkc130-unify-katakana-wo-sound` is added."
msgstr ":ref:`normalizer-nfkc130-unify-katakana-wo-sound` is added."

msgid ":ref:`normalizer-nfkc130-unify-katakana-zu-small-sounds` is added."
msgstr ":ref:`normalizer-nfkc130-unify-katakana-zu-small-sounds` is added."

msgid ":ref:`normalizer-nfkc130-unify-katakana-du-sound` is added."
msgstr ":ref:`normalizer-nfkc130-unify-katakana-du-sound` is added."

msgid ":ref:`normalizer-nfkc130-unify-katakana-trailing-o` is added."
msgstr ":ref:`normalizer-nfkc130-unify-katakana-trailing-o` is added."

msgid ":ref:`normalizer-nfkc130-unify-katakana-du-small-sounds` is added."
msgstr ":ref:`normalizer-nfkc130-unify-katakana-du-small-sounds` is added."

msgid ":ref:`normalizer-nfkc130-unify-kana-prolonged-sound-mark` is added."
msgstr ":ref:`normalizer-nfkc130-unify-kana-prolonged-sound-mark` is added."

msgid ":ref:`normalizer-nfkc130-unify-kana-hyphen` is added."
msgstr ":ref:`normalizer-nfkc130-unify-kana-hyphen` is added."

msgid "Specify multiple options::"
msgstr "Specify multiple options::"

msgid "``NormalizerNFKC130`` also specify multiple options as above. You can also specify mingle multiple options except above example."
msgstr "``NormalizerNFKC130`` also specify multiple options as above. You can also specify mingle multiple options except above example."

msgid "Usage"
msgstr "Usage"

msgid "Simple usage"
msgstr "Simple usage"

msgid "Here is an example of ``NormalizerNFKC130``. ``NormalizerNFKC130`` normalizes text by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 13.0."
msgstr "Here is an example of ``NormalizerNFKC130``. ``NormalizerNFKC130`` normalizes text by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 13.0."

msgid "Execution example::"
msgstr "Execution example::"

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-kana` option."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-kana` option."

msgid "This option enables that same pronounced characters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."
msgstr "This option enables that same pronounced characters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-kana-case` option."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-kana-case` option."

msgid "This option enables that large and small versions of same letters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."
msgstr "This option enables that large and small versions of same letters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-kana-voiced-sound-mark` option."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-kana-voiced-sound-mark` option."

msgid "This option enables that letters with/without voiced sound mark and semi voiced sound mark in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."
msgstr "This option enables that letters with/without voiced sound mark and semi voiced sound mark in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-hyphen` option. This option enables normalize hyphen to \"-\" (U+002D HYPHEN-MINUS) as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-hyphen` option. This option enables normalize hyphen to \"-\" (U+002D HYPHEN-MINUS) as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-prolonged-sound-mark` option. This option enables normalize prolonged sound to \"-\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK) as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-prolonged-sound-mark` option. This option enables normalize prolonged sound to \"-\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK) as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-hyphen-and-prolonged-sound-mark` option. This option enables normalize hyphen and prolonged sound to \"-\" (U+002D HYPHEN-MINUS) as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-hyphen-and-prolonged-sound-mark` option. This option enables normalize hyphen and prolonged sound to \"-\" (U+002D HYPHEN-MINUS) as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-middle-dot` option. This option enables normalize middle dot to \"·\" (U+00B7 MIDDLE DOT) as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-middle-dot` option. This option enables normalize middle dot to \"·\" (U+00B7 MIDDLE DOT) as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-v-sounds` option. This option enables normalize \"ヴァヴィヴヴェヴォ\" to \"バビブベボ\" as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-v-sounds` option. This option enables normalize \"ヴァヴィヴヴェヴォ\" to \"バビブベボ\" as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-bu-sounds` option. This option enables normalize \"ヴァヴィヴゥヴェヴォ\" to \"ブ\" as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-bu-sounds` option. This option enables normalize \"ヴァヴィヴゥヴェヴォ\" to \"ブ\" as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-to-romaji` option. This option enables normalize hiragana and katakana to romaji as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-to-romaji` option. This option enables normalize hiragana and katakana to romaji as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-remove-symbol` option. This option removes symbols (e.g. #, !, \", &, %, ...) as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-remove-symbol` option. This option removes symbols (e.g. #, !, \", &, %, ...) as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-gu-small-sounds` option. This option enables to normalize \"グァグィグェグォ\" to \"ガギゲゴ\" as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-gu-small-sounds` option. This option enables to normalize \"グァグィグェグォ\" to \"ガギゲゴ\" as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-di-sound` option. This option enables to normalize \"ヂ\" to \"ジ\" as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-di-sound` option. This option enables to normalize \"ヂ\" to \"ジ\" as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-wo-sound` option. This option enables to normalize \"ヲ\" to \"オ\" as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-wo-sound` option. This option enables to normalize \"ヲ\" to \"オ\" as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-zu-small-sounds` option. This option enables to normalize \"ズァズィズェズォ\" to \"ザジゼゾ\" as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-zu-small-sounds` option. This option enables to normalize \"ズァズィズェズォ\" to \"ザジゼゾ\" as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-du-sound` option. This option enables to normalize \"ヅ\" to \"ズ\" as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-du-sound` option. This option enables to normalize \"ヅ\" to \"ズ\" as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-trailing-o` option. This option enables to normalize \"オ\" to \"ウ\" when the vowel in the previous letter is \"オ\" as below."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-trailing-o` option. This option enables to normalize \"オ\" to \"ウ\" when the vowel in the previous letter is \"オ\" as below."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-du-small-sounds` option. This option enables to normalize \"ヅァヅィヅェヅォ\" to \"ザジゼゾ\"."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-katakana-du-small-sounds` option. This option enables to normalize \"ヅァヅィヅェヅォ\" to \"ザジゼゾ\"."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-kana-prolonged-sound-mark` option. This option enables to normalize \"ー\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK) to a vowel of a previous kana letter."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-kana-prolonged-sound-mark` option. This option enables to normalize \"ー\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK) to a vowel of a previous kana letter."

msgid "If a previous kana letter is \"ん\" , \"ー\" is normalized to \"ん\", And a previous kana letter is \"ン\" , \"ー\" is normalized to \"ン\"."
msgstr "If a previous kana letter is \"ん\" , \"ー\" is normalized to \"ん\", And a previous kana letter is \"ン\" , \"ー\" is normalized to \"ン\"."

msgid "Here is an example of :ref:`normalizer-nfkc130-unify-kana-hyphen` option. This option enables to normalize \"-\" (U+002D HYPHEN-MINUS) to a vowel of a previous kana letter."
msgstr "Here is an example of :ref:`normalizer-nfkc130-unify-kana-hyphen` option. This option enables to normalize \"-\" (U+002D HYPHEN-MINUS) to a vowel of a previous kana letter."

msgid "If a previous kana letter is \"ん\" , \"-\" is normalized to \"ん\", And a previous kana letter is \"ン\" , \"-\" is normalized to \"ン\"."
msgstr "If a previous kana letter is \"ん\" , \"-\" is normalized to \"ん\", And a previous kana letter is \"ン\" , \"-\" is normalized to \"ン\"."

msgid "Advanced usage"
msgstr "Advanced usage"

msgid "You can output romaji of specific a part of speech with using to combine ``TokenMecab`` and ``NormalizerNFKC130`` as below."
msgstr "You can output romaji of specific a part of speech with using to combine ``TokenMecab`` and ``NormalizerNFKC130`` as below."

msgid "First of all, you extract reading of a noun with excluding non-independent word and suffix of person name with ``target_class`` option and ``include_reading`` option."
msgstr "First of all, you extract reading of a noun with excluding non-independent word and suffix of person name with ``target_class`` option and ``include_reading`` option."

msgid "Next, you normalize reading of the noun that extracted with ``unify_to_romaji`` option of ``NormalizerNFKC130``."
msgstr "Next, you normalize reading of the noun that extracted with ``unify_to_romaji`` option of ``NormalizerNFKC130``."

msgid "Parameters"
msgstr "Parameters"

msgid "Optional parameter"
msgstr "Optional parameter"

msgid "There are optional parameters as below."
msgstr "There are optional parameters as below."

msgid "``unify_kana``"
msgstr "``unify_kana``"

msgid "This option enables that same pronounced characters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."
msgstr "This option enables that same pronounced characters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."

msgid "``unify_kana_case``"
msgstr "``unify_kana_case``"

msgid "This option enables that large and small versions of same letters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."
msgstr "This option enables that large and small versions of same letters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."

msgid "``unify_kana_voiced_sound_mark``"
msgstr "``unify_kana_voiced_sound_mark``"

msgid "This option enables that letters with/without voiced sound mark and semi voiced sound mark in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."
msgstr "This option enables that letters with/without voiced sound mark and semi voiced sound mark in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."

msgid "``unify_hyphen``"
msgstr "``unify_hyphen``"

msgid "This option enables normalize hyphen to \"-\" (U+002D HYPHEN-MINUS)."
msgstr "This option enables normalize hyphen to \"-\" (U+002D HYPHEN-MINUS)."

msgid "Hyphen of the target of normalizing is as below."
msgstr "Hyphen of the target of normalizing is as below."

msgid "\"-\" (U+002D HYPHEN-MINUS)"
msgstr "\"-\" (U+002D HYPHEN-MINUS)"

msgid "\"֊\" (U+058A ARMENIAN HYPHEN)"
msgstr "\"֊\" (U+058A ARMENIAN HYPHEN)"

msgid "\"˗\" (U+02D7 MODIFIER LETTER MINUS SIGN)"
msgstr "\"˗\" (U+02D7 MODIFIER LETTER MINUS SIGN)"

msgid "\"‐\" (U+2010 HYPHEN)"
msgstr "\"‐\" (U+2010 HYPHEN)"

msgid "\"—\" (U+2014 EM DASH)"
msgstr "\"—\" (U+2014 EM DASH)"

msgid "\"⁃\" (U+2043 HYPHEN BULLET)"
msgstr "\"⁃\" (U+2043 HYPHEN BULLET)"

msgid "\"⁻\" (U+207B SUPERSCRIPT MINUS)"
msgstr "\"⁻\" (U+207B SUPERSCRIPT MINUS)"

msgid "\"₋\" (U+208B SUBSCRIPT MINUS)"
msgstr "\"₋\" (U+208B SUBSCRIPT MINUS)"

msgid "\"−\" (U+2212 MINUS SIGN)"
msgstr "\"−\" (U+2212 MINUS SIGN)"

msgid "``unify_prolonged_sound_mark``"
msgstr "``unify_prolonged_sound_mark``"

msgid "This option enables normalize prolonged sound to \"-\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK)."
msgstr "This option enables normalize prolonged sound to \"-\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK)."

msgid "Prolonged sound of the target of normalizing is as below."
msgstr "Prolonged sound of the target of normalizing is as below."

msgid "\"―\" (U+2015 HORIZONTAL BAR)"
msgstr "\"―\" (U+2015 HORIZONTAL BAR)"

msgid "\"─\" (U+2500 BOX DRAWINGS LIGHT HORIZONTAL)"
msgstr "\"─\" (U+2500 BOX DRAWINGS LIGHT HORIZONTAL)"

msgid "\"━\" (U+2501 BOX DRAWINGS HEAVY HORIZONTAL)"
msgstr "\"━\" (U+2501 BOX DRAWINGS HEAVY HORIZONTAL)"

msgid "\"ー\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK)"
msgstr "\"ー\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK)"

msgid "\"ｰ\" (U+FF70 HALFWIDTH KATAKANA-HIRAGANA PROLONGED SOUND MARK)"
msgstr "\"ｰ\" (U+FF70 HALFWIDTH KATAKANA-HIRAGANA PROLONGED SOUND MARK)"

msgid "``unify_hyphen_and_prolonged_sound_mark``"
msgstr "``unify_hyphen_and_prolonged_sound_mark``"

msgid "This option enables normalize hyphen and prolonged sound to \"-\" (U+002D HYPHEN-MINUS)."
msgstr "This option enables normalize hyphen and prolonged sound to \"-\" (U+002D HYPHEN-MINUS)."

msgid "Hyphen and prolonged sound of the target normalizing is below."
msgstr "Hyphen and prolonged sound of the target normalizing is below."

msgid "``unify_middle_dot``"
msgstr "``unify_middle_dot``"

msgid "This option enables normalize middle dot to \"·\" (U+00B7 MIDDLE DOT)."
msgstr "This option enables normalize middle dot to \"·\" (U+00B7 MIDDLE DOT)."

msgid "Middle dot of the target of normalizing is as below."
msgstr "Middle dot of the target of normalizing is as below."

msgid "\"·\" (U+00B7 MIDDLE DOT)"
msgstr "\"·\" (U+00B7 MIDDLE DOT)"

msgid "\"ᐧ\" (U+1427 CANADIAN SYLLABICS FINAL MIDDLE DOT)"
msgstr "\"ᐧ\" (U+1427 CANADIAN SYLLABICS FINAL MIDDLE DOT)"

msgid "\"•\" (U+2022 BULLET)"
msgstr "\"•\" (U+2022 BULLET)"

msgid "\"∙\" (U+2219 BULLET OPERATOR)"
msgstr "\"∙\" (U+2219 BULLET OPERATOR)"

msgid "\"⋅\" (U+22C5 DOT OPERATOR)"
msgstr "\"⋅\" (U+22C5 DOT OPERATOR)"

msgid "\"⸱\" (U+2E31 WORD SEPARATOR MIDDLE DOT)"
msgstr "\"⸱\" (U+2E31 WORD SEPARATOR MIDDLE DOT)"

msgid "\"・\" (U+30FB KATAKANA MIDDLE DOT)"
msgstr "\"・\" (U+30FB KATAKANA MIDDLE DOT)"

msgid "\"･\" (U+FF65 HALFWIDTH KATAKANA MIDDLE DOT)"
msgstr "\"･\" (U+FF65 HALFWIDTH KATAKANA MIDDLE DOT)"

msgid "``unify_katakana_v_sounds``"
msgstr "``unify_katakana_v_sounds``"

msgid "This option enables normalize \"ヴァヴィヴヴェヴォ\" to \"バビブベボ\"."
msgstr "This option enables normalize \"ヴァヴィヴヴェヴォ\" to \"バビブベボ\"."

msgid "``unify_katakana_bu_sound``"
msgstr "``unify_katakana_bu_sound``"

msgid "This option enables normalize \"ヴァヴィヴゥヴェヴォ\" to \"ブ\"."
msgstr "This option enables normalize \"ヴァヴィヴゥヴェヴォ\" to \"ブ\"."

msgid "``unify_to_romaji``"
msgstr "``unify_to_romaji``"

msgid "This option enables normalize hiragana and katakana to romaji."
msgstr "This option enables normalize hiragana and katakana to romaji."

msgid "``remove_symbol``"
msgstr "``remove_symbol``"

msgid "This option removes symbols (e.g. #, !, \", &, %, ...) from the string that the target of normalizing."
msgstr "This option removes symbols (e.g. #, !, \", &, %, ...) from the string that the target of normalizing."

msgid "``unify_katakana_gu_small_sounds``"
msgstr "``unify_katakana_gu_small_sounds``"

msgid "This option enables to normalize \"グァグィグェグォ\" to \"ガギゲゴ\"."
msgstr "This option enables to normalize \"グァグィグェグォ\" to \"ガギゲゴ\"."

msgid "``unify_katakana_di_sound``"
msgstr "``unify_katakana_di_sound``"

msgid "This option enables to normalize \"ヂ\" to \"ジ\"."
msgstr "This option enables to normalize \"ヂ\" to \"ジ\"."

msgid "``unify_katakana_wo_sound``"
msgstr "``unify_katakana_wo_sound``"

msgid "This option enables to normalize \"ヲ\" to \"オ\"."
msgstr "This option enables to normalize \"ヲ\" to \"オ\"."

msgid "``unify_katakana_zu_small_sounds``"
msgstr "``unify_katakana_zu_small_sounds``"

msgid "This option enables to normalize \"ズァズィズェズォ\" to \"ザジゼゾ\"."
msgstr "This option enables to normalize \"ズァズィズェズォ\" to \"ザジゼゾ\"."

msgid "``unify_katakana_du_sound``"
msgstr "``unify_katakana_du_sound``"

msgid "This option enables to normalize \"ヅ\" to \"ズ\"."
msgstr "This option enables to normalize \"ヅ\" to \"ズ\"."

msgid "``unify_katakana_trailing_o``"
msgstr "``unify_katakana_trailing_o``"

msgid "This option enables to normalize \"オ\" to \"ウ\" when the vowel in the previous letter is \"オ\"."
msgstr "This option enables to normalize \"オ\" to \"ウ\" when the vowel in the previous letter is \"オ\"."

msgid "\"ォオ\" -> \"ォウ\""
msgstr "\"ォオ\" -> \"ォウ\""

msgid "\"オオ\" -> \"オウ\""
msgstr "\"オオ\" -> \"オウ\""

msgid "\"コオ\" -> \"コウ\""
msgstr "\"コオ\" -> \"コウ\""

msgid "\"ソオ\" -> \"ソウ\""
msgstr "\"ソオ\" -> \"ソウ\""

msgid "\"トオ\" -> \"トウ\""
msgstr "\"トオ\" -> \"トウ\""

msgid "\"ノオ\" -> \"ノウ\""
msgstr "\"ノオ\" -> \"ノウ\""

msgid "\"ホオ\" -> \"ホウ\""
msgstr "\"ホオ\" -> \"ホウ\""

msgid "\"モオ\" -> \"モウ\""
msgstr "\"モオ\" -> \"モウ\""

msgid "\"ョオ\" -> \"ョオ\""
msgstr "\"ョオ\" -> \"ョオ\""

msgid "\"ヨオ\" -> \"ヨウ\""
msgstr "\"ヨオ\" -> \"ヨウ\""

msgid "\"ロオ\" -> \"ロウ\""
msgstr "\"ロオ\" -> \"ロウ\""

msgid "\"ゴオ\" -> \"ゴウ\""
msgstr "\"ゴオ\" -> \"ゴウ\""

msgid "\"ゾオ\" -> \"ゾウ\""
msgstr "\"ゾオ\" -> \"ゾウ\""

msgid "\"ドオ\" -> \"ドウ\""
msgstr "\"ドオ\" -> \"ドウ\""

msgid "\"ボオ\" -> \"ボウ\""
msgstr "\"ボオ\" -> \"ボウ\""

msgid "\"ポオ\" -> \"ポウ\""
msgstr "\"ポオ\" -> \"ポウ\""

msgid "\"ヺオ\" -> \"ヺウ\""
msgstr "\"ヺオ\" -> \"ヺウ\""

msgid "``unify_katakana_du_small_sounds``"
msgstr "``unify_katakana_du_small_sounds``"

msgid "This option enables to normalize \"ヅァヅィヅェヅォ\" to \"ザジゼゾ\"."
msgstr "This option enables to normalize \"ヅァヅィヅェヅォ\" to \"ザジゼゾ\"."

msgid "``unify_kana_prolonged_sound_mark``"
msgstr "``unify_kana_prolonged_sound_mark``"

msgid "This option enables to normalize \"ー\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK) to a vowel of a previous kana letter."
msgstr "This option enables to normalize \"ー\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK) to a vowel of a previous kana letter."

msgid "``unify_kana_hyphen``"
msgstr "``unify_kana_hyphen``"

msgid "This option enables to normalize \"-\" (U+002D HYPHEN-MINUS) to a vowel of a previous kana letter."
msgstr "This option enables to normalize \"-\" (U+002D HYPHEN-MINUS) to a vowel of a previous kana letter."

msgid "See also"
msgstr "See also"

msgid ":doc:`../commands/normalize`"
msgstr ":doc:`../commands/normalize`"
