��          �      �       0     1  =   E  E   �  g   �     1     9     @  %   F     l  J   �  '   �  �   �  1  �     �  =   �  E     g   ]     �     �     �  %   �        J     '   `  �   �                                	      
                    Execution example:: Here is an example that uses ``NormalizerNFKC51`` normalizer: It is called ``NormalizerNFKC51``, but it is for Unicode version 5.0. Normally you don't need to use ``NormalizerNFKC51`` explicitly. You can use ``NormalizerAuto`` instead. Summary Syntax Usage Use :doc:`./normalizer_nfkc` instead. ``NormalizerNFKC51`` ``NormalizerNFKC51`` and ``NormalizerNFKC("version", "5.0.0")`` are equal. ``NormalizerNFKC51`` hasn't parameter:: ``NormalizerNFKC51`` normalizes texts by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 5.0. It supports only UTF-8 encoding. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here is an example that uses ``NormalizerNFKC51`` normalizer: It is called ``NormalizerNFKC51``, but it is for Unicode version 5.0. Normally you don't need to use ``NormalizerNFKC51`` explicitly. You can use ``NormalizerAuto`` instead. Summary Syntax Usage Use :doc:`./normalizer_nfkc` instead. ``NormalizerNFKC51`` ``NormalizerNFKC51`` and ``NormalizerNFKC("version", "5.0.0")`` are equal. ``NormalizerNFKC51`` hasn't parameter:: ``NormalizerNFKC51`` normalizes texts by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 5.0. It supports only UTF-8 encoding. 