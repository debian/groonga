��    	      d      �       �   �   �   P   h  <   �     �               3  %   Q  1  w  �   �  P   0  <   �     �     �     �     �  %            	                                     For example, if we executed ``plugin_register normalizers/mysql`` on Groonga when we didn't install `` groonga-normalizer-mysql`` yet. If we registered the plugin that we didn't install yet with ``plugin_register``. If we used ``TokenMecab`` when ``mecab`` didn't install yet. Major action on this error Major cause We install ``mecab``. We install the target plugin. ``-3: GRN_NO_SUCH_FILE_OR_DIRECTORY`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 For example, if we executed ``plugin_register normalizers/mysql`` on Groonga when we didn't install `` groonga-normalizer-mysql`` yet. If we registered the plugin that we didn't install yet with ``plugin_register``. If we used ``TokenMecab`` when ``mecab`` didn't install yet. Major action on this error Major cause We install ``mecab``. We install the target plugin. ``-3: GRN_NO_SUCH_FILE_OR_DIRECTORY`` 