��          L      |       �   q   �           6  V   B     �  1  �  q   �     [     v  V   �     �                                         If we set invalid arguments or invalid options into functions or commands or tokenizers or normalizers, or so on. Major action on this error Major cause We confirm correct arguments or correct options with the official document of Groonga. ``-22: GRN_INVALID_ARGUMENT`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 If we set invalid arguments or invalid options into functions or commands or tokenizers or normalizers, or so on. Major action on this error Major cause We confirm correct arguments or correct options with the official document of Groonga. ``-22: GRN_INVALID_ARGUMENT`` 