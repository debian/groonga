��    
      l      �       �   +   �   N     \   l     �     �  �   �  F   q  6   �  #   �  1    +   E  N   q  \   �          8  �   D  F   �  6     #   C     
         	                               (Windows only) If network access is denied. If the number of threads is greater than 1 when we execute ``database_unmap``. If the table that is a target of ``table_remove`` has reference from other table and column. Major action on this error Major cause We confirm the status network(e.g. confirm whether the server process starting, confirm the setting of the firewall, and so on). We make the number of threads one by executing ``thread_limit max=1``. We remove tables and columns of the reference sources. ``-2: GRN_OPERATION_NOT_PERMITTED`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 (Windows only) If network access is denied. If the number of threads is greater than 1 when we execute ``database_unmap``. If the table that is a target of ``table_remove`` has reference from other table and column. Major action on this error Major cause We confirm the status network(e.g. confirm whether the server process starting, confirm the setting of the firewall, and so on). We make the number of threads one by executing ``thread_limit max=1``. We remove tables and columns of the reference sources. ``-2: GRN_OPERATION_NOT_PERMITTED`` 