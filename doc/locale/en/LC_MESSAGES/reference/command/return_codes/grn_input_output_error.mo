��          T      �       �      �      �   6   �   K     *   c     �  1  �     �     �  6     K   =  *   �     �                                        Major action on this error Major cause This error causes when Groonga can't write to storage. We confirm that the other process can write files correctly to the storage. We confirm the storage capacity is enough. ``-6: GRN_INPUT_OUTPUT_ERROR`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Major action on this error Major cause This error causes when Groonga can't write to storage. We confirm that the other process can write files correctly to the storage. We confirm the storage capacity is enough. ``-6: GRN_INPUT_OUTPUT_ERROR`` 