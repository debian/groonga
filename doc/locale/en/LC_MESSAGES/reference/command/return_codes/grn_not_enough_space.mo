��    	      d      �       �   .   �   Q        b     }  M   �  S   �     +  C   I  1  �  .   �  Q   �     @     [  M   g  S   �     	  C   '                                       	       If the space to store the index is not enough. If the total key size is over the limit of a hash table or a patricia trie table. Major action on this error Major cause We reduce the number of records of the target table by such as a split table. We reduce the number of records that set the target index by such as a split table. ``-13: GRN_NOT_ENOUGH_SPACE`` please see :doc:`/limitations` for the detail of these limitations. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 If the space to store the index is not enough. If the total key size is over the limit of a hash table or a patricia trie table. Major action on this error Major cause We reduce the number of records of the target table by such as a split table. We reduce the number of records that set the target index by such as a split table. ``-13: GRN_NOT_ENOUGH_SPACE`` please see :doc:`/limitations` for the detail of these limitations. 