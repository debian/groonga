��          �      �       0  )   1  C   [  n   �  7        F  
   b  x   m     �     �  �   �  O   ~  "   �  1  �  )   #  C   M  n   �  7         8  
   T  x   _     �     �  �   �  O   p  "   �               
       	                                    :doc:`/reference/commands/request_cancel` A request ID is a string. The maximum request ID size is 4096 byte. All commands accept ``request_id`` parameter. You can assign ID to request by adding ``request_id`` parameter. Here is an example to assign ``id-1`` ID to a request:: How to assign ID to request Request ID Request ID should be managed by user. If you assign the same ID for some running requests, you can't cancel the request. See also Summary The ID can be used by canceling the request. See also :doc:`/reference/commands/request_cancel` for details about canceling a request. The simplest ID sequence is incremented numbers such as ``1``, ``2`` , ``...``. You can assign ID to each request. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/commands/request_cancel` A request ID is a string. The maximum request ID size is 4096 byte. All commands accept ``request_id`` parameter. You can assign ID to request by adding ``request_id`` parameter. Here is an example to assign ``id-1`` ID to a request:: How to assign ID to request Request ID Request ID should be managed by user. If you assign the same ID for some running requests, you can't cancel the request. See also Summary The ID can be used by canceling the request. See also :doc:`/reference/commands/request_cancel` for details about canceling a request. The simplest ID sequence is incremented numbers such as ``1``, ``2`` , ``...``. You can assign ID to each request. 