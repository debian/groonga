��          T      �       �   L   �   6     5   =     s     �     �  1  �  L   �  6     5   D     z     �     �                                        Groonga supports pretty print when you choose JSON for :doc:`output_format`. Here is a result without ``output_pretty`` parameter:: Just specify ``yes`` to ``output_pretty`` parameter:: Pretty print Summary Usage Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Groonga supports pretty print when you choose JSON for :doc:`output_format`. Here is a result without ``output_pretty`` parameter:: Just specify ``yes`` to ``output_pretty`` parameter:: Pretty print Summary Usage 