��          �      L      �     �  B   �       
   +  ,   6     c     k  m   r  +   �       ^     ~   q     �  }     0   �  [   �  8        L  1  X     �  B   �     �  
   �  ,   �     ,     4  m   ;  +   �     �  ^   �  ~   :     �  }   �  0   O	  [   �	  8   �	     
     
                                                         	                                      Execution example:: Here is an example that uses ``TokenFilterStopWord`` token filter: Optional parameter Parameters Specify a column that specified a stop word. Summary Syntax The stop word is specified ``is_stop_word`` column on lexicon table when you don't specify ``column`` option. There is a optional parameters ``columns``. Usage You can specify stop word in column except ``is_stop_columns`` by ``columns`` option as below. ``"Hello"`` that doesn't have ``and`` in content is matched. Because ``and`` is a stop word and ``and`` is removed from query. ``TokenFilterStopWord`` ``TokenFilterStopWord`` can specify stop word after adding the documents because it removes token in searching the documents. ``TokenFilterStopWord`` has optional parameter:: ``TokenFilterStopWord`` removes stop words from tokenized token in searching the documents. ``and`` token is marked as stop word in ``Terms`` table. ``columns`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Execution example:: Here is an example that uses ``TokenFilterStopWord`` token filter: Optional parameter Parameters Specify a column that specified a stop word. Summary Syntax The stop word is specified ``is_stop_word`` column on lexicon table when you don't specify ``column`` option. There is a optional parameters ``columns``. Usage You can specify stop word in column except ``is_stop_columns`` by ``columns`` option as below. ``"Hello"`` that doesn't have ``and`` in content is matched. Because ``and`` is a stop word and ``and`` is removed from query. ``TokenFilterStopWord`` ``TokenFilterStopWord`` can specify stop word after adding the documents because it removes token in searching the documents. ``TokenFilterStopWord`` has optional parameter:: ``TokenFilterStopWord`` removes stop words from tokenized token in searching the documents. ``and`` token is marked as stop word in ``Terms`` table. ``columns`` 