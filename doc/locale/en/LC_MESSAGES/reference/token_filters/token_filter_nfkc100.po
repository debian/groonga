# -*- po -*-
# English translations for Groonga package.
# Copyright (C) 2009-2025 Groonga Project
# This file is distributed under the same license as the Groonga package.
# Automatically generated, 2025.
#
msgid ""
msgstr ""
"Project-Id-Version: Groonga 15.0.2\n"
"Report-Msgid-Bugs-To: \n"
"PO-Revision-Date: 2025-02-21 09:46+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "``TokenFilterNFKC100``"
msgstr "``TokenFilterNFKC100``"

msgid "Use :doc:`./token_filter_nfkc` instead."
msgstr "Use :doc:`./token_filter_nfkc` instead."

msgid "``TokenFilterNFKC100`` and ``TokenFilterNFKC(\"version\", \"10.0.0\")`` are equal."
msgstr "``TokenFilterNFKC100`` and ``TokenFilterNFKC(\"version\", \"10.0.0\")`` are equal."

msgid "Summary"
msgstr "Summary"

msgid "This token filter can use the same option by :ref:`normalizer-nfkc100`. This token filter is used to normalize after tokenizing. Because, if you normalize before tokenizing with ``TokenMecab`` , the meaning of a token may be lost."
msgstr "This token filter can use the same option by :ref:`normalizer-nfkc100`. This token filter is used to normalize after tokenizing. Because, if you normalize before tokenizing with ``TokenMecab`` , the meaning of a token may be lost."

msgid "Syntax"
msgstr "Syntax"

msgid "``TokenFilterNFKC100`` has optional parameter."
msgstr "``TokenFilterNFKC100`` has optional parameter."

msgid "No options::"
msgstr "No options::"

msgid "``TokenFilterNFKC100`` normalizes text by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 10.0."
msgstr "``TokenFilterNFKC100`` normalizes text by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 10.0."

msgid "Specify option::"
msgstr "Specify option::"

msgid "Usage"
msgstr "Usage"

msgid "Simple usage"
msgstr "Simple usage"

msgid "Here is an example of ``TokenFilterNFKC100``. ``TokenFilterNFKC100`` normalizes text by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 10.0."
msgstr "Here is an example of ``TokenFilterNFKC100``. ``TokenFilterNFKC100`` normalizes text by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 10.0."

msgid "Execution example::"
msgstr "Execution example::"

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-kana` option."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-kana` option."

msgid "This option enables that same pronounced characters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."
msgstr "This option enables that same pronounced characters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-kana-case` option."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-kana-case` option."

msgid "This option enables that large and small versions of same letters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."
msgstr "This option enables that large and small versions of same letters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-kana-voiced-sound-mark` option."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-kana-voiced-sound-mark` option."

msgid "This option enables that letters with/without voiced sound mark and semi voiced sound mark in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."
msgstr "This option enables that letters with/without voiced sound mark and semi voiced sound mark in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character as below."

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-hyphen` option. This option enables normalize hyphen to \"-\" (U+002D HYPHEN-MINUS) as below."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-hyphen` option. This option enables normalize hyphen to \"-\" (U+002D HYPHEN-MINUS) as below."

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-prolonged-sound-mark` option. This option enables normalize prolonged sound to \"-\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK) as below."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-prolonged-sound-mark` option. This option enables normalize prolonged sound to \"-\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK) as below."

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-hyphen-and-prolonged-sound-mark` option. This option enables normalize hyphen and prolonged sound to \"-\" (U+002D HYPHEN-MINUS) as below."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-hyphen-and-prolonged-sound-mark` option. This option enables normalize hyphen and prolonged sound to \"-\" (U+002D HYPHEN-MINUS) as below."

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-middle-dot` option. This option enables normalize middle dot to \"·\" (U+00B7 MIDDLE DOT) as below."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-middle-dot` option. This option enables normalize middle dot to \"·\" (U+00B7 MIDDLE DOT) as below."

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-katakana-v-sounds` option. This option enables normalize \"ヴァヴィヴヴェヴォ\" to \"バビブベボ\" as below."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-katakana-v-sounds` option. This option enables normalize \"ヴァヴィヴヴェヴォ\" to \"バビブベボ\" as below."

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-katakana-bu-sounds` option. This option enables normalize \"ヴァヴィヴゥヴェヴォ\" to \"ブ\" as below."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-katakana-bu-sounds` option. This option enables normalize \"ヴァヴィヴゥヴェヴォ\" to \"ブ\" as below."

msgid "Here is an example of :ref:`token-filter-nfkc100-unify-to-romaji` option. This option enables normalize hiragana and katakana to romaji as below."
msgstr "Here is an example of :ref:`token-filter-nfkc100-unify-to-romaji` option. This option enables normalize hiragana and katakana to romaji as below."

msgid "Advanced usage"
msgstr "Advanced usage"

msgid "You can output all input string as hiragana with cimbining ``TokenFilterNFKC100`` with ``use_reading`` option of ``TokenMecab`` as below."
msgstr "You can output all input string as hiragana with cimbining ``TokenFilterNFKC100`` with ``use_reading`` option of ``TokenMecab`` as below."

msgid "Parameters"
msgstr "Parameters"

msgid "Optional parameter"
msgstr "Optional parameter"

msgid "There are optional parameters as below."
msgstr "There are optional parameters as below."

msgid "``unify_kana``"
msgstr "``unify_kana``"

msgid "This option enables that same pronounced characters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."
msgstr "This option enables that same pronounced characters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."

msgid "``unify_kana_case``"
msgstr "``unify_kana_case``"

msgid "This option enables that large and small versions of same letters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."
msgstr "This option enables that large and small versions of same letters in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."

msgid "``unify_kana_voiced_sound_mark``"
msgstr "``unify_kana_voiced_sound_mark``"

msgid "This option enables that letters with/without voiced sound mark and semi voiced sound mark in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."
msgstr "This option enables that letters with/without voiced sound mark and semi voiced sound mark in all of full-width Hiragana, full-width Katakana and half-width Katakana are regarded as the same character."

msgid "``unify_hyphen``"
msgstr "``unify_hyphen``"

msgid "This option enables normalize hyphen to \"-\" (U+002D HYPHEN-MINUS)."
msgstr "This option enables normalize hyphen to \"-\" (U+002D HYPHEN-MINUS)."

msgid "Hyphen of the target of normalizing is as below."
msgstr "Hyphen of the target of normalizing is as below."

msgid "\"-\" (U+002D HYPHEN-MINUS)"
msgstr "\"-\" (U+002D HYPHEN-MINUS)"

msgid "\"֊\" (U+058A ARMENIAN HYPHEN)"
msgstr "\"֊\" (U+058A ARMENIAN HYPHEN)"

msgid "\"˗\" (U+02D7 MODIFIER LETTER MINUS SIGN)"
msgstr "\"˗\" (U+02D7 MODIFIER LETTER MINUS SIGN)"

msgid "\"‐\" (U+2010 HYPHEN)"
msgstr "\"‐\" (U+2010 HYPHEN)"

msgid "\"—\" (U+2014 EM DASH)"
msgstr "\"—\" (U+2014 EM DASH)"

msgid "\"⁃\" (U+2043 HYPHEN BULLET)"
msgstr "\"⁃\" (U+2043 HYPHEN BULLET)"

msgid "\"⁻\" (U+207B SUPERSCRIPT MINUS)"
msgstr "\"⁻\" (U+207B SUPERSCRIPT MINUS)"

msgid "\"₋\" (U+208B SUBSCRIPT MINUS)"
msgstr "\"₋\" (U+208B SUBSCRIPT MINUS)"

msgid "\"−\" (U+2212 MINUS SIGN)"
msgstr "\"−\" (U+2212 MINUS SIGN)"

msgid "``unify_prolonged_sound_mark``"
msgstr "``unify_prolonged_sound_mark``"

msgid "This option enables normalize prolonged sound to \"-\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK)."
msgstr "This option enables normalize prolonged sound to \"-\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK)."

msgid "Prolonged sound of the target of normalizing is as below."
msgstr "Prolonged sound of the target of normalizing is as below."

msgid "\"―\" (U+2015 HORIZONTAL BAR)"
msgstr "\"―\" (U+2015 HORIZONTAL BAR)"

msgid "\"─\" (U+2500 BOX DRAWINGS LIGHT HORIZONTAL)"
msgstr "\"─\" (U+2500 BOX DRAWINGS LIGHT HORIZONTAL)"

msgid "\"━\" (U+2501 BOX DRAWINGS HEAVY HORIZONTAL)"
msgstr "\"━\" (U+2501 BOX DRAWINGS HEAVY HORIZONTAL)"

msgid "\"ー\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK)"
msgstr "\"ー\" (U+30FC KATAKANA-HIRAGANA PROLONGED SOUND MARK)"

msgid "\"ｰ\" (U+FF70 HALFWIDTH KATAKANA-HIRAGANA PROLONGED SOUND MARK)"
msgstr "\"ｰ\" (U+FF70 HALFWIDTH KATAKANA-HIRAGANA PROLONGED SOUND MARK)"

msgid "``unify_hyphen_and_prolonged_sound_mark``"
msgstr "``unify_hyphen_and_prolonged_sound_mark``"

msgid "This option enables normalize hyphen and prolonged sound to \"-\" (U+002D HYPHEN-MINUS)."
msgstr "This option enables normalize hyphen and prolonged sound to \"-\" (U+002D HYPHEN-MINUS)."

msgid "Hyphen and prolonged sound of the target normalizing is below."
msgstr "Hyphen and prolonged sound of the target normalizing is below."

msgid "``unify_middle_dot``"
msgstr "``unify_middle_dot``"

msgid "This option enables normalize middle dot to \"·\" (U+00B7 MIDDLE DOT)."
msgstr "This option enables normalize middle dot to \"·\" (U+00B7 MIDDLE DOT)."

msgid "Middle dot of the target of normalizing is as below."
msgstr "Middle dot of the target of normalizing is as below."

msgid "\"·\" (U+00B7 MIDDLE DOT)"
msgstr "\"·\" (U+00B7 MIDDLE DOT)"

msgid "\"ᐧ\" (U+1427 CANADIAN SYLLABICS FINAL MIDDLE DOT)"
msgstr "\"ᐧ\" (U+1427 CANADIAN SYLLABICS FINAL MIDDLE DOT)"

msgid "\"•\" (U+2022 BULLET)"
msgstr "\"•\" (U+2022 BULLET)"

msgid "\"∙\" (U+2219 BULLET OPERATOR)"
msgstr "\"∙\" (U+2219 BULLET OPERATOR)"

msgid "\"⋅\" (U+22C5 DOT OPERATOR)"
msgstr "\"⋅\" (U+22C5 DOT OPERATOR)"

msgid "\"⸱\" (U+2E31 WORD SEPARATOR MIDDLE DOT)"
msgstr "\"⸱\" (U+2E31 WORD SEPARATOR MIDDLE DOT)"

msgid "\"・\" (U+30FB KATAKANA MIDDLE DOT)"
msgstr "\"・\" (U+30FB KATAKANA MIDDLE DOT)"

msgid "\"･\" (U+FF65 HALFWIDTH KATAKANA MIDDLE DOT)"
msgstr "\"･\" (U+FF65 HALFWIDTH KATAKANA MIDDLE DOT)"

msgid "``unify_katakana_v_sounds``"
msgstr "``unify_katakana_v_sounds``"

msgid "This option enables normalize \"ヴァヴィヴヴェヴォ\" to \"バビブベボ\"."
msgstr "This option enables normalize \"ヴァヴィヴヴェヴォ\" to \"バビブベボ\"."

msgid "``unify_katakana_bu_sound``"
msgstr "``unify_katakana_bu_sound``"

msgid "This option enables normalize \"ヴァヴィヴゥヴェヴォ\" to \"ブ\"."
msgstr "This option enables normalize \"ヴァヴィヴゥヴェヴォ\" to \"ブ\"."

msgid "``unify_to_romaji``"
msgstr "``unify_to_romaji``"

msgid "This option enables normalize hiragana and katakana to romaji."
msgstr "This option enables normalize hiragana and katakana to romaji."
