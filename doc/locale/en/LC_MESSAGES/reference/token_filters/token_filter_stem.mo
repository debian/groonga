��          �      \      �  �   �     �  $   �  >   �       
        !  H   >     �     �  -   �     �  �   �  T   x  �   �     g  ,   {  *   �     �  1  �  �        �  $   �  >        E  
   X     c  H   �     �     �  -   �     	  �   	  T   �	  �   
     �
  ,   �
  *   �
                    
   	                                                                                 All of ``develop``, ``developing``, ``developed`` and ``develops`` tokens are stemmed as ``develop``. So we can find ``develop``, ``developing`` and ``developed`` by ``develops`` query. Execution example:: Here are support steming algorithm:: Here is an example that uses ``TokenFilterStem`` token filter: Optional parameter Parameters Specify a steming algorithm. Steming algorithm is extract the stem. It is prepared for each language. Summary Syntax There is a optional parameters ``algorithm``. Usage You can extract the stem of each language by changing steming algorithm. For example, if you want extract the stem of the French, you specify French to ``algorithm`` option. You can specify steming algorithm except English with ``algorithm`` option as below. You need to install an additional package to using ``TokenFilterStem``. For more detail of how to installing an additional package, see :doc:`/install` . ``TokenFilterStem`` ``TokenFilterStem`` has optional parameter:: ``TokenFilterStem`` stems tokenized token. ``algorithm`` Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 All of ``develop``, ``developing``, ``developed`` and ``develops`` tokens are stemmed as ``develop``. So we can find ``develop``, ``developing`` and ``developed`` by ``develops`` query. Execution example:: Here are support steming algorithm:: Here is an example that uses ``TokenFilterStem`` token filter: Optional parameter Parameters Specify a steming algorithm. Steming algorithm is extract the stem. It is prepared for each language. Summary Syntax There is a optional parameters ``algorithm``. Usage You can extract the stem of each language by changing steming algorithm. For example, if you want extract the stem of the French, you specify French to ``algorithm`` option. You can specify steming algorithm except English with ``algorithm`` option as below. You need to install an additional package to using ``TokenFilterStem``. For more detail of how to installing an additional package, see :doc:`/install` . ``TokenFilterStem`` ``TokenFilterStem`` has optional parameter:: ``TokenFilterStem`` stems tokenized token. ``algorithm`` 