��    	      d      �       �   l   �   Y   N     �      �  !   �  #   �  0   #  4   T  1  �  l   �  Y   (     �      �  !   �  #   �  0   �  4   .                                 	             Groonga project has :doc:`/community` for discussing about groonga. Please send a mail that describes a bug. Groonga project uses `GitHub issue tracker <https://github.com/groonga/groonga/issues>`_. How to report a bug Report a bug to the mailing list Submit a bug to the issue tracker There are two ways to report a bug: You can use English or Japanese to report a bug. You can use either way It makes no difference to us. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Groonga project has :doc:`/community` for discussing about groonga. Please send a mail that describes a bug. Groonga project uses `GitHub issue tracker <https://github.com/groonga/groonga/issues>`_. How to report a bug Report a bug to the mailing list Submit a bug to the issue tracker There are two ways to report a bug: You can use English or Japanese to report a bug. You can use either way It makes no difference to us. 