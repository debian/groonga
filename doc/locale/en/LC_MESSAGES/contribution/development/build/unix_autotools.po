# -*- po -*-
# English translations for Groonga package.
# Copyright (C) 2009-2025 Groonga Project
# This file is distributed under the same license as the Groonga package.
# Automatically generated, 2025.
#
msgid ""
msgstr ""
"Project-Id-Version: Groonga 15.0.2\n"
"Report-Msgid-Bugs-To: \n"
"PO-Revision-Date: 2025-02-21 09:46+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

msgid "Building with GNU Autotools is deprecated, you should build with CMake: :doc:`/install/cmake`"
msgstr "Building with GNU Autotools is deprecated, you should build with CMake: :doc:`/install/cmake`"

msgid "How to build Groonga at the repository by GNU Autotools"
msgstr "How to build Groonga at the repository by GNU Autotools"

msgid "This document describes how to build Groonga at the repository by GNU Autotools."
msgstr "This document describes how to build Groonga at the repository by GNU Autotools."

msgid "You can't choose this way if you develop Groonga on Windows. If you want to use Windows for developing Groonga, see :doc:`windows_cmake`."
msgstr "You can't choose this way if you develop Groonga on Windows. If you want to use Windows for developing Groonga, see :doc:`windows_cmake`."

msgid "Install depended software"
msgstr "Install depended software"

msgid "TODO"
msgstr "TODO"

msgid "`Autoconf <http://www.gnu.org/software/autoconf/>`_"
msgstr "`Autoconf <http://www.gnu.org/software/autoconf/>`_"

msgid "`Automake <http://www.gnu.org/software/automake/>`_"
msgstr "`Automake <http://www.gnu.org/software/automake/>`_"

msgid "`GNU Libtool <http://www.gnu.org/software/libtool/>`_"
msgstr "`GNU Libtool <http://www.gnu.org/software/libtool/>`_"

msgid "`Ruby <https://www.ruby-lang.org/>`_"
msgstr "`Ruby <https://www.ruby-lang.org/>`_"

msgid "`Git <https://git-scm.com/>`_"
msgstr "`Git <https://git-scm.com/>`_"

msgid "`Cutter <http://cutter.sourceforge.net/>`_"
msgstr "`Cutter <http://cutter.sourceforge.net/>`_"

msgid "..."
msgstr "..."

msgid "Checkout Groonga from the repository"
msgstr "Checkout Groonga from the repository"

msgid "Users use released source archive. But developers must build Groonga at the repository. Because source code in the repository is the latest."
msgstr "Users use released source archive. But developers must build Groonga at the repository. Because source code in the repository is the latest."

msgid "The Groonga repository is hosted on `GitHub <https://github.com/groonga/groonga>`_. Checkout the latest source code from the repository::"
msgstr "The Groonga repository is hosted on `GitHub <https://github.com/groonga/groonga>`_. Checkout the latest source code from the repository::"

msgid "Create ``configure``"
msgstr "Create ``configure``"

msgid "You need to create ``configure``. ``configure`` is included in source archive but not included in the repository."
msgstr "You need to create ``configure``. ``configure`` is included in source archive but not included in the repository."

msgid "``configure`` is a build tool that detects your system and generates build configurations for your environment."
msgstr "``configure`` is a build tool that detects your system and generates build configurations for your environment."

msgid "Run ``autogen.sh`` to create ``configure``::"
msgstr "Run ``autogen.sh`` to create ``configure``::"

msgid "Run ``configure``"
msgstr "Run ``configure``"

msgid "You can custom your build configuration by passing options to ``configure``."
msgstr "You can custom your build configuration by passing options to ``configure``."

msgid "Here are recommended ``configure`` options for developers::"
msgstr "Here are recommended ``configure`` options for developers::"

msgid "Here are descriptions of these options:"
msgstr "Here are descriptions of these options:"

msgid "``--prefix=/tmp/local``"
msgstr "``--prefix=/tmp/local``"

msgid "It specifies that you install your Groonga into temporary directory. You can do \"clean install\" by removing ``/tmp/local`` directory. It'll be useful for debugging install."
msgstr "It specifies that you install your Groonga into temporary directory. You can do \"clean install\" by removing ``/tmp/local`` directory. It'll be useful for debugging install."

msgid "``--enable-debug``"
msgstr "``--enable-debug``"

msgid "It enables debug options for C/C++ compiler. It's useful for debugging on debugger such as GDB and LLDB."
msgstr "It enables debug options for C/C++ compiler. It's useful for debugging on debugger such as GDB and LLDB."

msgid "``--eanble-mruby``"
msgstr "``--eanble-mruby``"

msgid "It enables mruby support. The feature isn't enabled by default but developers should enable the feature."
msgstr "It enables mruby support. The feature isn't enabled by default but developers should enable the feature."

msgid "``--with-ruby``"
msgstr "``--with-ruby``"

msgid "It's needed for ``--enable-mruby`` and running functional tests."
msgstr "It's needed for ``--enable-mruby`` and running functional tests."

msgid "Run ``make``"
msgstr "Run ``make``"

msgid "Now, you can build Groonga."
msgstr "Now, you can build Groonga."

msgid "Here is a recommended ``make`` command line for developers::"
msgstr "Here is a recommended ``make`` command line for developers::"

msgid "``-j8`` decreases build time. It enables parallel build. If you have 8 or more CPU cores, you can increase ``8`` to decreases more build time."
msgstr "``-j8`` decreases build time. It enables parallel build. If you have 8 or more CPU cores, you can increase ``8`` to decreases more build time."

msgid "You can just see only warning and error messages by ``> /dev/null``. Developers shouldn't add new warnings and errors in new commit."
msgstr "You can just see only warning and error messages by ``> /dev/null``. Developers shouldn't add new warnings and errors in new commit."

msgid "See also"
msgstr "See also"

msgid ":doc:`/contribution/development/test`"
msgstr ":doc:`/contribution/development/test`"
