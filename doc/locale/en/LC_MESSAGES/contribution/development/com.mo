��          �      �       0     1  #   Q     u  0   �     �     �     �  "   
  �   -  $   �  =        Z  1  y     �  #   �     �  0        7     N     m  "   �  �   �  $   q  =   �     �                                             
   	          GQTPでのアーキテクチャ Groonga 通信アーキテクチャ comがedgeを作る。 comが外部からの接続を受け付ける。 comは1スレッド。 edgeごとにqueueを持つ。 edgeはctxを含む。 edgeは接続と１対１対応。 msgはcomによって、edgeのqueueにenqueueされる。 edgeがworkerに結びついていないときは、同時に、ctx_newというqueueに、msgをenqueueした対象のedgeをenqueueする。 workerはthreadと１対１対応。 workerは、１つのedgeと結びつくことができる。 workerは上限が個定数。 Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 GQTPでのアーキテクチャ Groonga 通信アーキテクチャ comがedgeを作る。 comが外部からの接続を受け付ける。 comは1スレッド。 edgeごとにqueueを持つ。 edgeはctxを含む。 edgeは接続と１対１対応。 msgはcomによって、edgeのqueueにenqueueされる。 edgeがworkerに結びついていないときは、同時に、ctx_newというqueueに、msgをenqueueした対象のedgeをenqueueする。 workerはthreadと１対１対応。 workerは、１つのedgeと結びつくことができる。 workerは上限が個定数。 