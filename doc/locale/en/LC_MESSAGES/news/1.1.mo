��          �      �       0     1  j   P     �  1   �  }   �  q   |  $   �  e     [   y     �     �     �  1  �       j   ;     �  1   �  }   �  q   g  $   �  e   �  [   d     �     �     �              
                   	                         1.1.0リリース - 2011-02-09 :doc:`/reference/commands/select` の--query内で前方一致検索構文"キーワード*"対応。 #837 Daiki Uenoさん TokenDelimitNullトークナイザーを追加。 groonga.pcにgroonga-suggest-create-datasetのパスが設定されている groonga_suggest_create_dataset変数を追加。 サンプル内のスクリプトに実行属性がつかない問題を修正。 （Daiki Uenoさんが報告） バージョン1.1系のお知らせ プラグイン登録APIの名前を改良。 grn_db_register_by_name() -> grn_plugin_register() #834 ログにタイムスタンプの秒より小さい値が常に0になる問題を修正。 修正 感謝 改良 Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 1.1.0リリース - 2011-02-09 :doc:`/reference/commands/select` の--query内で前方一致検索構文"キーワード*"対応。 #837 Daiki Uenoさん TokenDelimitNullトークナイザーを追加。 groonga.pcにgroonga-suggest-create-datasetのパスが設定されている groonga_suggest_create_dataset変数を追加。 サンプル内のスクリプトに実行属性がつかない問題を修正。 （Daiki Uenoさんが報告） バージョン1.1系のお知らせ プラグイン登録APIの名前を改良。 grn_db_register_by_name() -> grn_plugin_register() #834 ログにタイムスタンプの秒より小さい値が常に0になる問題を修正。 修正 感謝 改良 