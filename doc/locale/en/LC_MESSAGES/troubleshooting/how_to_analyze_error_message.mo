��          �      <      �  \   �  C        R  [   Z  �   �  l   �     �     
     (  {   E  4   �  i   �  /   `  s   �  =     G   B  =   �  1  �  \   �  C   W     �  [   �  �   �  l   �	     D
     S
     q
  {   �
  4   
  i   ?  /   �  s   �  =   M  G   �  =   �                           	                 
                                                         10055 is a Windows socket error code and "no buffer" is a message by Groonga given in SOERR. 10055 is assigned to WSAENOBUFS and its description is as follows:: Example First, grep Groonga source files for "SOERR" that is the name of a macro for socket errors. From the above description, you can narrow down the causes. The possible causes are the lack of memory and too many connections. Finally, determine which one is appropriate for the situation when the error occurred. From the above source code, you can confirm that the error occurred due to accept. Let's dig into the cause. How to analyze How to analyze error messages How to analyze socket errors It is clear that the above error message is associated with the last line because the error message contains only "accept". The error message provides hints for investigation:: The following is an example of an error message reported by Groonga, where xxxxx is an arbitrary number:: The source code around the line is as follows:: Then, extract SOERRs whose argument contains "accept" from the grep output and you will find the following SOERRs:: This section describes how to analyze Groonga error messages. This subsection describes how to analyze socket errors with an example. Windows socket error codes are listed in the following page:: Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 10055 is a Windows socket error code and "no buffer" is a message by Groonga given in SOERR. 10055 is assigned to WSAENOBUFS and its description is as follows:: Example First, grep Groonga source files for "SOERR" that is the name of a macro for socket errors. From the above description, you can narrow down the causes. The possible causes are the lack of memory and too many connections. Finally, determine which one is appropriate for the situation when the error occurred. From the above source code, you can confirm that the error occurred due to accept. Let's dig into the cause. How to analyze How to analyze error messages How to analyze socket errors It is clear that the above error message is associated with the last line because the error message contains only "accept". The error message provides hints for investigation:: The following is an example of an error message reported by Groonga, where xxxxx is an arbitrary number:: The source code around the line is as follows:: Then, extract SOERRs whose argument contains "accept" from the grep output and you will find the following SOERRs:: This section describes how to analyze Groonga error messages. This subsection describes how to analyze socket errors with an example. Windows socket error codes are listed in the following page:: 