��          D      l       �   a   �      �   c   �   �   V  1  '  a   Y     �  c   �  �   &                          As HTTP and memcached binary protocol is matured protocol, you can use existing client libraries. Client Groonga supports the original protocol (:doc:`/spec/gqtp`), the memcached binary protocol and HTTP. There are some client libraries which provides convenient API to connect to Groonga server in some program languages. See `Client libraries <https://groonga.org/related-projects.html#libraries>`_ for details. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 As HTTP and memcached binary protocol is matured protocol, you can use existing client libraries. Client Groonga supports the original protocol (:doc:`/spec/gqtp`), the memcached binary protocol and HTTP. There are some client libraries which provides convenient API to connect to Groonga server in some program languages. See `Client libraries <https://groonga.org/related-projects.html#libraries>`_ for details. 