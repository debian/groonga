��          L      |       �      �   �   �      :  �   T  �   �  1  f     �  �   �     )  �   C  �   �                                         Form:: Groonga supports the memcached binary protocol. The following form shows how to run Groonga as a memcached binary protocol server daemon. Memcached binary protocol The `--protocol` option and its argument specify the protocol of the server. "memcached" specifies to use the memcached binary protocol. You don't need to create a table. When Groonga receives a request, it creates a table automatically. The table name will be `Memcache` . Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Form:: Groonga supports the memcached binary protocol. The following form shows how to run Groonga as a memcached binary protocol server daemon. Memcached binary protocol The `--protocol` option and its argument specify the protocol of the server. "memcached" specifies to use the memcached binary protocol. You don't need to create a table. When Groonga receives a request, it creates a table automatically. The table name will be `Memcache` . 