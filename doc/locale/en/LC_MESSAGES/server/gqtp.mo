��          �      <      �  �   �  4   5     j  q   o  C   �  �   %  .     
   D  =   O  Z   �  4   �  6     C   T  D   �     �  \   �  H   B  1  �  �   �  4   A     v  q   {  C   �  �   1	  .   !
  
   P
  =   [
  Z   �
  4   �
  6   )  C   `  D   �     �  \   �  H   N                     
                                                           	                     :doc:`/reference/executables/groonga` is a GQTP server implementation. You can run a Groonga server by the following command line:: C/C++: Groonga (Groonga can be also used as library) GQTP GQTP is a protocol designed for Groonga. It's a stateful protocol. You can send multiple commands in one session. GQTP is the acronym standing for "Groonga Query Transfer Protocol". GQTP will be faster rather than :doc:`/server/http` when you send many light commands like :doc:`/reference/commands/status`. GQTP will be almost same performance as HTTP when you send heavy commands like :doc:`/reference/commands/select`. Go: `goroo <https://github.com/hhatto/goroo>`_ How to run If you want to use GQTP, you can use the following libraries: It's not a library but you can use :doc:`/reference/executables/groonga` as a GQTP client. PHP: `proonga <https://github.com/Yujiro3/proonga>`_ Python: `poyonga <https://github.com/hhatto/poyonga>`_ Ruby: `groonga-client <https://github.com/ranguba/groonga-client>`_ See :doc:`/reference/executables/groonga` for available ``options``. Summary We recommend that you use HTTP for many cases. Because there are many HTTP client libraries. You can run a Groonga server as a daemon by the following command line:: Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 :doc:`/reference/executables/groonga` is a GQTP server implementation. You can run a Groonga server by the following command line:: C/C++: Groonga (Groonga can be also used as library) GQTP GQTP is a protocol designed for Groonga. It's a stateful protocol. You can send multiple commands in one session. GQTP is the acronym standing for "Groonga Query Transfer Protocol". GQTP will be faster rather than :doc:`/server/http` when you send many light commands like :doc:`/reference/commands/status`. GQTP will be almost same performance as HTTP when you send heavy commands like :doc:`/reference/commands/select`. Go: `goroo <https://github.com/hhatto/goroo>`_ How to run If you want to use GQTP, you can use the following libraries: It's not a library but you can use :doc:`/reference/executables/groonga` as a GQTP client. PHP: `proonga <https://github.com/Yujiro3/proonga>`_ Python: `poyonga <https://github.com/hhatto/poyonga>`_ Ruby: `groonga-client <https://github.com/ranguba/groonga-client>`_ See :doc:`/reference/executables/groonga` for available ``options``. Summary We recommend that you use HTTP for many cases. Because there are many HTTP client libraries. You can run a Groonga server as a daemon by the following command line:: 