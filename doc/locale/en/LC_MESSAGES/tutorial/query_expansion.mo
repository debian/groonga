��          �      �       H  F   I     �  �   �  �   v  �   �  t   �  .   �     (     4     D  g   K  T   �  �     1  �  F   1     x  �   �  �   ^  �   �  t   l	  .   �	     
     
     ,
  g   3
  T   �
  �   �
                             
                                	    Above query returns the record which completely equal to query string. Execution example:: For example, if user searches "theatre" instead of "theater", query expansion enables to return search results of "theatre OR theater". This kind of way reduces search leakages. This is what really user wants. Groonga accepts ``query_expander`` parameter for :doc:`/reference/commands/select` command. It enables you to extend your query string. In this case, it doesn't occur search leakage because it creates synonym table which accepts "theatre" and "theater" as query string. In which cases, query string is replaced to "(theater OR theatre)", thus synonym is considered for full text search. Let's create document table and synonym table. Preparation Query expansion Search Then, let's use prepared synonym table. First, use select command without ``query_expander`` parameter. Then, use ``query_expander`` parameter against ``body`` column of ``Synonym`` table. To use query expansion, you need to create table which stores documents, synonym table which stores query string and replacement string. In synonym table, primary key represents original string, the column of ShortText represents modified string. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Above query returns the record which completely equal to query string. Execution example:: For example, if user searches "theatre" instead of "theater", query expansion enables to return search results of "theatre OR theater". This kind of way reduces search leakages. This is what really user wants. Groonga accepts ``query_expander`` parameter for :doc:`/reference/commands/select` command. It enables you to extend your query string. In this case, it doesn't occur search leakage because it creates synonym table which accepts "theatre" and "theater" as query string. In which cases, query string is replaced to "(theater OR theatre)", thus synonym is considered for full text search. Let's create document table and synonym table. Preparation Query expansion Search Then, let's use prepared synonym table. First, use select command without ``query_expander`` parameter. Then, use ``query_expander`` parameter against ``body`` column of ``Synonym`` table. To use query expansion, you need to create table which stores documents, synonym table which stores query string and replacement string. In synonym table, primary key represents original string, the column of ShortText represents modified string. 