��          t      �         W        i  ^  }  i   �  �   F     E      b     �  �   �  e   )  1  �  W   �       ^  -  i   �  �   �     �	      
     3
  �   P
  e   �
               	                                
             And more, you can do suffix search against primary key by specifying additional option. Execution example:: For example, in order to make this distinction between the original records and automatically added records, add the original column indicating that it is the original record, and add original column is ``true`` to the search condition. For attention, use ``--filter`` option because ``--query`` option is not specify ``Bool`` type value intuitively. Groonga supports to create a table with patricia trie option. By specifying it, You can do prefix search. If you set KEY_WITH_SIS flag, suffix search records also are added when you add the data. So if you search simply, the automatically added records are hit in addition to the original records. In order to search only the original records, you need a plan. Prefix search by primary key Prefix search with patricia trie Suffix search by primary key table_create command which uses TABLE_PAT_KEY and KEY_WITH_SIS for flags option supports prefix search and suffix search by primary key. table_create command which uses TABLE_PAT_KEY for flags option supports prefix search by primary key. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 And more, you can do suffix search against primary key by specifying additional option. Execution example:: For example, in order to make this distinction between the original records and automatically added records, add the original column indicating that it is the original record, and add original column is ``true`` to the search condition. For attention, use ``--filter`` option because ``--query`` option is not specify ``Bool`` type value intuitively. Groonga supports to create a table with patricia trie option. By specifying it, You can do prefix search. If you set KEY_WITH_SIS flag, suffix search records also are added when you add the data. So if you search simply, the automatically added records are hit in addition to the original records. In order to search only the original records, you need a plan. Prefix search by primary key Prefix search with patricia trie Suffix search by primary key table_create command which uses TABLE_PAT_KEY and KEY_WITH_SIS for flags option supports prefix search and suffix search by primary key. table_create command which uses TABLE_PAT_KEY for flags option supports prefix search by primary key. 