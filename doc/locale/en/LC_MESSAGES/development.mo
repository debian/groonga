��          4      L       `      a   �   m   1  0     b  �   n                    Development This section describes about developing with Groonga. You may develop an application that uses Groonga as its database, a library that uses libgroonga, language bindings of libgroonga and so on. Project-Id-Version: Groonga 15.0.2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2025-02-21 09:46+0000
Last-Translator: Automatically generated
Language-Team: none
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: en
Plural-Forms: nplurals=2; plural=(n != 1);
 Development This section describes about developing with Groonga. You may develop an application that uses Groonga as its database, a library that uses libgroonga, language bindings of libgroonga and so on. 