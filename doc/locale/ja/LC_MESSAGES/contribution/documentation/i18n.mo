Þ    9      ä  O   ¬      è  V   é  4   @  >   u  ¿   ´  Æ   t  ²   ;     î            ¨   2     Û  ¸   ð  8   ©	  (   â	  R   
  -   ^
     
     (     D     \     |          ­     È     Þ     ø     
     #     :     X     t  }   y     ÷  '   {  2   £  A   Ö       x   2  Â   «  E   n  +   ´     à     ý  3     3   H     |          ¤  l   º  !  '  Y   I  Z   £  ²   þ  ¤   ±  s   V  O   Ê  ü          9      >   Ú         .  0  /  -   `  "     +   ±  ÿ   Ý  %   Ý  û     D   ÿ  0   D  r   u  5   è  Õ        ô       '   3     [  (   x     ¡  !   Á      ã                 3   (   O   &   x   	         ©   ©   G!  W   ñ!  _   I"  g   ©"  %   #  Ç   7#  4  ÿ#  `   4%  ;   %  2   Ñ%  L   &  H   Q&  :   &     Õ&  +   å&  )   '  ±   ;'  Ë  í'  {   ¹)  ®   5*    ä*  ê   é+  t   Ô,  O   I-     2                  5   #   ,   7      %      9       $             *   3   /          	   1                   )   .         +                                                   -      "      
       6   !                   &       8                (      0   4                     '    .mo files are updated automatically by `make html`. So you don't care about .mo files. :doc:`/community` describes our contact information. After doc/source/\*.txt are updated, we can start translation. After editing ``.edit`` files, you can update ``.po`` files by running ``make`` on ``doc/locale/${LANGUAGE}/LC_MESSAGES``. (Please replace `${LANGUAGE}` with your language code such as 'ja'.) Both of ``.edit`` files and ``.po`` files are `PO files <https://www.gnu.org/software/gettext/manual/html_node/PO-Files.html>`_. ``.edit`` files are for editing and ``.po`` files are for versioning. But we still use Japanese in doc/source/ for now. We need to translate Japanese documentation in doc/source/ into English. We welcome to you help us by translating documentation. Clone Groonga repository. Confirm HTML output. Edit ``.edit`` files First, please fork Groonga repository on GitHub. You just access https://github.com/groonga/groonga and press `Fork` button. Now you can clone your Groonga repository:: Generate HTML files. HTML files are generated in doc/locale/${LANGUAGE}/html/. (Please substitute `${LANGUAGE}` with your language code such as 'ja'.) You can confirm HTML output by your favorite browser:: Here are command lines to add new translation language:: Here are command lines to create patch:: Here are command lines to do the above flow. Following sections describes details. Here are command lines to send pull request:: Here is a list of editors specialized for editing ``.po`` files. You can use these editors to edit ``.edit`` files because they are actually ``.po`` files. Here is a translation flow: How to add new language How to clone Groonga repository How to confirm HTML output How to edit ``.edit`` How to generate HTML files How to install Sphinx How to send ``.po`` files How to send patch How to send pull request How to send your works How to update ``.edit`` files How to update ``.po`` files I18N If you have troubles on the above steps, you can use source files available on https://packages.groonga.org/source/groonga/ . In order to edit ``.edit`` files, there are some tools to edit. ``.edit`` files are just text, so you can use your favorite editor. Install Sphinx, if it is not installed. It is a ``.po`` editor and works on many platform. It is also a ``.po`` editor and is implemented as Eclipse plugin. It is bundled in gettext. Now you can send pull request on GitHub. You just access your repository page on GitHub and press `Pull Request` button. Please archive doc/locale/${LANGUAGE}/LC_MESSAGES/ (Please substitute `${LANGUAGE}` with your language code such as 'ja'.) and send it to us! We extract and merge them to the Groonga repository. Please substitute `${LANGUAGE}` with your language code such as 'ja'. Repeat 3.-7. until you finish to translate. See the :doc:`introduction`. Send your works to us! The above steps are just needed at the first setup. Then you need to configure your cloned repository:: Translation flow Update ``.edit`` files Update ``.po`` files. We can receive your works via pull request on GitHub or E-mail attachment patch or ``.po`` files themselves. We only had documentation in Japanese.  We start to support I18N documentation by gettext based `Sphinx I18N feature`_. We'll use English as base language and translate English into other languages such as Japanese. We'll put all documentations into doc/source/ and process them by Sphinx. You can also generate HTML files for all languages by running `make html` on doc/locale:: You can find 000X-YYY.patch files in the current directory. Please send those files to us! You can generate HTML files with updated ``.po`` files by running `make html` on doc/locale/${LANGUAGE}. (Please substitute `${LANGUAGE}` with your language code such as 'ja'.):: You can update ``.edit`` files by running ``make`` on ``doc/locale/${LANGUAGE}/LC_MESSAGES``. (Please replace `${LANGUAGE}` with your language code such as 'ja'.):: `Codes for the Representation of Names of Languages <http://www.loc.gov/standards/iso639-2/php/English_list.php>`_. `Help.GitHub - Sending pull requests <http://help.github.com/pull-requests/>`_. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-06-28 09:14+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 .moãã¡ã¤ã«ã¯ `make html` ã§èªåçã«æ´æ°ãããã®ã§ã.moãã¡ã¤ã«ã®ãã¨ãæ°ã«ããå¿è¦ã¯ããã¾ããã :doc:`/community` ã«é£çµ¡åã®æå ±ãããã¾ãã doc/source/\*.txtãæ´æ°ããããç¿»è¨³ãå§ãã¾ãã ``.edit`` ãã¡ã¤ã«ãç·¨éããå¾ã ``doc/locale/${LANGUAGE}/LC_MESSAGES`` ãã£ã¬ã¯ããªã§ ``make`` ãå®è¡ããã¨ ``.po`` ãã¡ã¤ã«ãæ´æ°ã§ãã¾ããï¼`${LANGUAGE}` ã¯'ja'ãªã©èªåã®è¨èªã®è¨èªã³ã¼ãã«ç½®ãæãã¦ãã ãããï¼ ``edit`` ãã¡ã¤ã«ã¨ ``po`` ãã¡ã¤ã«ã¯ã©ã¡ãã `PO ãã¡ã¤ã« <https://www.gnu.org/software/gettext/manual/html_node/PO-Files.html>`_ ã§ãã ``.edit`` ãã¡ã¤ã«ã¯ç·¨éç¨ã§ã ``.po`` ãã¡ã¤ã«ã¯ãã¼ã¸ã§ã³ç®¡çç¨ã§ãã ããããä»ã®ã¨ãããdoc/source/ã§ã¯æ¥æ¬èªãä½¿ã£ã¦ãã¾ãããã®ãããã¾ãã¯ãdoc/source/ä»¥ä¸ã«ããæ¥æ¬èªã®ãã­ã¥ã¡ã³ããè±èªã«ç¿»è¨³ããå¿è¦ãããã¾ãããã­ã¥ã¡ã³ããç¿»è¨³ãã¦ãããããéã£ã¦ãããã¨ã¨ã¦ãåã³ã¾ãã Groongaã®ãªãã¸ããªãcloneãã¾ãã HTMLã®åºåãç¢ºèªãã¾ãã ``.edit`` ãã¡ã¤ã«ãç·¨éãã¾ãã ã¯ããã«ãGitHubä¸ã®Groongaãªãã¸ããªãforkãã¦ãã ããã https://github.com/groonga/groonga ã«ã¢ã¯ã»ã¹ãã¦ `Fork` ãã¿ã³ãæ¼ãã ãã§ããããã§èªåã®Groongaãªãã¸ããªãcloneãããã¨ãã§ãã¾ãã:: HTMLãã¡ã¤ã«ãçæãã¾ãã HTMLãã¡ã¤ã«ã¯doc/locale/${LANGUAGE}/html/ä»¥ä¸ã«åºåããã¾ããï¼`${LANGUAGE}` ã¯'ja'ãªã©èªåã®è¨èªã®è¨èªã³ã¼ãã«ç½®ãæãã¦ãã ãããï¼å¥½ããªãã©ã¦ã¶ã§åºåãããHTMLãç¢ºèªãã¦ãã ããã:: æ°ããç¿»è¨³å¯¾è±¡ã®è¨èªãè¿½å ããã³ãã³ãã©ã¤ã³:: ããããä½ãããã®ã³ãã³ãã©ã¤ã³ ä¸è¨ã®æµããå®è¡ããã³ãã³ãã©ã¤ã³ã§ããè©³ç´°ã¯ä»¥éã®ã»ã¯ã·ã§ã³ã§èª¬æãã¾ãã pull requestãéãããã®ã³ãã³ãã©ã¤ã³:: ä»¥ä¸ã¯ ``.edit`` ãã¡ã¤ã«ã®ç·¨éã«ç¹åããã¨ãã£ã¿ã®ãªã¹ãã§ãã``.edit`` ãã¡ã¤ã«ã®å®ä½ã¯ ``.po`` ãã¡ã¤ã«ãªã®ã§ããããã®ã¨ãã£ã¿ãä½¿ããã¨ãã§ãã¾ãã ãããç¿»è¨³ã®æµãã§ã: æ°ããè¨èªã®è¿½å æ¹æ³ Groongaãªãã¸ããªã®cloneã®ä»æ¹ HTMLåºåã®ç¢ºèªã®ä»æ¹ ``.edit`` ãã¡ã¤ã«ã®ç·¨éã®ä»æ¹ HTMLãã¡ã¤ã«ã®çææ¹æ³ Sphinxã®ã¤ã³ã¹ãã¼ã«æ¹æ³ ``.po`` ãã¡ã¤ã«ã®éãæ¹ ãããã®éãæ¹ pull requestã®éãæ¹ ç¿»è¨³ã®ææã®éãæ¹ ``.edit`` ãã¡ã¤ã«ã®æ´æ°ã®ä»æ¹ ``.po`` ãã¡ã¤ã«ã®æ´æ°ã®ä»æ¹ å½éå ä»¥ä¸ã®ä½æ¥­ã§åé¡ããã£ãå ´åã¯ã https://packages.groonga.org/source/groonga/ ã«ããã½ã¼ã¹ãã¡ã¤ã«ãå©ç¨ãã¦ãããã§ãã æ§ããªãã¼ã«ã§ ``.edit`` ãã¡ã¤ã«ãç·¨éã§ãã¾ãã ``.edit`` ãã¡ã¤ã«ã¯åãªããã­ã¹ããªã®ã§å¥½ããªã¨ãã£ã¿ã§ç·¨éã§ãã¾ãã Sphinxãã¤ã³ã¹ãã¼ã«ãã¾ããï¼ã¤ã³ã¹ãã¼ã«ããã¦ããªãå ´åï¼ ``.po`` å°ç¨ã¨ãã£ã¿ã§ããããããã®ãã©ãããã©ã¼ã ã§åä½ãã¾ãã ããã ``.po`` å°ç¨ã¨ãã£ã¿ã§ããEclipseãã©ã°ã¤ã³ã¨ãã¦å®è£ããã¦ãã¾ãã gettextã«åæ¢±ããã¦ãã¾ãã ããã§GitHubä¸ã§pull requestãéãæºåãã§ãã¾ããããã¨ã¯ãGitHubä¸ã®èªåã®ãªãã¸ããªã®ãã¼ã¸ã¸ã¢ã¯ã»ã¹ãã¦ `Pull Request` ãã¿ã³ãæ¼ãã ãã§ãã doc/locale/${LANGUAGE}/LC_MESSAGES/ä»¥ä¸ã.tar.gzã.zipãªã©ã§ã¢ã¼ã«ã¤ãã«ãã¦Groongaãã­ã¸ã§ã¯ãã«éã£ã¦ãã ããï¼ï¼`${LANGUAGE}` ã¯'ja'ãªã©èªåã®è¨èªã®è¨èªã³ã¼ãã«ç½®ãæãã¦ãã ãããï¼ãã¡ãã§ã¢ã¼ã«ã¤ãã®ä¸­ã®åå®¹ããã¼ã¸ãã¾ãã `${LANGUAGE}` ã¯'ja'ãªã©ã®èªåã®è¨èªã®è¨èªã³ã¼ãã«ç½®ãæãã¦ãã ããã ç¿»è¨³ãå®äºããã¾ã§ã3.-7.ãç¹°ãè¿ãã¾ãã :doc:`introduction` ãåç§ãã¦ãã ããã ç¿»è¨³ä½æ¥­ã®ææãGroongaãã­ã¸ã§ã¯ãã«éã£ã¦ãã ããï¼ ãã®ä½æ¥­ã¯ååã»ããã¢ããæã®ã¿ã ãã®ä½æ¥­ã§ãã cloneããå¾ã¯configureããå¿è¦ãããã¾ãã:: ç¿»è¨³ã®æµã ``.edit`` ãã¡ã¤ã«ãæ´æ°ãã¾ãã ``.po`` ãã¡ã¤ã«ãæ´æ°ãã¾ãã ç¿»è¨³ã®ææã¯GitHubã®pull requestãã¡ã¼ã«ã§éã£ã¦ãã ãããã¡ã¼ã«ã§éãå ´åã¯ãããã§ã ``.po`` ãã¡ã¤ã«ãã®ãã®ã§ãæ§ãã¾ããã ä»ã®ã¨ãããGroongaã«ã¯æ¥æ¬èªã§ã®ãã­ã¥ã¡ã³ãããããã¾ããã1.2.2ããgettextãã¼ã¹ã® `Sphinx I18N feature`_ ãä½¿ã£ã¦ãã­ã¥ã¡ã³ãã®å½éåå¯¾å¿ãå§ãã¾ããããã®ä»çµã¿ã§ã¯ãã¼ã¹ã®è¨èªã¨ãã¦è±èªãä½¿ããæ¥æ¬èªãªã©ã®ä»ã®è¨èªã«ã¯è±èªãããã®è¨èªã«ç¿»è¨³ãã¾ãããã¹ã¦ã®ãã­ã¥ã¡ã³ãã¯doc/source/ä»¥ä¸ã«ããã¦ããããSphinxã§å¦çãã¾ãã å¨ã¦ã®è¨èªã®HTMLãã¡ã¤ã«ãçæããã«ã¯doc/locale/ãã£ã¬ã¯ããªã§ `make html` ãå®è¡ãã¾ãã:: ã«ã¬ã³ããã£ã¬ã¯ããªã«000X-YYY.patchã¨ããååã®ãã¡ã¤ã«ãã§ãã¦ããã¨æãã¾ãããããGroongaãã­ã¸ã§ã¯ãã«éã£ã¦ãã ããï¼ doc/locale/${LANGUAGE}ãã£ã¬ã¯ããªã§ `make html` ãå®è¡ããã¨æ´æ°ãã ``.po`` ãã¡ã¤ã«ãä½¿ã£ã¦HTMLãã¡ã¤ã«ãçæã§ãã¾ããï¼`${LANGUAGE}` ã¯'ja'ãªã©èªåã®è¨èªã®è¨èªã³ã¼ãã«ç½®ãæãã¦ãã ãããï¼:: ``doc/locale/${LANGUAGE}/LC_MESSAGES`` ãã£ã¬ã¯ããªã§ `make` ãå®è¡ããã¨ ``.edit`` ãã¡ã¤ã«ãæ´æ°ã§ãã¾ããï¼`${LANGUAGE}` ã¯'ja'ãªã©èªåã®è¨èªã®è¨èªã³ã¼ãã«ç½®ãæãã¦ãã ãããï¼:: `è¨èªåãè¡¨è¨ããããã®ã³ã¼ãã®ä¸è¦§ <http://www.loc.gov/standards/iso639-2/php/English_list.php>`_. `Help.GitHub - pull requestãéã <http://help.github.com/pull-requests/>`_. 