��    	      d      �       �   +   �   N     \   \     �     �  �   �  F   a  6   �    �  J   �  J   ,  k   w     �     �  �   �  H   �  9                   	                               (Windows only) If network access is denied. If the number of threads is greater than 1 when we execute ``database_unmap``. If the table that is a target of ``table_remove`` has reference from other table and column. Major action on this error Major cause We confirm the status network(e.g. confirm whether the server process starting, confirm the setting of the firewall, and so on). We make the number of threads one by executing ``thread_limit max=1``. We remove tables and columns of the reference sources. Project-Id-Version: Groonga 12.0.5
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-06-17 12:34+0900
Language-Team: none
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 (Windows のみ) ネットワークアクセスを拒否された場合。 ``database_unmap`` 実行時にスレッド数が1より大きい場合。 ``table_remove`` の対象のテーブルに別のテーブルやカラムからの参照がある場合。 主な対策 主な原因 ネットワークの状態を確認します。(例えば、サーバープロセスが起動しているかどうかや、ファイヤーウォールの設定等を確認します。) ``thread_limit max=1`` を実行してスレッド数を1にします。 参照元のテーブルやカラムを削除します。 