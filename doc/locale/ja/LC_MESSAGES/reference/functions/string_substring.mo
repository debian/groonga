��            )   �      �     �  ;   �     �  X   �  F   T  7   �  D   �       
   ,     7     K     X     e  [   t  7   �  1     l   :     �     �     �     �  X   �     J  1   P  L   �  B   �  5     b   H  �   �     �  f   �  $   	  f   @	  <   �	  B   �	  E   '
     m
     �
     �
  	   �
     �
     �
  `   �
  8   0  K   i  �   �  $   F     k     r  !   y  x   �  	     T     q   s  H   �  G   .  `   v                                            
                                                      	                                        Execution example:: Here are a schema definition and sample data to show usage. Here is a simple example. If you omit or specify a negative value, this function extracts from ``nth`` to the end. If you specify a negative value, it counts from the end of ``target``. In the following example, specifying the default value. In the following example, specifying the negative value for ``nth``. Optional parameters Parameters Required parameters Return value Sample data: Sample schema: Specify a 0-based index number of charactors where to start the extraction from ``target``. Specify a number of characters to extract from ``nth``. Specify a string literal or a string type column. Specify a string to be returned when a substring is an empty string except when specifying 0 for ``length``. Specify the following key. Summary Syntax The default is an empty string. To enable this function, register ``functions/string`` plugin by following the command:: Usage You can specify string literal instead of column. ``options`` uses the following format. All of key-value pairs are optional:: ``string_substring`` extracts a substring of a string by position. ``string_substring`` requires two to four parameters. ``string_substring`` returns a substring extracted under the specified conditions from ``target``. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 実行例:: 使い方を示すために使うスキーマ定義とサンプルデータは以下の通りです。 以下は簡単な使用例です。 省略するか、負の値を指定した場合は、 ``nth`` から終端までを抽出します。 負の値を指定した場合は終端から数えます。 以下の例では、デフォルト値を指定しています。 以下の例では、 ``nth`` に負の値を指定しています。 省略可能引数 引数 必須引数 戻り値 サンプルデータ: サンプルスキーマ: ``target`` から抽出を開始する位置を0始まりの文字数単位で指定します。 ``nth`` から抽出する文字数を指定します。 対象となる文字列または文字列型カラムを指定します。 ``length`` に0を指定した場合を除いて、部分文字列が空文字列になった場合に返される文字列を指定します。 以下のキーを指定します。 概要 構文 省略時は空文字列です。 この関数を有効にするには、以下のコマンドで ``functions/string`` プラグインを登録します:: 使い方 カラムの代わりに文字列リテラルを指定することもできます。 ``options`` には以下のキーを指定します。すべてのキー・値のペアは省略可能です。:: ``string_substring`` は文字列の部分文字列を抽出します。 ``string_substring`` は2つから4つの引数を指定できます。 ``string_substring`` は指定した条件で抽出された部分文字列を返却します。 