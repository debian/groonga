��          �      ,      �     �  ,   �     �  
   J     U     b     o  Z   ~     �     �  %   �  X        g  3   m  ?   �  8   �  �          f   #  X   �     �  	   �     �       j   %     �     �     �  {   �  	   :  V   D  O   �  F   �                                             	             
                    Execution example:: Here is a schema definition and sample data. Here is the simple usage of ``vector_size`` function which returns tags and size - the value of ``tags`` column and size of it. Parameters Return value Sample data: Sample schema: Specifies a vector column of table that is specified by ``table`` parameter in ``select``. Summary Syntax There is only one required parameter. To enable this function, register ``functions/vector`` plugin by the following command:: Usage ``vector_size`` requires one argument - ``target``. ``vector_size`` returns the value of target vector column size. ``vector_size`` returns the value of vector column size. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 実行例:: 使い方を示すために使うスキーマ定義とサンプルデータは以下の通りです。 ``vector_size`` 関数が ``tag`` カラムの値とそのサイズを返す例です。 引数 戻り値 サンプルデータ: サンプルスキーマ: ``select`` 対象の ``table`` に指定されたテーブルのベクターカラムを指定します。 概要 構文 必須の引数は1つです。 この関数を有効にするには、以下のコマンドで ``functions/vector`` プラグインを登録します。:: 使い方 ``vector_size`` は引数を一つだけとります。 それは ``target`` です。 ``vector_size`` は対象のベクターカラムのサイズを返します。 ``vector_size`` はベクターカラムのサイズを返します。 