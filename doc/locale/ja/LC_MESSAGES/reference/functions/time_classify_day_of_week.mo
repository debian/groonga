��          �   %   �      @     A     U  ,   \  8   �  J   �  j        x          �     �     �     �     �     �     �  <   �  &        ?  V   H     �     �     �  	   �  8   �  �   �     �  	   �  f   	  F   p  o   �     '  	   �  	   �     �     �  	   �     �  	   �            F     '   \  	   �  y   �  	   	  	   	     	  	    	  >   *	                                                                                   	                    
                       Execution example:: Friday Here is a schema definition and sample data. Here is a simple usage of ``time_classify_day_of_week``: It returns ``0`` for Sunday, ``1`` for Monday, ... and ``6`` for Saturday. It returns the day of the week of the given time as a ``UInt8`` value. ``0`` is Sunday. ``6`` is Saturday. Monday Return value Sample data: Sample schema: Saturday Summary Sunday Syntax The day of the week The day of the week as ``UInt8``. Here are available values: This function has only one parameter:: Thursday To enable this function, register ``functions/time`` plugin by the following command:: Tuesday Usage Value Wednesday You need to register ``functions/time`` plugin at first: Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 実行例:: 金曜日 使い方を示すために使うスキーマ定義とサンプルデータは以下の通りです。 以下は簡単な ``time_classify_day_of_week`` の使用例です。 日曜日の場合は ``0`` 、月曜日の場合は ``1`` …、土曜日の場合は ``6`` を返します。 指定された時刻の曜日を ``UInt8`` の値として返します。 ``0`` は日曜日で、 ``6`` は土曜日です。 月曜日 戻り値 サンプルデータ: サンプルスキーマ: 土曜日 概要 日曜日 構文 曜日 ``UInt8`` で表現した曜日。有効な値は次の通りです。 この関数の引数は1つです。:: 木曜日 この関数を有効にするには、以下のコマンドで ``functions/time`` プラグインを登録します。:: 火曜日 使い方 値 水曜日 まず ``functions/time`` プラグインを登録します。 