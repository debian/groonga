��          �            x     y  ;   �  5   �  1   �  .   1     `  
   i     t     �     �     �     �    �  5   �       �          f     ?   w  9   �  $   �  	           	   '     1     8     ?  3   F  �  z  ?   C
  	   �
                  
                    	                                         Execution example:: Here are a schema definition and sample data to show usage. Here is an example that computes ranks for each game: Here is an example that specifies only sort keys: Here is an example that uses descending order: Nothing. Parameters Return value See also Summary Syntax The rank as ``UInt32`` value. This window function computes the rank of each record with gaps. This is similar to :doc:`window_record_number`. :doc:`window_record_number` computes the number of each record. The number is always incremented but the rank isn't incremented when multiple records that are the same order. The next rank after multiple records that are the same order has gap. If values of sort keys are ``100, 100, 200`` then the ranks of them are ``1, 1, 3``. The rank of the last record is ``3`` not ``2`` because there are two ``1`` rank records. This window function doesn't require any parameters:: Usage Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 実行例:: 使い方を示すために使うスキーマ定義とサンプルデータは以下の通りです。 以下は各ゲームごとに順位を計算する例です。 以下はソートキーのみを指定した例です。 以下は降順を使う例です。 なし。 引数 戻り値 参考 概要 構文 順位です。 ``UInt32`` の値になります。 このウィンドウ関数はギャップを含んだ各レコードの順位を計算します。 :doc:`window_record_number` に似ています。 :doc:`window_record_number` は各レコードが何番目のレコードかを計算します。何番目かは常に増加しますが、順位は複数のレコードが同じ順位の場合は増加しません。同じ順位のレコードが複数あった後の順位にはギャップがあります。ソートキーの値が ``100, 100, 200`` だった場合、順位はそれぞれ ``1, 1, 3`` になります。最後のレコードの順位は ``2`` ではなく ``3`` です。なぜなら順位が ``1`` のレコードが2つあるからです。 このウィンドウ関数には引数はありません。 :: 使い方 