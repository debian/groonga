��    	      d      �       �      �   
   �      �   )        -     5  %   >     d  �   j     g     w     ~  7   �     �     �     �  	   �                     	                         Options Parameters Related tables Specifies the path to a groonga database. Summary Synopsis There is only one required parameter. Usage Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 オプション 引数 関連するテーブル groongaデータベースのパスを指定します。 概要 書式 必須の引数は1つです。 使い方 