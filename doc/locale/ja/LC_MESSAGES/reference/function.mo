��          <      \       p      q   �   z   =     �   Y     V  �   ]  l                      Function Function can be used in some commands. For example, you can use function in ``--filter``, ``--scorer`` and ``output_columns`` options of :doc:`commands/select`. This section describes about function and built-in functions. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 関数 関数はいくつかのコマンドの中で使えます。 :doc:`commands/select` コマンドの ``--filter`` 、 ``--scorer`` 、 ``output_columns`` オプションで使えます。 このセクションでは関数についての説明と組み込みの関数について説明します。 