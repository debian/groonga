��          <      \       p   E   q      �   �   �   �   c  �   `     �  �                      This section describes window function and built-in window functions. Window function Window function can be used in :doc:`commands/select` and :doc:`commands/logical_select`. See :ref:`select-window-function-related-parameters` for details. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 このセクションではウィンドウ関数についての説明と組み込みのウィンドウ関数について説明します。 ウィンドウ関数 ウィンドウ関数は :doc:`commands/select` と :doc:`commands/logical_select` で使えます。詳細は :ref:`select-window-function-related-parameters` を見てください。 