Þ    	      d      ¬       à   ¬   á        D   ¢  Z   ç     B     K  -   S  W     ü   Ù  õ   Ö     Ì     Ø  v   d     Û     â  Z   é     D                                       	       A table can have zero or more token filters. You can attach token filters to a table by :ref:`table-create-token-filters` option in :doc:`/reference/commands/table_create`. Execution example:: Groonga has token filter module that some processes tokenized token. Here is an example ``table_create`` that uses ``TokenFilterStopWord`` token filter module: See also Summary Token filter module can be added as a plugin. You can customize tokenized token by registering your token filters plugins to Groonga. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 ãã¼ãã«ã¯0åä»¥ä¸ã®ãã¼ã¯ã³ãã£ã«ã¿ã¼ãæã¦ã¾ãããã¼ãã«ã«ãã¼ã¯ã³ãã£ã«ã¿ã¼ãä»ããã«ã¯ :doc:`/reference/commands/table_create` ã® :ref:`table-create-token-filters` ãªãã·ã§ã³ãä½¿ãã¾ãã å®è¡ä¾:: Groongaã«ã¯ãã¼ã¯ãã¤ãºããããã¼ã¯ã³ã«æå®ã®å¦çãè¡ããã¼ã¯ã³ãã£ã«ã¿ã¼ã¢ã¸ã¥ã¼ã«ãããã¾ãã ä»¥ä¸ã¯ ``TokenFilterStopWord`` ãã¼ã¯ã³ãã£ã«ã¿ã¼ã¢ã¸ã¥ã¼ã«ãä½¿ã ``table_create`` ã®ä¾ã§ãã åè æ¦è¦ ãã¼ã¯ã³ãã£ã«ã¿ã¼ã¢ã¸ã¥ã¼ã«ã¯ãã©ã°ã¤ã³ã¨ãã¦è¿½å ã§ãã¾ãã ãã¼ã¯ã³ãã£ã«ã¿ã¼ãã©ã°ã¤ã³ãGroongaã«è¿½å ãããã¨ã§ãã¼ã¯ãã¤ãºããããã¼ã¯ã³ãã«ã¹ã¿ãã¤ãºã§ãã¾ãã 