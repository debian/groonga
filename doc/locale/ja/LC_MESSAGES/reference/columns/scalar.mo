��          �      ,      �     �     �  B  �  [   �     N     c     {  %   �  <   �  6   �     #     ,  $   4  .   Y     �  9   �  �   �     �  	   �  �  �  �   �  !   i     �     �  E   �  X   	  N   \	     �	     �	  1   �	  ]   �	  	   I
  U   S
     
                                             	                            Generated scalar column How to create If you want to store zero or more values per record, you can use multiple scalar columns or one :doc:`vector`. If these values are related and use the same types such as tags (zero or more strings), :doc:`vector` is suitable. If these values aren't related such as title and location, multiple scalar columns are suitable. It can store one scalar value per record. Scalar value is one of the following type values: Normal scalar column Reference scalar column Scalar column Scalar column is a data store object. See :doc:`../commands/column_create` how to create a column. See :ref:`column-create-generated-column` for details. See also Summary There are three scalar column types: This section describes how to use these types. Usage You can use a scalar column as a :ref:`generated-column`. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-10-28 14:54+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 生成スカラーカラム 作り方 1レコードごとに複数の値を保存したいときは、複数のスカラーカラムを使うか、1つの :doc:`vector` を使えます。もしそれらの値が「複数のタグ（0個以上の文字列）」のように関連していてすべて同じ型の値の場合は :doc:`vector` が適しています。もし、それらの値が「タイトル」と「所在地」のように関連していない場合は複数のスカラーカラムが適しています。 スカラーカラムは1レコードごとに1つのスカラー値を保存できます。スカラー値は次の型の値のどれか1つです。 ノーマルスカラーカラム 参照スカラーカラム スカラーカラム スカラーカラムはデータストアオブジェクトです。 カラムの作り方は :doc:`../commands/column_create` を参照してください。 詳細は :ref:`column-create-generated-column` を参照してください。 参照 概要 3種類のスカラーカラムがあります。 このセクションではこれらのスカラーカラムの使い方を説明します。 使い方 スカラーカラム :ref:`generated-column` として使うことができます。 