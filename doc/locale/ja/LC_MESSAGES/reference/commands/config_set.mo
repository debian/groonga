Þ          Ü   %         0     1  c   E  K   ©     õ  
   	          (  =   5     s  /   |  J   ¬     ÷     ÿ       *         K     j  ,     &   ·     Þ  %   ä  _   
  H   j  ü   ³     °  i   ¼  u   &          ¯     ¶  	   Ã  a   Í     /  0   6  D   g     ¬     ³  %   º  7   à  -   	  *   F	  6   q	  K   ¨	  	   ô	  <   þ	  b   ;
  Q   
            	                 
                                                                                                  Execution example:: Here is an example to set a value to ``alias.column`` configuration item and confirm the set value: If command succeeded, it returns true, otherwise it returns false on error. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Specifies the key of target configuration item. Specifies the value of the target configuration item specified by ``key``. Summary Syntax The max key size is 4KiB. The max value size is 4091B (= 4KiB - 5B). There are required parameters. There is no optional parameter. This command takes two required parameters:: This section describes all parameters. Usage You can't use an empty string as key. ``config_set`` command returns whether setting a configuration item value is succeeded or not:: ``config_set`` command sets a value to the specified configuration item. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 å®è¡ä¾:: ä»¥ä¸ã¯ ``alias.column`` è¨­å®é ç®ã«å¤ãè¨­å®ãã¦ãè¨­å®ããå¤ãç¢ºèªããä¾ã§ãã ã³ãã³ãã®å®è¡ãæåããã¨trueãè¿ãã¾ããå¤±æããã¨ã¨ã©ã¼ã¨ãã¦falseãè¿ãã¾ãã çç¥å¯è½å¼æ° å¼æ° å¿é å¼æ° æ»ãå¤ ``HEADER`` ã«ã¤ãã¦ã¯ :doc:`/reference/command/output_format` ãåç§ãã¦ãã ããã åè å¯¾è±¡è¨­å®é ç®ã®ã­ã¼ãæå®ãã¾ãã ``key`` ã§æå®ããå¯¾è±¡è¨­å®é ç®ã®å¤ãæå®ãã¾ãã æ¦è¦ æ§æ æå¤§ã­ã¼ãµã¤ãºã¯4KiBã§ãã å¤ã®æå¤§ãµã¤ãºã¯4091Bï¼= 4KiB - 5Bï¼ã§ãã ããã¤ãå¿é ã®å¼æ°ãããã¾ãã çç¥å¯è½ãªå¼æ°ã¯ããã¾ããã ãã®ã³ãã³ãã®å¼æ°ã¯2ã¤ã§å¿é ã§ãã:: ãã®ã»ã¯ã·ã§ã³ã§ã¯ãã¹ã¦ã®å¼æ°ã«ã¤ãã¦èª¬æãã¾ãã ä½¿ãæ¹ ã­ã¼ã«ã¯ç©ºæå­åãä½¿ããã¨ã¯ã§ãã¾ããã ``config_set`` ã³ãã³ãã¯è¨­å®é ç®ã«å¤ãè¨­å®ã§ãããã©ãããè¿ãã¾ãã:: ``config_set`` ã³ãã³ãã¯æå®ããè¨­å®é ç®ã«å¤ãè¨­å®ãã¾ãã 