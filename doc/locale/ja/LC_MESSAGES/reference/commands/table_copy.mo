��          �      <      �     �     �  6   �     #  
   7     B     V  ;   c  %   �  )   �     �     �  9   �     8  "   X  "   {     �  �   �  $   �  #   �  ]   �     H     [     b  	   o  a   y  3   �  -        =     D  c   K  *   �  7   �  ?     	   R                                                            
         	                                All parameters are required. All parameters are required:: If the command fails, error details are in ``HEADER``. Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. Specifies the destination table name. Specifies the table name of source table. Summary Syntax The command returns ``true`` as body on success such as:: There is no optional parameter. This command takes two parameters. This section describes parameters. Usage Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 すべての引数は必須です。 すべての引数は必須です:: このコマンドが失敗すると、 ``HEADER`` にエラーの詳細が含まれます。 省略可能引数 引数 必須引数 戻り値 ``HEADER`` については :doc:`/reference/command/output_format` を参照してください。 コピー先のテーブル名を指定します。 ソーステーブル名を指定します。 概要 構文 このコマンドが成功したときは以下のようにボディは ``true`` になります:: 省略可能な引数はありません。 このコマンドには2つの引数があります。 このセクションでは引数について説明します。 使い方 