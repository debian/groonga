��          �      |      �     �            
   1     <     P  ;   ]     �  k   �            >     4   \     �  %   �  0   �  "        +  d   1  =   �  a   �  �   6     3  0   ?     p     �     �  	   �  a   �       �   
     �     �  Y   �  3     *   :     e  3   �  ?   �  	   �  v   	  \   z	  t   �	                                                                     	   
                         Execution example:: Here are sample shards: Optional parameters Parameters Required parameters Return value See :doc:`/reference/command/output_format` for ``HEADER``. See also Specifies the logical table name. ``logical_shard_list`` returns a list of shard name of the logical table: Summary Syntax The command returns a list of shard names in ascending order:: The list is sorted by shard name in ascending order. There is no optional parameter. There is only one required parameter. This command takes only one required parameter:: This section describes parameters. Usage You can get the all shard names in ascending order by specifying ``Logs`` as the logical table name: You need to register ``sharding`` plugin to use this command: ``logical_shard_list`` returns all existing shard names against the specified logical table name. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 実行例:: サンプルシャードは次の通りです。 省略可能引数 引数 必須引数 戻り値 ``HEADER`` については :doc:`/reference/command/output_format` を参照してください。 参考 論理テーブル名を指定します。 ``logical_shard_list`` は指定した論理テーブルのシャード名のリストを返します。 概要 構文 このコマンドは昇順でソートしたシャード名のリストを返します:: このリストは昇順でソート済みです。 省略可能な引数はありません。 必須の引数は1つです。 このコマンドの引数は1つで必須です:: このセクションでは引数について説明します。 使い方 論理テーブル名として ``Logs`` を指定すると昇順ですべてのシャード名を取得できます。 このコマンドを使うには事前に ``sharding`` プラグインを登録します。 ``logical_shard_list`` は指定した論理テーブル名に対するすべてのシャード名を返します。 