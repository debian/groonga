��          �      ,      �     �     �     �  $   �     �     �  =   �     8     A     I  "   P     s     �  :   �  �   �  b   ]  �   �     �     �  $   �  Z   �     P  	   W  a   a     �     �     �  /   �       	   $  |   .  �   �  �   �                  	                         
                                   Description Execution example:: Here is a simple example. It returns tokenizers in a database. Name Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Summary Syntax This command takes no parameters:: Tokenizer name. Usage ``tokenizer_list`` command lists tokenizers in a database. ``tokenizer_list`` command returns tokenizers. Each tokenizers has an attribute that contains the name. The attribute will be increased in the feature:: ``tokenizers`` is an array of tokenizer. Tokenizer is an object that has the following attributes. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 説明 実行例:: 以下は簡単な使用例です。 データベースに登録されているトークナイザーの一覧を返します。 名前 戻り値 ``HEADER`` については :doc:`/reference/command/output_format` を参照してください。 参考 概要 構文 このコマンドに引数はありません:: トークナイザー名。 使い方 ``tokenizer_list`` コマンドはデータベースに登録されているトークナイザーの一覧を返します。 ``tokenizer_list`` コマンドはトークナイザーの一覧を返します。各トークナイザーは属性を持っています。例えば名前です。将来、属性は増えるかもしれません。:: ``tokenizers`` はトークナイザーの配列です。トークナイザーは次の属性を持つオブジェクトです。 