Þ          ¬      <      °     ±     Ñ  "   X  h   {  
   ä  _   ï     O     \     e     m  1   t  F   Š     í  à     "   î       3     ü   K  *   H     s  6   ÿ     6     Ó  y   Ú  	   T     ^     e     l  4   s  c   š  $   	  '  1	  /   Y
  	   
  ?   
               
                                                          	                            Execute ``log_reopen`` command. It is used to reload log files such as groonga log or query log which are specified by ``--log-path`` or ``--query-log-path`` options. Lotate log files with `log_reopen` New log file is created as same as existing log file name. newer log content is written to new log file. Parameters Rename target log files such as mv command. (Log content is still written into moved log files) Return value See also Summary Syntax The command returns ``false`` otherwise such as:: The command returns ``true`` as body if the command succeeds such as:: There is no required parameter. This command only works when the number of worker processes is equal to 1.  Thus, it means that if you use :doc:`/reference/executables/groonga-httpd` with 2 or more workers, you must use ``groonga-httpd -s reopen`` instead. This command takes no parameters:: Usage ``log_reopen`` is a command that reloads log files. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-21 17:35+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 ``log_reopen`` ã³ãã³ããå®è¡ãã ``--log-path`` ã ``--query-log-path`` ãªãã·ã§ã³ã§æå®ãããã­ã°ãã¯ãšãªãŒã­ã°ãåèª­èŸŒããã®ã«äœ¿ããŸãã ``log_reopen`` ã§ã­ã°ã­ãŒããŒããå®æœãã æ¢å­ã®ã­ã°ãã¡ã€ã«åãšååã®æ°ããã­ã°ãã¡ã€ã«ãäœæãããŸããæ°èŠã­ã°ã¯æ°ããã­ã°ãã¡ã€ã«ã«æžãããŸãã åŒæ° å¯Ÿè±¡ãšãªãã­ã°ãã¡ã€ã«ãmvã³ãã³ããªã©ã§å€æŽãã(ã­ã°ã¯å€æŽãããã­ã°ã«æžãããŸã) æ»ãå€ åè æŠèŠ æ§æ ããã§ãªãå Žåã¯ ``false`` ãè¿ããŸã:: ãã®ã³ãã³ããæåãããšãã¯ä»¥äžã®ããã«ããã£ã¯ ``true`` ã«ãªããŸã:: å¿é ã®åŒæ°ã¯ãããŸããã ãã®ã³ãã³ãã¯ã¯ãŒã«ãŒãã­ã»ã¹æ°ã1ã®ãšãã®ã¿åäœããŸããã€ãŸãã :doc:`/reference/executables/groonga-httpd` ãã¯ãŒã«ãŒæ°2ä»¥äžã§äœ¿ã£ãŠããå Žåã«ã¯ã``log_reopen`` ã§ã¯ãªã ``groonga-httpd -s reopen`` ãäœ¿ããªããã°ãªããŸããã ãã®ã³ãã³ãã«åŒæ°ã¯ãããŸãã:: äœ¿ãæ¹ ``log_reopen`` ã¯ã­ã°ãåèª­èŸŒããã³ãã³ãã§ãã 