��          �      ,      �     �     �     �  %   �     �     �     �  =        J     S     [  "   b     �  <   �  �   �  e   d  �   �     �     �  $   �  Z   �     Z     a  	   }  a   �     �     �     �  /   �  	   .  }   8  �   �  �   �                                          
                          	          Description Execution example:: Here is a simple example. It returns normalizers in a database. Name Normalizer name. Return value See :doc:`/reference/command/output_format` about ``HEADER``. See also Summary Syntax This command takes no parameters:: Usage ``normalizer_list`` command lists normalizers in a database. ``normalizer_list`` command returns normalizers. Each normalizers has an attribute that contains the name. The attribute will be increased in the feature:: ``normalizers`` is an array of normalizer. Normalizer is an object that has the following attributes. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 説明 実行例:: 以下は簡単な使用例です。 データベースに登録されているノーマライザーの一覧を返します。 名前 ノーマライザー名。 戻り値 ``HEADER`` については :doc:`/reference/command/output_format` を参照してください。 参考 概要 構文 このコマンドに引数はありません:: 使い方 ``normalizer_list`` コマンドはデータベースに登録されているノーマライザーの一覧を返します。 ``normalizer_list`` コマンドはノーマライザーの一覧を返します。各ノーマライザーは属性を持っています。例えば名前です。将来、属性は増えるかもしれません。:: ``normalizers`` はノーマライザーの配列です。ノーマライザーは次の属性を持つオブジェクトです。 