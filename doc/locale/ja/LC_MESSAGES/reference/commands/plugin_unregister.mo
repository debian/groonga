��          �            x     y  �   �  @   !     b  ;   o     �     �     �  (   �  0   �       g   #  l   �  3   �  C   ,  �   p     m  �   y  a   0  	   �  a   �     �            0     3   D  	   x  �   �  �     R   �  g   �                                	                                  
             Execution example:: Here is a sample that unregisters ``QueryExpanderTSV`` query expander that is included in ``${PREFIX}/lib/groonga/plugins/query_expanders/tsv.so``. If ``plugin_unregister`` fails, error details are in ``HEADER``. Return value See :doc:`/reference/command/output_format` for ``HEADER``. See also Summary Syntax This command is an experimental feature. This command takes only one required parameter:: Usage You can omit ``${PREFIX}/lib/groonga/plugins/`` and suffix (``.so``). They are completed automatically. You can specify absolute path such as ``plugin_unregister /usr/lib/groonga/plugins/query_expanders/tsv.so``. ``plugin_unregister`` command unregisters a plugin. ``plugin_unregister`` returns ``true`` as body on success such as:: Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 実行例:: これは ``${PREFIX}/lib/groonga/plugins/query_expanders/tsv.so`` に含まれている ``QueryExpanderTSV`` クエリー展開オブジェクトの登録を解除する例です。 ``plugin_unregister`` が失敗すると、エラーの詳細は ``HEADER`` に含まれます。 戻り値 ``HEADER`` については :doc:`/reference/command/output_format` を参照してください。 参考 概要 構文 このコマンドは実験的な機能です。 このコマンドの引数は1つで必須です:: 使い方 ``${PREFIX}/lib/groonga/plugins/`` と拡張子（ ``.so`` ）は省略可能です。これらは自動で補完されます。 ``plugin_unregister /usr/lib/groonga/plugins/query_expanders/tsv.so`` というように絶対パスを指定することもできます。 ``plugin_unregister`` コマンドはプラグインの登録を解除します。 ``plugin_unregister`` が成功したときは以下のようにボディは ``true`` になります:: 