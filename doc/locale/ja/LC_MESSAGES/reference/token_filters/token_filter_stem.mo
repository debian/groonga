��          �      <      �  �   �     k  $     >   �     �  
   �       H        g     o  -   v     �  �   �  T   X  �   �  ,   G  *   t  �   �    �     �  S   �  N        S     f  6   m  u   �     	     !	  ?   (	  	   h	  �   r	  �   e
  �   �
  A   �  _   !               
   	                                                                                   All of ``develop``, ``developing``, ``developed`` and ``develops`` tokens are stemmed as ``develop``. So we can find ``develop``, ``developing`` and ``developed`` by ``develops`` query. Execution example:: Here are support steming algorithm:: Here is an example that uses ``TokenFilterStem`` token filter: Optional parameter Parameters Specify a steming algorithm. Steming algorithm is extract the stem. It is prepared for each language. Summary Syntax There is a optional parameters ``algorithm``. Usage You can extract the stem of each language by changing steming algorithm. For example, if you want extract the stem of the French, you specify French to ``algorithm`` option. You can specify steming algorithm except English with ``algorithm`` option as below. You need to install an additional package to using ``TokenFilterStem``. For more detail of how to installing an additional package, see :doc:`/install` . ``TokenFilterStem`` has optional parameter:: ``TokenFilterStem`` stems tokenized token. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-06-28 09:15+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 ``develop`` も ``developing`` も ``developed`` も ``develops`` も、すべてステミングすると ``develop`` になります。そのため、 ``develops`` というクエリーで ``develop`` も ``developing`` も ``developed`` も検索できます。 実行例:: サポートしているステミングアルゴリズムは以下の通りです:: 以下は ``TokenFilterStem`` トークンフィルターを使う例です。 省略可能引数 引数 ステミングアルゴリズムを指定します。 ステミングアルゴリズムとは、語幹を抽出するもので、各言語用に用意されています。 概要 構文 省略可能な引数として ``algorithm`` があります。 使い方 ステミングアルゴリズムを切り替えることに寄って、各言語の語幹を抽出できます。例えば、フランス語の語幹を抽出したい場合は、 ``algorithm`` オプションに French を指定します。 以下のように ``algorithm`` オプションを使って、英語以外のステミングアルゴリズムを指定できます。 ``TokenFilterStem`` を使うには、追加のパッケージをインストールする必要があります。追加のパッケージをインストールする方法の詳細については、 :doc:`/install` を参照して下さい。 ``TokenFilterStem`` は、省略可能な引数があります。 ``TokenFilterStem`` は、トークナイズされたトークンをステミングします。 