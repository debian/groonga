��          \      �       �      �      �      �      �   -   �   m      G   �  �   �     �     �     �  	   �  ?   �  i   7  c   �                                       Execution example:: Summary Syntax Usage ``TokenBigramSplitSymbol`` hasn't parameter:: ``TokenBigramSplitSymbol`` is similar to :ref:`token-bigram`. The difference between them is symbol handling. ``TokenBigramSplitSymbol`` tokenizes symbols by bigram tokenize method: Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 実行例:: 概要 構文 使い方 ``TokenBigramSplitSymbol`` には、引数がありません。 ``TokenBigramSplitSymbol`` は :ref:`token-bigram` と似ています。違いは記号の扱いです。 ``TokenBigramSplitSymbol`` は記号のトークナイズ方法にバイグラムを使います。 