��          \      �       �      �      �      �      �   7   �   �   *  �   �  �   d     a     m     t  	   {  I   �  �   �  �   d                                       Execution example:: Summary Syntax Usage ``TokenBigramSplitSymbolAlphaDigit`` hasn't parameter:: ``TokenBigramSplitSymbolAlphaDigit`` is similar to :ref:`token-bigram`. The difference between them is symbol, alphabet and digit handling. ``TokenBigramSplitSymbolAlphaDigit`` tokenizes symbols, alphabets and digits by bigram tokenize method. It means that all characters are tokenized by bigram tokenize method: Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 実行例:: 概要 構文 使い方 ``TokenBigramSplitSymbolAlphaDigit`` には、引数がありません。 ``TokenBigramSplitSymbolAlphaDigit`` は :ref:`token-bigram` と似ています。違いは記号とアルファベットと数字の扱いです。 ``TokenBigramSplitSymbolAlphaDigit`` は記号、アルファベット、数字のトークナイズ方法にバイグラムを使います。つまり、すべての文字をバイグラムでトークナイズします。 