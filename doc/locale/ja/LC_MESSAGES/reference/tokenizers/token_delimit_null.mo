Þ    	      d      ¬       à      á   +   õ      !     )     0  '   6  3   ^  Ý     ü   p     m  .   y     ¨     ¯  	   ¶  9   À  <   ú  ö   7               	                               Execution example:: Here is an example of ``TokenDelimitNull``: Summary Syntax Usage ``TokenDelimitNull`` hasn't parameter:: ``TokenDelimitNull`` is also suitable for tag text. ``TokenDelimitNull`` is similar to :ref:`token-delimit`. The difference between them is separator character. :ref:`token-delimit` uses space character (``U+0020``) but ``TokenDelimitNull`` uses NUL character (``U+0000``). Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 å®è¡ä¾:: ä»¥ä¸ã¯ ``TokenDelimitNull`` ã®ä¾ã§ãã æ¦è¦ æ§æ ä½¿ãæ¹ ``TokenDelimitNull`` ã«ã¯ãå¼æ°ãããã¾ããã ``TokenDelimitNull`` ãã¿ã°ãã­ã¹ãã«é©åã§ãã ``TokenDelimitNull`` ã¯ :ref:`token-delimit` ã«ä¼¼ã¦ãã¾ããéãã¯åºåãæå­ã§ãã :ref:`token-delimit` ã¯ç©ºç½æå­ï¼ ``U+0020`` ï¼ãä½¿ãã¾ããã ``TokenDelimitNull`` ã¯NULæå­ï¼ ``U+0000`` ï¼ãä½¿ãã¾ãã 