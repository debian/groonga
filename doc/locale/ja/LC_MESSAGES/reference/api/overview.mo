��          |      �          ]   !       C   �     �  	   �     �  h   �  �   O  9     z   Q  }   �  �   J  t   G     �  L   �               '  �   .  �   �  I   �  �   �  �   �                         
                	                  :c:func:`grn_init()` initializes Groonga. In contrast, :c:func:`grn_fin()` finalizes Groonga. Example Here is an example that uses Groonga as a full-text search library. Overview Reference Summary You can use Groonga as a library. You need to use the following APIs to initialize and finalize Groonga. You must call :c:func:`grn_init()` only once before you use APIs which are provided by Groonga. You must call :c:func:`grn_fin()` only once after you finish to use APIs which are provided by Groonga. ``GRN_SUCCESS`` on success, not ``GRN_SUCCESS`` on error. ``grn_fin()`` releases resources that are used by Groonga. You can't call other Groonga APIs after you call ``grn_fin()``. ``grn_init()`` initializes resources that are used by Groonga. You must call it just once before you call other Groonga APIs. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 :c:func:`grn_init()` はGroongaを初期化します。一方、 :c:func:`grn_fin()` はGroongaを終了します。 例 以下はGroongaを全文検索ライブラリーとして使う例です。 概要 リファレンス 概要 Groongaをライブラリーとして使うことができます。Groongaを初期化・終了するために次のAPIを使う必要があります。 Groongaが提供するAPIを使う前に :c:func:`grn_init()` を1度だけ呼ぶ必要があります。Groongaが提供するAPIを呼び終わったら、 :c:func:`grn_fin()` を1度だけ呼ぶ必要があります。 成功時は ``GRN_SUCCESS`` 、エラー時は ``GRN_SUCCESS`` 以外。 ``grn_fin()`` はGroongaが使ったリソースを解放します。 ``grn_fin()`` を呼んだ後はGroongaのAPIを呼ぶことはできません。 ``grn_init()`` はGroongaが使うリソースを初期化します。他のGroongaのAPIを呼ぶ前に1度だけこれを呼ぶ必要があります。 