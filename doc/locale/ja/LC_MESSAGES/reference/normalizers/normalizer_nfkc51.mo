Þ          |      Ü             !  =   5  E   s  g   ¹     !     )     0  %   6  J   \  '   §     Ï  ü   n     k  I   w  G   Á     	            	   ¢  A   ¬  R   î  8   A  Û   z                                	   
                        Execution example:: Here is an example that uses ``NormalizerNFKC51`` normalizer: It is called ``NormalizerNFKC51``, but it is for Unicode version 5.0. Normally you don't need to use ``NormalizerNFKC51`` explicitly. You can use ``NormalizerAuto`` instead. Summary Syntax Usage Use :doc:`./normalizer_nfkc` instead. ``NormalizerNFKC51`` and ``NormalizerNFKC("version", "5.0.0")`` are equal. ``NormalizerNFKC51`` hasn't parameter:: ``NormalizerNFKC51`` normalizes texts by Unicode NFKC (Normalization Form Compatibility Composition) for Unicode version 5.0. It supports only UTF-8 encoding. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-09 09:45+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 å®è¡ä¾:: ä»¥ä¸ã¯ ``NormalizerNFKC51`` ãã¼ãã©ã¤ã¶ã¼ãä½¿ãä¾ã§ãã ``NormalizerNFKC51`` ã¨ããååã§ãããUnicode 5.0ç¨ã§ãã éå¸¸ã ``NormalizerNFKC51`` ãæç¤ºçã«ä½¿ãå¿è¦ã¯ããã¾ãããä»£ããã« ``NormalizerAuto`` ãä½¿ã£ã¦ãã ããã æ¦è¦ æ§æ ä½¿ãæ¹ ä»£ããã« :doc:`./normalizer_nfkc` ããå©ç¨ãã ããã ``NormalizerNFKC51`` ã¯ ``NormalizerNFKC("version", "5.0.0")`` ã¨åãã§ãã ``NormalizerNFKC51`` ã«ã¯å¼æ°ã¯ããã¾ããã:: ``NormalizerNFKC51`` ã¯Unicode 5.0ç¨ã®Unicode NFKCï¼Normalization Form Compatibility Compositionï¼ãä½¿ã£ã¦ãã­ã¹ããæ­£è¦åãã¾ããUTF-8ã¨ã³ã³ã¼ãã£ã³ã°ã®ã¿ããµãã¼ããã¦ãã¾ãã 