��          <      \       p   f   q   $   �   N   �   �   L  �   I  8   �  o                      Groonga supports AlmaLinux. So you can continue using Groonga by switching from CentOS 7 to AlmaLinux. See also :doc:`/install/almalinux` . Support for Groonga has ended from version 14.0.5 because of CentOS 7’s EOL. Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-21 17:25+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 Groongaは、AlmaLinuxをサポートしているので、CentOS 7からAlmaLinuxにOSを切り替えることで引続きGroongaを利用できます。 :doc:`/install/almalinux` を参照してください。 CentOS 7がEOLになったのでGroonga 14.0.5以降はCentOS 7用パッケージを提供していません。 