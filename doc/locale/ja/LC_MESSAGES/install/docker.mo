��          \      �       �   &   �   	   �      �   (     e   1  =   �     �  �   �  5   �          1  &   G  �   n  ^         x                                       Create docker-compose.yml as follows:: Install:: Pulling image Then run it with the following command:: This section describes how to install Groonga on Docker. You can install Groonga image via DockerHub. We distribute Alpine Linux Groonga docker image on DockerHub. With docker-compose Project-Id-Version: 1.2.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-09-26 09:22+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 docker-compose.ymlを次のように作成します:: インストール:: イメージの取得 次のコマンドで実行します:: このセクションではDockerでGroongaをインストールする方法を説明します。GroongaイメージはDockerHub経由でインストールできます。 DockerHubにおいて、Alpine LinuxのGroongaのDockerイメージを配布しています。 docker-composeを用いる場合 