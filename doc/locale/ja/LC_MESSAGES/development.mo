��          4      L       `      a   �   m   �   0     -  *  4                    Development This section describes about developing with Groonga. You may develop an application that uses Groonga as its database, a library that uses libgroonga, language bindings of libgroonga and so on. Project-Id-Version: 2.0.4
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-06-22 21:16+0900
Language-Team: Japanese
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 開発 このセクションではGroongaを使った開発について説明します。例えば、Groongaをデータベースとして使ったアプリケーション、libgroongaを使ったライブラリ、libgroongaの言語バインディングなどを開発することがあるでしょう。 