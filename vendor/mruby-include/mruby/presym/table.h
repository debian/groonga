static const uint16_t presym_length_table[] = {
  1,	/* ! */
  1,	/* % */
  1,	/* & */
  1,	/* * */
  1,	/* + */
  1,	/* - */
  1,	/* / */
  1,	/* < */
  1,	/* > */
  1,	/* E */
  1,	/* ^ */
  1,	/* _ */
  1,	/* ` */
  1,	/* a */
  1,	/* b */
  1,	/* c */
  1,	/* d */
  1,	/* e */
  1,	/* f */
  1,	/* g */
  1,	/* h */
  1,	/* i */
  1,	/* j */
  1,	/* k */
  1,	/* l */
  1,	/* m */
  1,	/* n */
  1,	/* o */
  1,	/* p */
  1,	/* q */
  1,	/* r */
  1,	/* s */
  1,	/* t */
  1,	/* v */
  1,	/* w */
  1,	/* x */
  1,	/* y */
  1,	/* z */
  1,	/* | */
  1,	/* ~ */
  2,	/* != */
  2,	/* !~ */
  2,	/* $& */
  2,	/* $' */
  2,	/* $+ */
  2,	/* $0 */
  2,	/* $1 */
  2,	/* $; */
  2,	/* $> */
  2,	/* $? */
  2,	/* $` */
  2,	/* $~ */
  2,	/* && */
  2,	/* ** */
  2,	/* +@ */
  2,	/* -@ */
  2,	/* << */
  2,	/* <= */
  2,	/* == */
  2,	/* =~ */
  2,	/* >= */
  2,	/* >> */
  2,	/* GC */
  2,	/* IO */
  2,	/* PI */
  2,	/* PP */
  2,	/* [] */
  2,	/* ar */
  2,	/* as */
  2,	/* at */
  2,	/* bi */
  2,	/* bs */
  2,	/* cp */
  2,	/* e2 */
  2,	/* e3 */
  2,	/* ed */
  2,	/* ei */
  2,	/* fd */
  2,	/* gm */
  2,	/* gs */
  2,	/* id */
  2,	/* in */
  2,	/* io */
  2,	/* lm */
  2,	/* lz */
  2,	/* my */
  2,	/* nc */
  2,	/* nv */
  2,	/* on */
  2,	/* pp */
  2,	/* re */
  2,	/* sv */
  2,	/* tr */
  2,	/* vs */
  2,	/* wd */
  2,	/* || */
  3,	/* <=> */
  3,	/* === */
  3,	/* @pp */
  3,	/* DIG */
  3,	/* Dir */
  3,	/* EIO */
  3,	/* ENV */
  3,	/* MAX */
  3,	/* MIN */
  3,	/* NAN */
  3,	/* []= */
  3,	/* abs */
  3,	/* add */
  3,	/* arg */
  3,	/* ary */
  3,	/* beg */
  3,	/* blk */
  3,	/* chr */
  3,	/* cmd */
  3,	/* cmp */
  3,	/* cos */
  3,	/* day */
  3,	/* deq */
  3,	/* dev */
  3,	/* dig */
  3,	/* dir */
  3,	/* div */
  3,	/* dup */
  3,	/* end */
  3,	/* enq */
  3,	/* eof */
  3,	/* erf */
  3,	/* err */
  3,	/* exp */
  3,	/* fib */
  3,	/* get */
  3,	/* gid */
  3,	/* hex */
  3,	/* idx */
  3,	/* ino */
  3,	/* key */
  3,	/* kwd */
  3,	/* len */
  3,	/* lhs */
  3,	/* lim */
  3,	/* log */
  3,	/* low */
  3,	/* map */
  3,	/* max */
  3,	/* mid */
  3,	/* min */
  3,	/* mon */
  3,	/* msg */
  3,	/* new */
  3,	/* now */
  3,	/* nth */
  3,	/* num */
  3,	/* obj */
  3,	/* oct */
  3,	/* old */
  3,	/* opt */
  3,	/* ord */
  3,	/* out */
  3,	/* pat */
  3,	/* pid */
  3,	/* pop */
  3,	/* pos */
  3,	/* pow */
  3,	/* pwd */
  3,	/* quo */
  3,	/* req */
  3,	/* res */
  3,	/* ret */
  3,	/* rhs */
  3,	/* row */
  3,	/* sec */
  3,	/* sep */
  3,	/* set */
  3,	/* sin */
  3,	/* str */
  3,	/* sub */
  3,	/* sum */
  3,	/* sym */
  3,	/* tan */
  3,	/* tap */
  3,	/* tmp */
  3,	/* tr! */
  3,	/* uid */
  3,	/* utc */
  3,	/* val */
  3,	/* zip */
  4,	/* @dst */
  4,	/* @fib */
  4,	/* @kwd */
  4,	/* @obj */
  4,	/* ARGV */
  4,	/* EADV */
  4,	/* EDOM */
  4,	/* EXCL */
  4,	/* FREE */
  4,	/* File */
  4,	/* Hash */
  4,	/* Lazy */
  4,	/* Math */
  4,	/* NONE */
  4,	/* NULL */
  4,	/* Proc */
  4,	/* RDWR */
  4,	/* SYNC */
  4,	/* Slop */
  4,	/* Stat */
  4,	/* Text */
  4,	/* Time */
  4,	/* _dev */
  4,	/* acos */
  4,	/* all? */
  4,	/* any? */
  4,	/* arg0 */
  4,	/* arg1 */
  4,	/* arg2 */
  4,	/* args */
  4,	/* argv */
  4,	/* arys */
  4,	/* asin */
  4,	/* atan */
  4,	/* attr */
  4,	/* bsiz */
  4,	/* byte */
  4,	/* call */
  4,	/* cbrt */
  4,	/* ceil */
  4,	/* char */
  4,	/* chop */
  4,	/* cosh */
  4,	/* curr */
  4,	/* data */
  4,	/* desc */
  4,	/* drop */
  4,	/* dst? */
  4,	/* dump */
  4,	/* each */
  4,	/* elem */
  4,	/* eof? */
  4,	/* epos */
  4,	/* eql? */
  4,	/* erfc */
  4,	/* fail */
  4,	/* feed */
  4,	/* file */
  4,	/* fill */
  4,	/* find */
  4,	/* flag */
  4,	/* getc */
  4,	/* gets */
  4,	/* gmt? */
  4,	/* grep */
  4,	/* gsub */
  4,	/* hash */
  4,	/* help */
  4,	/* high */
  4,	/* hour */
  4,	/* idx2 */
  4,	/* init */
  4,	/* join */
  4,	/* key? */
  4,	/* keys */
  4,	/* lary */
  4,	/* last */
  4,	/* lazy */
  4,	/* left */
  4,	/* lidx */
  4,	/* line */
  4,	/* list */
  4,	/* log2 */
  4,	/* loop */
  4,	/* lval */
  4,	/* map! */
  4,	/* mask */
  4,	/* mday */
  4,	/* mesg */
  4,	/* meth */
  4,	/* mode */
  4,	/* name */
  4,	/* nan? */
  4,	/* nest */
  4,	/* next */
  4,	/* nil? */
  4,	/* node */
  4,	/* none */
  4,	/* obj= */
  4,	/* objs */
  4,	/* one? */
  4,	/* open */
  4,	/* opts */
  4,	/* orig */
  4,	/* pad1 */
  4,	/* pad2 */
  4,	/* pair */
  4,	/* path */
  4,	/* peek */
  4,	/* perm */
  4,	/* pipe */
  4,	/* plen */
  4,	/* pos= */
  4,	/* proc */
  4,	/* push */
  4,	/* puts */
  4,	/* rand */
  4,	/* rdev */
  4,	/* read */
  4,	/* rest */
  4,	/* ridx */
  4,	/* rval */
  4,	/* save */
  4,	/* seek */
  4,	/* send */
  4,	/* sinh */
  4,	/* size */
  4,	/* sort */
  4,	/* sqrt */
  4,	/* stat */
  4,	/* step */
  4,	/* str2 */
  4,	/* sub! */
  4,	/* succ */
  4,	/* sync */
  4,	/* tail */
  4,	/* take */
  4,	/* tanh */
  4,	/* tell */
  4,	/* text */
  4,	/* then */
  4,	/* to_a */
  4,	/* to_f */
  4,	/* to_h */
  4,	/* to_i */
  4,	/* to_s */
  4,	/* tr_s */
  4,	/* tty? */
  4,	/* type */
  4,	/* uniq */
  4,	/* upto */
  4,	/* usec */
  4,	/* user */
  4,	/* utc? */
  4,	/* vals */
  4,	/* wday */
  4,	/* yday */
  4,	/* year */
  4,	/* zone */
  5,	/* @args */
  5,	/* @desc */
  5,	/* @flag */
  5,	/* @memo */
  5,	/* @meth */
  5,	/* @name */
  5,	/* @objs */
  5,	/* @path */
  5,	/* @proc */
  5,	/* Array */
  5,	/* CREAT */
  5,	/* Class */
  5,	/* DSYNC */
  5,	/* E2BIG */
  5,	/* EAUTH */
  5,	/* EBADE */
  5,	/* EBADF */
  5,	/* EBADR */
  5,	/* EBUSY */
  5,	/* ECOMM */
  5,	/* EFBIG */
  5,	/* EIDRM */
  5,	/* EINTR */
  5,	/* ELOOP */
  5,	/* ENOSR */
  5,	/* ENXIO */
  5,	/* EPERM */
  5,	/* EPIPE */
  5,	/* EROFS */
  5,	/* ESRCH */
  5,	/* ETIME */
  5,	/* EXDEV */
  5,	/* Errno */
  5,	/* Error */
  5,	/* Fiber */
  5,	/* Float */
  5,	/* Group */
  5,	/* RADIX */
  5,	/* RSYNC */
  5,	/* Range */
  5,	/* STDIN */
  5,	/* TOTAL */
  5,	/* TRUNC */
  5,	/* TSort */
  5,	/* T_ENV */
  5,	/* _mode */
  5,	/* _pipe */
  5,	/* _rdev */
  5,	/* acosh */
  5,	/* args= */
  5,	/* arity */
  5,	/* array */
  5,	/* ary_F */
  5,	/* ary_T */
  5,	/* asinh */
  5,	/* assoc */
  5,	/* atan2 */
  5,	/* atanh */
  5,	/* atime */
  5,	/* begin */
  5,	/* block */
  5,	/* break */
  5,	/* bytes */
  5,	/* cache */
  5,	/* chars */
  5,	/* chdir */
  5,	/* child */
  5,	/* chmod */
  5,	/* chomp */
  5,	/* chop! */
  5,	/* class */
  5,	/* clear */
  5,	/* clone */
  5,	/* close */
  5,	/* count */
  5,	/* ctime */
  5,	/* curry */
  5,	/* cycle */
  5,	/* depth */
  5,	/* enums */
  5,	/* errno */
  5,	/* fetch */
  5,	/* field */
  5,	/* file? */
  5,	/* first */
  5,	/* flags */
  5,	/* flock */
  5,	/* floor */
  5,	/* flush */
  5,	/* fname */
  5,	/* force */
  5,	/* found */
  5,	/* frexp */
  5,	/* ftype */
  5,	/* getgm */
  5,	/* getwd */
  5,	/* group */
  5,	/* gsub! */
  5,	/* help? */
  5,	/* hypot */
  5,	/* index */
  5,	/* is_a? */
  5,	/* items */
  5,	/* klass */
  5,	/* ldexp */
  5,	/* level */
  5,	/* limit */
  5,	/* lines */
  5,	/* ljust */
  5,	/* local */
  5,	/* log10 */
  5,	/* lsize */
  5,	/* lstat */
  5,	/* match */
  5,	/* merge */
  5,	/* meth= */
  5,	/* mkdir */
  5,	/* month */
  5,	/* mtime */
  5,	/* names */
  5,	/* next! */
  5,	/* nlink */
  5,	/* none? */
  5,	/* null? */
  5,	/* other */
  5,	/* pairs */
  5,	/* parse */
  5,	/* pipe? */
  5,	/* popen */
  5,	/* pproc */
  5,	/* pread */
  5,	/* print */
  5,	/* quote */
  5,	/* raise */
  5,	/* reset */
  5,	/* right */
  5,	/* rjust */
  5,	/* rmdir */
  5,	/* round */
  5,	/* shift */
  5,	/* size? */
  5,	/* slice */
  5,	/* sort! */
  5,	/* split */
  5,	/* srand */
  5,	/* stack */
  5,	/* start */
  5,	/* state */
  5,	/* stats */
  5,	/* store */
  5,	/* strip */
  5,	/* succ! */
  5,	/* sync= */
  5,	/* tail? */
  5,	/* taken */
  5,	/* tally */
  5,	/* times */
  5,	/* total */
  5,	/* tr_s! */
  5,	/* tsort */
  5,	/* umask */
  5,	/* union */
  5,	/* uniq! */
  5,	/* value */
  5,	/* width */
  5,	/* write */
  5,	/* yield */
  5,	/* zero? */
  6,	/* $stdin */
  6,	/* @block */
  6,	/* @break */
  6,	/* @count */
  6,	/* @depth */
  6,	/* @first */
  6,	/* @flags */
  6,	/* @group */
  6,	/* @queue */
  6,	/* @value */
  6,	/* @width */
  6,	/* APPEND */
  6,	/* BINARY */
  6,	/* Cyclic */
  6,	/* DIRECT */
  6,	/* EACCES */
  6,	/* EAGAIN */
  6,	/* EBADFD */
  6,	/* EBFONT */
  6,	/* ECHILD */
  6,	/* ECHRNG */
  6,	/* EDQUOT */
  6,	/* EEXIST */
  6,	/* EFAULT */
  6,	/* EFTYPE */
  6,	/* EILSEQ */
  6,	/* EINVAL */
  6,	/* EIPSEC */
  6,	/* EISDIR */
  6,	/* EISNAM */
  6,	/* EL2HLT */
  6,	/* EL3HLT */
  6,	/* EL3RST */
  6,	/* ELNRNG */
  6,	/* EMFILE */
  6,	/* EMLINK */
  6,	/* ENFILE */
  6,	/* ENOANO */
  6,	/* ENOCSI */
  6,	/* ENODEV */
  6,	/* ENOENT */
  6,	/* ENOKEY */
  6,	/* ENOLCK */
  6,	/* ENOMEM */
  6,	/* ENOMSG */
  6,	/* ENONET */
  6,	/* ENOPKG */
  6,	/* ENOSPC */
  6,	/* ENOSTR */
  6,	/* ENOSYS */
  6,	/* ENOTTY */
  6,	/* EPROTO */
  6,	/* ERANGE */
  6,	/* ESPIPE */
  6,	/* ESRMNT */
  6,	/* ESTALE */
  6,	/* EUSERS */
  6,	/* EXFULL */
  6,	/* Fixnum */
  6,	/* Kernel */
  6,	/* Module */
  6,	/* NOCTTY */
  6,	/* NOTBOL */
  6,	/* NOTBOS */
  6,	/* NOTEOL */
  6,	/* NOTEOS */
  6,	/* Object */
  6,	/* Option */
  6,	/* Parser */
  6,	/* RDONLY */
  6,	/* Random */
  6,	/* Regexp */
  6,	/* Result */
  6,	/* STDERR */
  6,	/* STDOUT */
  6,	/* Status */
  6,	/* String */
  6,	/* Struct */
  6,	/* Symbol */
  6,	/* T_CPTR */
  6,	/* T_DATA */
  6,	/* T_HASH */
  6,	/* T_PROC */
  6,	/* WRONLY */
  6,	/* __id__ */
  6,	/* _atime */
  6,	/* _chdir */
  6,	/* _ctime */
  6,	/* _getwd */
  6,	/* _mtime */
  6,	/* _popen */
  6,	/* _value */
  6,	/* alive? */
  6,	/* append */
  6,	/* banner */
  6,	/* blocks */
  6,	/* break? */
  6,	/* caller */
  6,	/* center */
  6,	/* chomp! */
  6,	/* chroot */
  6,	/* concat */
  6,	/* config */
  6,	/* cover? */
  6,	/* delete */
  6,	/* detect */
  6,	/* digits */
  6,	/* divmod */
  6,	/* downto */
  6,	/* empty? */
  6,	/* enable */
  6,	/* equal? */
  6,	/* escape */
  6,	/* except */
  6,	/* exist? */
  6,	/* extend */
  6,	/* fileno */
  6,	/* filter */
  6,	/* finish */
  6,	/* first? */
  6,	/* for_fd */
  6,	/* format */
  6,	/* freeze */
  6,	/* getutc */
  6,	/* gmtime */
  6,	/* groups */
  6,	/* id_map */
  6,	/* ifnone */
  6,	/* indent */
  6,	/* inject */
  6,	/* insert */
  6,	/* intern */
  6,	/* invert */
  6,	/* isatty */
  6,	/* itself */
  6,	/* lambda */
  6,	/* length */
  6,	/* longer */
  6,	/* lstrip */
  6,	/* match? */
  6,	/* max_by */
  6,	/* maxlen */
  6,	/* member */
  6,	/* merge! */
  6,	/* method */
  6,	/* min_by */
  6,	/* minmax */
  6,	/* mktime */
  6,	/* modulo */
  6,	/* n_args */
  6,	/* object */
  6,	/* offset */
  6,	/* option */
  6,	/* others */
  6,	/* output */
  6,	/* owned? */
  6,	/* padstr */
  6,	/* parser */
  6,	/* prefix */
  6,	/* printf */
  6,	/* public */
  6,	/* pwrite */
  6,	/* random */
  6,	/* rassoc */
  6,	/* reduce */
  6,	/* regexp */
  6,	/* rehash */
  6,	/* reject */
  6,	/* rename */
  6,	/* result */
  6,	/* resume */
  6,	/* rewind */
  6,	/* rindex */
  6,	/* rotate */
  6,	/* rstrip */
  6,	/* sample */
  6,	/* select */
  6,	/* slice! */
  6,	/* source */
  6,	/* string */
  6,	/* strip! */
  6,	/* to_int */
  6,	/* to_str */
  6,	/* to_sym */
  6,	/* ungetc */
  6,	/* unlink */
  6,	/* upcase */
  6,	/* update */
  6,	/* valid? */
  6,	/* value= */
  6,	/* value? */
  6,	/* values */
  7,	/* $stderr */
  7,	/* $stdout */
  7,	/* @banner */
  7,	/* @buffer */
  7,	/* @config */
  7,	/* @indent */
  7,	/* @output */
  7,	/* @parser */
  7,	/* @source */
  7,	/* Complex */
  7,	/* EBADMSG */
  7,	/* EBADRPC */
  7,	/* EBADRQC */
  7,	/* EBADSLT */
  7,	/* EDEADLK */
  7,	/* EDOOFUS */
  7,	/* EDOTDOT */
  7,	/* EISCONN */
  7,	/* ELIBACC */
  7,	/* ELIBBAD */
  7,	/* ELIBMAX */
  7,	/* ELIBSCN */
  7,	/* ENAVAIL */
  7,	/* ENOATTR */
  7,	/* ENOBUFS */
  7,	/* ENODATA */
  7,	/* ENOEXEC */
  7,	/* ENOLINK */
  7,	/* ENOTBLK */
  7,	/* ENOTDIR */
  7,	/* ENOTNAM */
  7,	/* ENOTSUP */
  7,	/* EPSILON */
  7,	/* EREMCHG */
  7,	/* EREMOTE */
  7,	/* ERFKILL */
  7,	/* ETXTBSY */
  7,	/* EUCLEAN */
  7,	/* EUNATCH */
  7,	/* IOError */
  7,	/* Integer */
  7,	/* LOCK_EX */
  7,	/* LOCK_NB */
  7,	/* LOCK_SH */
  7,	/* LOCK_UN */
  7,	/* MAX_EXP */
  7,	/* MIN_EXP */
  7,	/* NOATIME */
  7,	/* NOERROR */
  7,	/* Numeric */
  7,	/* Options */
  7,	/* Process */
  7,	/* TMPFILE */
  7,	/* T_ARRAY */
  7,	/* T_BREAK */
  7,	/* T_CLASS */
  7,	/* T_FIBER */
  7,	/* T_FLOAT */
  7,	/* T_RANGE */
  7,	/* VERSION */
  7,	/* Yielder */
  7,	/* __lines */
  7,	/* __merge */
  7,	/* _result */
  7,	/* asctime */
  7,	/* blksize */
  7,	/* bsearch */
  7,	/* casecmp */
  7,	/* ceildiv */
  7,	/* closed? */
  7,	/* collect */
  7,	/* command */
  7,	/* compact */
  7,	/* compile */
  7,	/* current */
  7,	/* default */
  7,	/* delete! */
  7,	/* dirname */
  7,	/* disable */
  7,	/* dropped */
  7,	/* entries */
  7,	/* exists? */
  7,	/* extname */
  7,	/* filter! */
  7,	/* finite? */
  7,	/* flatten */
  7,	/* foreach */
  7,	/* friday? */
  7,	/* frozen? */
  7,	/* getbyte */
  7,	/* include */
  7,	/* indexes */
  7,	/* inspect */
  7,	/* keep_if */
  7,	/* keyrest */
  7,	/* lambda? */
  7,	/* lstrip! */
  7,	/* max_cmp */
  7,	/* member? */
  7,	/* members */
  7,	/* message */
  7,	/* methods */
  7,	/* min_cmp */
  7,	/* modules */
  7,	/* monday? */
  7,	/* nesting */
  7,	/* new_key */
  7,	/* newline */
  7,	/* nobits? */
  7,	/* node_id */
  7,	/* options */
  7,	/* padding */
  7,	/* pattern */
  7,	/* pointer */
  7,	/* pp_hash */
  7,	/* prepend */
  7,	/* private */
  7,	/* process */
  7,	/* produce */
  7,	/* product */
  7,	/* reject! */
  7,	/* replace */
  7,	/* result= */
  7,	/* reverse */
  7,	/* rotate! */
  7,	/* rstrip! */
  7,	/* select! */
  7,	/* sep_len */
  7,	/* seplist */
  7,	/* setbyte */
  7,	/* setgid? */
  7,	/* setuid? */
  7,	/* shorter */
  7,	/* shuffle */
  7,	/* socket? */
  7,	/* sort_by */
  7,	/* sprintf */
  7,	/* squeeze */
  7,	/* sticky? */
  7,	/* strings */
  7,	/* sunday? */
  7,	/* symlink */
  7,	/* sysopen */
  7,	/* sysread */
  7,	/* sysseek */
  7,	/* to_enum */
  7,	/* to_hash */
  7,	/* to_path */
  7,	/* to_proc */
  7,	/* unshift */
  7,	/* upcase! */
  7,	/* version */
  7,	/* yielder */
  8,	/* @newline */
  8,	/* @options */
  8,	/* EALREADY */
  8,	/* EL2NSYNC */
  8,	/* ELIBEXEC */
  8,	/* EMSGSIZE */
  8,	/* ENETDOWN */
  8,	/* ENOTCONN */
  8,	/* ENOTSOCK */
  8,	/* ENOTUNIQ */
  8,	/* EOFError */
  8,	/* EPROCLIM */
  8,	/* ERESTART */
  8,	/* ESTRPIPE */
  8,	/* EXTENDED */
  8,	/* FileTest */
  8,	/* INFINITY */
  8,	/* KeyError */
  8,	/* MANT_DIG */
  8,	/* NOFOLLOW */
  8,	/* NONBLOCK */
  8,	/* NilClass */
  8,	/* Rational */
  8,	/* SEEK_CUR */
  8,	/* SEEK_END */
  8,	/* SEEK_SET */
  8,	/* T_BIGINT */
  8,	/* T_ICLASS */
  8,	/* T_MODULE */
  8,	/* T_OBJECT */
  8,	/* T_SCLASS */
  8,	/* T_STRING */
  8,	/* T_STRUCT */
  8,	/* __ary_eq */
  8,	/* __delete */
  8,	/* __send__ */
  8,	/* __svalue */
  8,	/* __to_int */
  8,	/* _gethome */
  8,	/* allbits? */
  8,	/* allocate */
  8,	/* anybits? */
  8,	/* argument */
  8,	/* basename */
  8,	/* between? */
  8,	/* bytesize */
  8,	/* captures */
  8,	/* casecmp? */
  8,	/* chardev? */
  8,	/* child_id */
  8,	/* collect! */
  8,	/* compact! */
  8,	/* default= */
  8,	/* defined? */
  8,	/* downcase */
  8,	/* dropping */
  8,	/* each_key */
  8,	/* enum_for */
  8,	/* extended */
  8,	/* filename */
  8,	/* find_all */
  8,	/* flat_map */
  8,	/* flatten! */
  8,	/* genspace */
  8,	/* getlocal */
  8,	/* group_by */
  8,	/* has_key? */
  8,	/* home_dir */
  8,	/* include? */
  8,	/* included */
  8,	/* kind_of? */
  8,	/* maxwidth */
  8,	/* modified */
  8,	/* new_args */
  8,	/* nonzero? */
  8,	/* open_obj */
  8,	/* overlap? */
  8,	/* readbyte */
  8,	/* readchar */
  8,	/* readline */
  8,	/* readlink */
  8,	/* realpath */
  8,	/* required */
  8,	/* reverse! */
  8,	/* self_len */
  8,	/* shuffle! */
  8,	/* squeeze! */
  8,	/* str_each */
  8,	/* swapcase */
  8,	/* symlink? */
  8,	/* syswrite */
  8,	/* transfer */
  8,	/* truncate */
  8,	/* tuesday? */
  9,	/* @genspace */
  9,	/* @maxwidth */
  9,	/* @stop_exc */
  9,	/* Breakable */
  9,	/* Constants */
  9,	/* ECANCELED */
  9,	/* EDEADLOCK */
  9,	/* EHOSTDOWN */
  9,	/* EMULTIHOP */
  9,	/* ENEEDAUTH */
  9,	/* ENETRESET */
  9,	/* ENOMEDIUM */
  9,	/* ENOTEMPTY */
  9,	/* EOVERFLOW */
  9,	/* EREMOTEIO */
  9,	/* ESHUTDOWN */
  9,	/* ETIMEDOUT */
  9,	/* Exception */
  9,	/* Generator */
  9,	/* IntOption */
  9,	/* MULTILINE */
  9,	/* MatchData */
  9,	/* NameError */
  9,	/* PPMethods */
  9,	/* SEPARATOR */
  9,	/* T_COMPLEX */
  9,	/* T_INTEGER */
  9,	/* T_ISTRUCT */
  9,	/* TrueClass */
  9,	/* TypeError */
  9,	/* __ary_cmp */
  9,	/* __compact */
  9,	/* __outer__ */
  9,	/* _gc_root_ */
  9,	/* _sys_fail */
  9,	/* _sysclose */
  9,	/* ancestors */
  9,	/* arguments */
  9,	/* backtrace */
  9,	/* base_path */
  9,	/* birthtime */
  9,	/* blockdev? */
  9,	/* breakable */
  9,	/* byteindex */
  9,	/* byteslice */
  9,	/* casefold? */
  9,	/* clean_key */
  9,	/* close_obj */
  9,	/* component */
  9,	/* const_get */
  9,	/* const_set */
  9,	/* constants */
  9,	/* delete_at */
  9,	/* delete_if */
  9,	/* delimiter */
  9,	/* dev_major */
  9,	/* dev_minor */
  9,	/* downcase! */
  9,	/* each_byte */
  9,	/* each_char */
  9,	/* each_cons */
  9,	/* each_line */
  9,	/* each_node */
  9,	/* each_pair */
  9,	/* end_with? */
  9,	/* exception */
  9,	/* exclusive */
  9,	/* feedvalue */
  9,	/* group_sub */
  9,	/* grpowned? */
  9,	/* infinite? */
  9,	/* inherited */
  9,	/* iterator? */
  9,	/* localtime */
  9,	/* minmax_by */
  9,	/* negative? */
  9,	/* object_id */
  9,	/* old_index */
  9,	/* old_slice */
  9,	/* orig_flag */
  9,	/* partition */
  9,	/* positive? */
  9,	/* pp_object */
  9,	/* pre_match */
  9,	/* prepended */
  9,	/* protected */
  9,	/* readable? */
  9,	/* readlines */
  9,	/* remainder */
  9,	/* required? */
  9,	/* satisfied */
  9,	/* saturday? */
  9,	/* separator */
  9,	/* swapcase! */
  9,	/* thursday? */
  9,	/* transpose */
  9,	/* ungetbyte */
  9,	/* validated */
  9,	/* values_at */
  9,	/* writable? */
  10,	/* @arguments */
  10,	/* @feedvalue */
  10,	/* @lookahead */
  10,	/* BoolOption */
  10,	/* Comparable */
  10,	/* EADDRINUSE */
  10,	/* ECONNRESET */
  10,	/* EOPNOTSUPP */
  10,	/* EOWNERDEAD */
  10,	/* EPROTOTYPE */
  10,	/* Enumerable */
  10,	/* Enumerator */
  10,	/* FalseClass */
  10,	/* FiberError */
  10,	/* GroupQueue */
  10,	/* IGNORECASE */
  10,	/* IndexError */
  10,	/* MAX_10_EXP */
  10,	/* MIN_10_EXP */
  10,	/* NullOption */
  10,	/* OnigRegexp */
  10,	/* RangeError */
  10,	/* SINGLELINE */
  10,	/* SingleLine */
  10,	/* T_RATIONAL */
  10,	/* __callee__ */
  10,	/* __case_eqq */
  10,	/* __method__ */
  10,	/* __num_to_a */
  10,	/* add_option */
  10,	/* breakables */
  10,	/* byterindex */
  10,	/* bytesplice */
  10,	/* capitalize */
  10,	/* class_eval */
  10,	/* codepoints */
  10,	/* difference */
  10,	/* directory? */
  10,	/* drop_while */
  10,	/* each_child */
  10,	/* each_index */
  10,	/* each_slice */
  10,	/* each_value */
  10,	/* fd_or_path */
  10,	/* filter_map */
  10,	/* find_index */
  10,	/* given_args */
  10,	/* has_value? */
  10,	/* initialize */
  10,	/* intersect? */
  10,	/* last_match */
  10,	/* make_curry */
  10,	/* match_data */
  10,	/* minimum_id */
  10,	/* open_width */
  10,	/* parameters */
  10,	/* path_token */
  10,	/* post_match */
  10,	/* rdev_major */
  10,	/* rdev_minor */
  10,	/* root_group */
  10,	/* rpartition */
  10,	/* self_arity */
  10,	/* separators */
  10,	/* step_ratio */
  10,	/* string_sub */
  10,	/* superclass */
  10,	/* take_while */
  10,	/* tsort_each */
  10,	/* wednesday? */
  10,	/* with_index */
  10,	/* yield_self */
  11,	/* @breakables */
  11,	/* @inspecting */
  11,	/* @last_match */
  11,	/* @separators */
  11,	/* ASCII_RANGE */
  11,	/* ArrayOption */
  11,	/* BasicObject */
  11,	/* DomainError */
  11,	/* EINPROGRESS */
  11,	/* EKEYEXPIRED */
  11,	/* EKEYREVOKED */
  11,	/* EMEDIUMTYPE */
  11,	/* ENETUNREACH */
  11,	/* ENOPROTOOPT */
  11,	/* EWOULDBLOCK */
  11,	/* Errno2class */
  11,	/* FNM_SYSCASE */
  11,	/* FloatOption */
  11,	/* FrozenError */
  11,	/* ObjectMixin */
  11,	/* ObjectSpace */
  11,	/* PrettyPrint */
  11,	/* RUBY_ENGINE */
  11,	/* RegexpError */
  11,	/* ScriptError */
  11,	/* SyntaxError */
  11,	/* TRUE_VALUES */
  11,	/* T_EXCEPTION */
  11,	/* __ary_index */
  11,	/* __members__ */
  11,	/* attr_reader */
  11,	/* attr_writer */
  11,	/* capitalize! */
  11,	/* cleaned_key */
  11,	/* close_width */
  11,	/* close_write */
  11,	/* combination */
  11,	/* default_dir */
  11,	/* each_object */
  11,	/* ensure_call */
  11,	/* executable? */
  11,	/* expand_path */
  11,	/* group_queue */
  11,	/* iter_method */
  11,	/* last_match= */
  11,	/* module_eval */
  11,	/* next_values */
  11,	/* owned_real? */
  11,	/* peek_values */
  11,	/* permutation */
  11,	/* respond_to? */
  11,	/* start_with? */
  11,	/* step_ratio= */
  11,	/* string_gsub */
  11,	/* string_scan */
  11,	/* try_process */
  11,	/* with_object */
  12,	/* @group_queue */
  12,	/* @group_stack */
  12,	/* EAFNOSUPPORT */
  12,	/* ECONNABORTED */
  12,	/* ECONNREFUSED */
  12,	/* EDESTADDRREQ */
  12,	/* EHOSTUNREACH */
  12,	/* EKEYREJECTED */
  12,	/* ENAMETOOLONG */
  12,	/* EPFNOSUPPORT */
  12,	/* EPROCUNAVAIL */
  12,	/* EPROGUNAVAIL */
  12,	/* ERPCMISMATCH */
  12,	/* ETOOMANYREFS */
  12,	/* FALSE_VALUES */
  12,	/* FIND_LONGEST */
  12,	/* FNM_CASEFOLD */
  12,	/* FNM_DOTMATCH */
  12,	/* FNM_NOESCAPE */
  12,	/* FNM_PATHNAME */
  12,	/* NEWLINE_CRLF */
  12,	/* RUBY_VERSION */
  12,	/* RegexpOption */
  12,	/* RuntimeError */
  12,	/* SHARE_DELETE */
  12,	/* StringOption */
  12,	/* SymbolOption */
  12,	/* VALID_VALUES */
  12,	/* __ENCODING__ */
  12,	/* __attached__ */
  12,	/* __codepoints */
  12,	/* _concat_path */
  12,	/* alias_method */
  12,	/* block_given? */
  12,	/* column_count */
  12,	/* column_index */
  12,	/* default_proc */
  12,	/* drive_prefix */
  12,	/* exclude_end? */
  12,	/* fetch_values */
  12,	/* force_false? */
  12,	/* ignored_args */
  12,	/* instance_of? */
  12,	/* intersection */
  12,	/* method_added */
  12,	/* mruby_Random */
  12,	/* object_group */
  12,	/* output_width */
  12,	/* pretty_flags */
  12,	/* pretty_print */
  12,	/* remove_const */
  12,	/* reverse_each */
  12,	/* stack_length */
  12,	/* string_split */
  12,	/* undef_method */
  12,	/* used_options */
  13,	/* @buffer_width */
  13,	/* @output_width */
  13,	/* ALT_SEPARATOR */
  13,	/* ArgumentError */
  13,	/* BooleanOption */
  13,	/* CAPTURE_GROUP */
  13,	/* EADDRNOTAVAIL */
  13,	/* EPROGMISMATCH */
  13,	/* IntegerOption */
  13,	/* MRUBY_VERSION */
  13,	/* NoMemoryError */
  13,	/* NoMethodError */
  13,	/* OnigMatchData */
  13,	/* StandardError */
  13,	/* StopIteration */
  13,	/* UnknownOption */
  13,	/* __classname__ */
  13,	/* __sub_replace */
  13,	/* __update_hash */
  13,	/* attr_accessor */
  13,	/* bsearch_index */
  13,	/* const_missing */
  13,	/* count_objects */
  13,	/* current_group */
  13,	/* default_proc= */
  13,	/* default_value */
  13,	/* define_method */
  13,	/* delete_prefix */
  13,	/* delete_suffix */
  13,	/* expanded_path */
  13,	/* extend_object */
  13,	/* in_lower_half */
  13,	/* instance_eval */
  13,	/* instance_exec */
  13,	/* partition_idx */
  13,	/* remove_method */
  13,	/* set_backtrace */
  13,	/* singleline_pp */
  13,	/* splitted_path */
  13,	/* validate_type */
  14,	/* DEFAULT_CONFIG */
  14,	/* FIND_NOT_EMPTY */
  14,	/* LocalJumpError */
  14,	/* PATH_SEPARATOR */
  14,	/* StandardOutput */
  14,	/* __upto_endless */
  14,	/* close_on_exec= */
  14,	/* close_on_exec? */
  14,	/* collect_concat */
  14,	/* const_defined? */
  14,	/* delete_prefix! */
  14,	/* delete_suffix! */
  14,	/* each_codepoint */
  14,	/* explicit_value */
  14,	/* fill_breakable */
  14,	/* interval_ratio */
  14,	/* longest_option */
  14,	/* method_missing */
  14,	/* method_removed */
  14,	/* named_captures */
  14,	/* paragraph_mode */
  14,	/* pretty_inspect */
  14,	/* public_methods */
  14,	/* readable_real? */
  14,	/* sub_minimum_id */
  14,	/* transform_keys */
  14,	/* unused_options */
  14,	/* validate_type? */
  14,	/* validate_types */
  14,	/* writable_real? */
  15,	/* ENOTRECOVERABLE */
  15,	/* EPROTONOSUPPORT */
  15,	/* ESOCKTNOSUPPORT */
  15,	/* MRUBY_COPYRIGHT */
  15,	/* MissingArgument */
  15,	/* SystemCallError */
  15,	/* append_features */
  15,	/* class_variables */
  15,	/* comma_breakable */
  15,	/* each_with_index */
  15,	/* explicit_value= */
  15,	/* include_private */
  15,	/* initialize_copy */
  15,	/* interval_ratio= */
  15,	/* local_variables */
  15,	/* matching_option */
  15,	/* method_defined? */
  15,	/* module_function */
  15,	/* onig_regexp_sub */
  15,	/* option_defined? */
  15,	/* pad_repetitions */
  15,	/* pop_inspect_key */
  15,	/* private_methods */
  15,	/* singleton_class */
  15,	/* source_location */
  15,	/* suppress_errors */
  15,	/* transform_keys! */
  15,	/* tsort_each_node */
  15,	/* world_readable? */
  15,	/* world_writable? */
  16,	/* FloatDomainError */
  16,	/* MRUBY_RELEASE_NO */
  16,	/* SystemStackError */
  16,	/* each_with_object */
  16,	/* executable_real? */
  16,	/* global_variables */
  16,	/* included_modules */
  16,	/* instance_methods */
  16,	/* onig_regexp_gsub */
  16,	/* onig_regexp_scan */
  16,	/* prepend_features */
  16,	/* push_inspect_key */
  16,	/* string_to_option */
  16,	/* suppress_errors? */
  16,	/* transform_values */
  16,	/* tsort_each_child */
  16,	/* underscore_flags */
  17,	/* INT_STRING_REGEXP */
  17,	/* MRUBY_DESCRIPTION */
  17,	/* NEGATE_SINGLELINE */
  17,	/* ZeroDivisionError */
  17,	/* check_inspect_key */
  17,	/* expand_path_array */
  17,	/* expects_argument? */
  17,	/* generational_mode */
  17,	/* guard_inspect_key */
  17,	/* onig_regexp_split */
  17,	/* protected_methods */
  17,	/* sharing_detection */
  17,	/* singleline_format */
  17,	/* singleton_methods */
  17,	/* transform_values! */
  17,	/* underscore_flags? */
  18,	/* @sharing_detection */
  18,	/* DONT_CAPTURE_GROUP */
  18,	/* InvalidOptionValue */
  18,	/* MRUBY_RELEASE_DATE */
  18,	/* class_variable_get */
  18,	/* class_variable_set */
  18,	/* generational_mode= */
  18,	/* instance_variables */
  18,	/* onig_regexp_match? */
  18,	/* pretty_print_cycle */
  19,	/* FLOAT_STRING_REGEXP */
  19,	/* NotImplementedError */
  19,	/* RUBY_ENGINE_VERSION */
  19,	/* longest_flag_length */
  19,	/* old_square_brancket */
  19,	/* respond_to_missing? */
  20,	/* WORD_BOUND_ALL_RANGE */
  20,	/* __inspect_recursive? */
  20,	/* break_outmost_groups */
  20,	/* object_address_group */
  20,	/* pretty_print_inspect */
  20,	/* repeated_combination */
  20,	/* repeated_permutation */
  21,	/* @set_global_variables */
  21,	/* MissingRequiredOption */
  21,	/* __coerce_step_counter */
  21,	/* enumerator_block_call */
  21,	/* instance_variable_get */
  21,	/* instance_variable_set */
  21,	/* remove_class_variable */
  21,	/* set_global_variables= */
  21,	/* set_global_variables? */
  22,	/* __repeated_combination */
  22,	/* clear_global_variables */
  22,	/* consume_next_argument? */
  22,	/* singleton_method_added */
  22,	/* string_to_option_class */
  23,	/* POSIX_BRACKET_ALL_RANGE */
  23,	/* class_variable_defined? */
  23,	/* define_singleton_method */
  23,	/* try_process_smashed_arg */
  24,	/* remove_instance_variable */
  25,	/* old_square_brancket_equal */
  25,	/* try_process_grouped_flags */
  26,	/* instance_variable_defined? */
  26,	/* undefined_instance_methods */
  29,	/* strongly_connected_components */
  31,	/* pretty_print_instance_variables */
  33,	/* each_strongly_connected_component */
  38,	/* each_strongly_connected_component_from */
  68,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/enum.rb */
  68,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/hash.rb */
  69,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/array.rb */
  69,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/range.rb */
  70,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/compar.rb */
  70,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/kernel.rb */
  70,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/string.rb */
  70,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/symbol.rb */
  71,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/00class.rb */
  71,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/10error.rb */
  71,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/numeric.rb */
  72,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/00kernel.rb */
  78,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-dir/mrblib/dir.rb */
  78,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-env/mrblib/env.rb */
  78,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-pp/mrblib/1_pp.rb */
  82,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-tsort/mrblib/tsort.rb */
  83,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-io/mrblib/io.rb */
  83,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/99_slop.rb */
  84,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-file-stat/mrblib/ext.rb */
  85,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-io/mrblib/file.rb */
  87,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-io/mrblib/kernel.rb */
  87,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-pp/mrblib/0_prettyprint.rb */
  89,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-print/mrblib/print.rb */
  89,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/04_slop_types.rb */
  89,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/05_slop_error.rb */
  90,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-file-stat/mrblib/file-stat.rb */
  90,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/00_slop_option.rb */
  90,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/02_slop_parser.rb */
  90,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/03_slop_result.rb */
  91,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-enum-ext/mrblib/enum.rb */
  91,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-hash-ext/mrblib/hash.rb */
  91,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-proc-ext/mrblib/proc.rb */
  91,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-struct/mrblib/struct.rb */
  91,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/01_slop_options.rb */
  92,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-enum-lazy/mrblib/lazy.rb */
  92,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-sprintf/mrblib/string.rb */
  93,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-array-ext/mrblib/array.rb */
  93,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-range-ext/mrblib/range.rb */
  94,	/* /home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-onig-regexp/mrblib/onig_regexp.rb */
  95,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-io/mrblib/file_constants.rb */
  95,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-object-ext/mrblib/object.rb */
  95,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-string-ext/mrblib/string.rb */
  95,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-symbol-ext/mrblib/symbol.rb */
  99,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-enumerator/mrblib/enumerator.rb */
  99,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-toplevel-ext/mrblib/toplevel.rb */
  101,	/* /home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-numeric-ext/mrblib/numeric_ext.rb */
};

static const char * const presym_name_table[] = {
  "!",
  "%",
  "&",
  "*",
  "+",
  "-",
  "/",
  "<",
  ">",
  "E",
  "^",
  "_",
  "`",
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "h",
  "i",
  "j",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "t",
  "v",
  "w",
  "x",
  "y",
  "z",
  "|",
  "~",
  "!=",
  "!~",
  "$&",
  "$'",
  "$+",
  "$0",
  "$1",
  "$;",
  "$>",
  "$?",
  "$`",
  "$~",
  "&&",
  "**",
  "+@",
  "-@",
  "<<",
  "<=",
  "==",
  "=~",
  ">=",
  ">>",
  "GC",
  "IO",
  "PI",
  "PP",
  "[]",
  "ar",
  "as",
  "at",
  "bi",
  "bs",
  "cp",
  "e2",
  "e3",
  "ed",
  "ei",
  "fd",
  "gm",
  "gs",
  "id",
  "in",
  "io",
  "lm",
  "lz",
  "my",
  "nc",
  "nv",
  "on",
  "pp",
  "re",
  "sv",
  "tr",
  "vs",
  "wd",
  "||",
  "<=>",
  "===",
  "@pp",
  "DIG",
  "Dir",
  "EIO",
  "ENV",
  "MAX",
  "MIN",
  "NAN",
  "[]=",
  "abs",
  "add",
  "arg",
  "ary",
  "beg",
  "blk",
  "chr",
  "cmd",
  "cmp",
  "cos",
  "day",
  "deq",
  "dev",
  "dig",
  "dir",
  "div",
  "dup",
  "end",
  "enq",
  "eof",
  "erf",
  "err",
  "exp",
  "fib",
  "get",
  "gid",
  "hex",
  "idx",
  "ino",
  "key",
  "kwd",
  "len",
  "lhs",
  "lim",
  "log",
  "low",
  "map",
  "max",
  "mid",
  "min",
  "mon",
  "msg",
  "new",
  "now",
  "nth",
  "num",
  "obj",
  "oct",
  "old",
  "opt",
  "ord",
  "out",
  "pat",
  "pid",
  "pop",
  "pos",
  "pow",
  "pwd",
  "quo",
  "req",
  "res",
  "ret",
  "rhs",
  "row",
  "sec",
  "sep",
  "set",
  "sin",
  "str",
  "sub",
  "sum",
  "sym",
  "tan",
  "tap",
  "tmp",
  "tr!",
  "uid",
  "utc",
  "val",
  "zip",
  "@dst",
  "@fib",
  "@kwd",
  "@obj",
  "ARGV",
  "EADV",
  "EDOM",
  "EXCL",
  "FREE",
  "File",
  "Hash",
  "Lazy",
  "Math",
  "NONE",
  "NULL",
  "Proc",
  "RDWR",
  "SYNC",
  "Slop",
  "Stat",
  "Text",
  "Time",
  "_dev",
  "acos",
  "all?",
  "any?",
  "arg0",
  "arg1",
  "arg2",
  "args",
  "argv",
  "arys",
  "asin",
  "atan",
  "attr",
  "bsiz",
  "byte",
  "call",
  "cbrt",
  "ceil",
  "char",
  "chop",
  "cosh",
  "curr",
  "data",
  "desc",
  "drop",
  "dst?",
  "dump",
  "each",
  "elem",
  "eof?",
  "epos",
  "eql?",
  "erfc",
  "fail",
  "feed",
  "file",
  "fill",
  "find",
  "flag",
  "getc",
  "gets",
  "gmt?",
  "grep",
  "gsub",
  "hash",
  "help",
  "high",
  "hour",
  "idx2",
  "init",
  "join",
  "key?",
  "keys",
  "lary",
  "last",
  "lazy",
  "left",
  "lidx",
  "line",
  "list",
  "log2",
  "loop",
  "lval",
  "map!",
  "mask",
  "mday",
  "mesg",
  "meth",
  "mode",
  "name",
  "nan?",
  "nest",
  "next",
  "nil?",
  "node",
  "none",
  "obj=",
  "objs",
  "one?",
  "open",
  "opts",
  "orig",
  "pad1",
  "pad2",
  "pair",
  "path",
  "peek",
  "perm",
  "pipe",
  "plen",
  "pos=",
  "proc",
  "push",
  "puts",
  "rand",
  "rdev",
  "read",
  "rest",
  "ridx",
  "rval",
  "save",
  "seek",
  "send",
  "sinh",
  "size",
  "sort",
  "sqrt",
  "stat",
  "step",
  "str2",
  "sub!",
  "succ",
  "sync",
  "tail",
  "take",
  "tanh",
  "tell",
  "text",
  "then",
  "to_a",
  "to_f",
  "to_h",
  "to_i",
  "to_s",
  "tr_s",
  "tty?",
  "type",
  "uniq",
  "upto",
  "usec",
  "user",
  "utc?",
  "vals",
  "wday",
  "yday",
  "year",
  "zone",
  "@args",
  "@desc",
  "@flag",
  "@memo",
  "@meth",
  "@name",
  "@objs",
  "@path",
  "@proc",
  "Array",
  "CREAT",
  "Class",
  "DSYNC",
  "E2BIG",
  "EAUTH",
  "EBADE",
  "EBADF",
  "EBADR",
  "EBUSY",
  "ECOMM",
  "EFBIG",
  "EIDRM",
  "EINTR",
  "ELOOP",
  "ENOSR",
  "ENXIO",
  "EPERM",
  "EPIPE",
  "EROFS",
  "ESRCH",
  "ETIME",
  "EXDEV",
  "Errno",
  "Error",
  "Fiber",
  "Float",
  "Group",
  "RADIX",
  "RSYNC",
  "Range",
  "STDIN",
  "TOTAL",
  "TRUNC",
  "TSort",
  "T_ENV",
  "_mode",
  "_pipe",
  "_rdev",
  "acosh",
  "args=",
  "arity",
  "array",
  "ary_F",
  "ary_T",
  "asinh",
  "assoc",
  "atan2",
  "atanh",
  "atime",
  "begin",
  "block",
  "break",
  "bytes",
  "cache",
  "chars",
  "chdir",
  "child",
  "chmod",
  "chomp",
  "chop!",
  "class",
  "clear",
  "clone",
  "close",
  "count",
  "ctime",
  "curry",
  "cycle",
  "depth",
  "enums",
  "errno",
  "fetch",
  "field",
  "file?",
  "first",
  "flags",
  "flock",
  "floor",
  "flush",
  "fname",
  "force",
  "found",
  "frexp",
  "ftype",
  "getgm",
  "getwd",
  "group",
  "gsub!",
  "help?",
  "hypot",
  "index",
  "is_a?",
  "items",
  "klass",
  "ldexp",
  "level",
  "limit",
  "lines",
  "ljust",
  "local",
  "log10",
  "lsize",
  "lstat",
  "match",
  "merge",
  "meth=",
  "mkdir",
  "month",
  "mtime",
  "names",
  "next!",
  "nlink",
  "none?",
  "null?",
  "other",
  "pairs",
  "parse",
  "pipe?",
  "popen",
  "pproc",
  "pread",
  "print",
  "quote",
  "raise",
  "reset",
  "right",
  "rjust",
  "rmdir",
  "round",
  "shift",
  "size?",
  "slice",
  "sort!",
  "split",
  "srand",
  "stack",
  "start",
  "state",
  "stats",
  "store",
  "strip",
  "succ!",
  "sync=",
  "tail?",
  "taken",
  "tally",
  "times",
  "total",
  "tr_s!",
  "tsort",
  "umask",
  "union",
  "uniq!",
  "value",
  "width",
  "write",
  "yield",
  "zero?",
  "$stdin",
  "@block",
  "@break",
  "@count",
  "@depth",
  "@first",
  "@flags",
  "@group",
  "@queue",
  "@value",
  "@width",
  "APPEND",
  "BINARY",
  "Cyclic",
  "DIRECT",
  "EACCES",
  "EAGAIN",
  "EBADFD",
  "EBFONT",
  "ECHILD",
  "ECHRNG",
  "EDQUOT",
  "EEXIST",
  "EFAULT",
  "EFTYPE",
  "EILSEQ",
  "EINVAL",
  "EIPSEC",
  "EISDIR",
  "EISNAM",
  "EL2HLT",
  "EL3HLT",
  "EL3RST",
  "ELNRNG",
  "EMFILE",
  "EMLINK",
  "ENFILE",
  "ENOANO",
  "ENOCSI",
  "ENODEV",
  "ENOENT",
  "ENOKEY",
  "ENOLCK",
  "ENOMEM",
  "ENOMSG",
  "ENONET",
  "ENOPKG",
  "ENOSPC",
  "ENOSTR",
  "ENOSYS",
  "ENOTTY",
  "EPROTO",
  "ERANGE",
  "ESPIPE",
  "ESRMNT",
  "ESTALE",
  "EUSERS",
  "EXFULL",
  "Fixnum",
  "Kernel",
  "Module",
  "NOCTTY",
  "NOTBOL",
  "NOTBOS",
  "NOTEOL",
  "NOTEOS",
  "Object",
  "Option",
  "Parser",
  "RDONLY",
  "Random",
  "Regexp",
  "Result",
  "STDERR",
  "STDOUT",
  "Status",
  "String",
  "Struct",
  "Symbol",
  "T_CPTR",
  "T_DATA",
  "T_HASH",
  "T_PROC",
  "WRONLY",
  "__id__",
  "_atime",
  "_chdir",
  "_ctime",
  "_getwd",
  "_mtime",
  "_popen",
  "_value",
  "alive?",
  "append",
  "banner",
  "blocks",
  "break?",
  "caller",
  "center",
  "chomp!",
  "chroot",
  "concat",
  "config",
  "cover?",
  "delete",
  "detect",
  "digits",
  "divmod",
  "downto",
  "empty?",
  "enable",
  "equal?",
  "escape",
  "except",
  "exist?",
  "extend",
  "fileno",
  "filter",
  "finish",
  "first?",
  "for_fd",
  "format",
  "freeze",
  "getutc",
  "gmtime",
  "groups",
  "id_map",
  "ifnone",
  "indent",
  "inject",
  "insert",
  "intern",
  "invert",
  "isatty",
  "itself",
  "lambda",
  "length",
  "longer",
  "lstrip",
  "match?",
  "max_by",
  "maxlen",
  "member",
  "merge!",
  "method",
  "min_by",
  "minmax",
  "mktime",
  "modulo",
  "n_args",
  "object",
  "offset",
  "option",
  "others",
  "output",
  "owned?",
  "padstr",
  "parser",
  "prefix",
  "printf",
  "public",
  "pwrite",
  "random",
  "rassoc",
  "reduce",
  "regexp",
  "rehash",
  "reject",
  "rename",
  "result",
  "resume",
  "rewind",
  "rindex",
  "rotate",
  "rstrip",
  "sample",
  "select",
  "slice!",
  "source",
  "string",
  "strip!",
  "to_int",
  "to_str",
  "to_sym",
  "ungetc",
  "unlink",
  "upcase",
  "update",
  "valid?",
  "value=",
  "value?",
  "values",
  "$stderr",
  "$stdout",
  "@banner",
  "@buffer",
  "@config",
  "@indent",
  "@output",
  "@parser",
  "@source",
  "Complex",
  "EBADMSG",
  "EBADRPC",
  "EBADRQC",
  "EBADSLT",
  "EDEADLK",
  "EDOOFUS",
  "EDOTDOT",
  "EISCONN",
  "ELIBACC",
  "ELIBBAD",
  "ELIBMAX",
  "ELIBSCN",
  "ENAVAIL",
  "ENOATTR",
  "ENOBUFS",
  "ENODATA",
  "ENOEXEC",
  "ENOLINK",
  "ENOTBLK",
  "ENOTDIR",
  "ENOTNAM",
  "ENOTSUP",
  "EPSILON",
  "EREMCHG",
  "EREMOTE",
  "ERFKILL",
  "ETXTBSY",
  "EUCLEAN",
  "EUNATCH",
  "IOError",
  "Integer",
  "LOCK_EX",
  "LOCK_NB",
  "LOCK_SH",
  "LOCK_UN",
  "MAX_EXP",
  "MIN_EXP",
  "NOATIME",
  "NOERROR",
  "Numeric",
  "Options",
  "Process",
  "TMPFILE",
  "T_ARRAY",
  "T_BREAK",
  "T_CLASS",
  "T_FIBER",
  "T_FLOAT",
  "T_RANGE",
  "VERSION",
  "Yielder",
  "__lines",
  "__merge",
  "_result",
  "asctime",
  "blksize",
  "bsearch",
  "casecmp",
  "ceildiv",
  "closed?",
  "collect",
  "command",
  "compact",
  "compile",
  "current",
  "default",
  "delete!",
  "dirname",
  "disable",
  "dropped",
  "entries",
  "exists?",
  "extname",
  "filter!",
  "finite?",
  "flatten",
  "foreach",
  "friday?",
  "frozen?",
  "getbyte",
  "include",
  "indexes",
  "inspect",
  "keep_if",
  "keyrest",
  "lambda?",
  "lstrip!",
  "max_cmp",
  "member?",
  "members",
  "message",
  "methods",
  "min_cmp",
  "modules",
  "monday?",
  "nesting",
  "new_key",
  "newline",
  "nobits?",
  "node_id",
  "options",
  "padding",
  "pattern",
  "pointer",
  "pp_hash",
  "prepend",
  "private",
  "process",
  "produce",
  "product",
  "reject!",
  "replace",
  "result=",
  "reverse",
  "rotate!",
  "rstrip!",
  "select!",
  "sep_len",
  "seplist",
  "setbyte",
  "setgid?",
  "setuid?",
  "shorter",
  "shuffle",
  "socket?",
  "sort_by",
  "sprintf",
  "squeeze",
  "sticky?",
  "strings",
  "sunday?",
  "symlink",
  "sysopen",
  "sysread",
  "sysseek",
  "to_enum",
  "to_hash",
  "to_path",
  "to_proc",
  "unshift",
  "upcase!",
  "version",
  "yielder",
  "@newline",
  "@options",
  "EALREADY",
  "EL2NSYNC",
  "ELIBEXEC",
  "EMSGSIZE",
  "ENETDOWN",
  "ENOTCONN",
  "ENOTSOCK",
  "ENOTUNIQ",
  "EOFError",
  "EPROCLIM",
  "ERESTART",
  "ESTRPIPE",
  "EXTENDED",
  "FileTest",
  "INFINITY",
  "KeyError",
  "MANT_DIG",
  "NOFOLLOW",
  "NONBLOCK",
  "NilClass",
  "Rational",
  "SEEK_CUR",
  "SEEK_END",
  "SEEK_SET",
  "T_BIGINT",
  "T_ICLASS",
  "T_MODULE",
  "T_OBJECT",
  "T_SCLASS",
  "T_STRING",
  "T_STRUCT",
  "__ary_eq",
  "__delete",
  "__send__",
  "__svalue",
  "__to_int",
  "_gethome",
  "allbits?",
  "allocate",
  "anybits?",
  "argument",
  "basename",
  "between?",
  "bytesize",
  "captures",
  "casecmp?",
  "chardev?",
  "child_id",
  "collect!",
  "compact!",
  "default=",
  "defined?",
  "downcase",
  "dropping",
  "each_key",
  "enum_for",
  "extended",
  "filename",
  "find_all",
  "flat_map",
  "flatten!",
  "genspace",
  "getlocal",
  "group_by",
  "has_key?",
  "home_dir",
  "include?",
  "included",
  "kind_of?",
  "maxwidth",
  "modified",
  "new_args",
  "nonzero?",
  "open_obj",
  "overlap?",
  "readbyte",
  "readchar",
  "readline",
  "readlink",
  "realpath",
  "required",
  "reverse!",
  "self_len",
  "shuffle!",
  "squeeze!",
  "str_each",
  "swapcase",
  "symlink?",
  "syswrite",
  "transfer",
  "truncate",
  "tuesday?",
  "@genspace",
  "@maxwidth",
  "@stop_exc",
  "Breakable",
  "Constants",
  "ECANCELED",
  "EDEADLOCK",
  "EHOSTDOWN",
  "EMULTIHOP",
  "ENEEDAUTH",
  "ENETRESET",
  "ENOMEDIUM",
  "ENOTEMPTY",
  "EOVERFLOW",
  "EREMOTEIO",
  "ESHUTDOWN",
  "ETIMEDOUT",
  "Exception",
  "Generator",
  "IntOption",
  "MULTILINE",
  "MatchData",
  "NameError",
  "PPMethods",
  "SEPARATOR",
  "T_COMPLEX",
  "T_INTEGER",
  "T_ISTRUCT",
  "TrueClass",
  "TypeError",
  "__ary_cmp",
  "__compact",
  "__outer__",
  "_gc_root_",
  "_sys_fail",
  "_sysclose",
  "ancestors",
  "arguments",
  "backtrace",
  "base_path",
  "birthtime",
  "blockdev?",
  "breakable",
  "byteindex",
  "byteslice",
  "casefold?",
  "clean_key",
  "close_obj",
  "component",
  "const_get",
  "const_set",
  "constants",
  "delete_at",
  "delete_if",
  "delimiter",
  "dev_major",
  "dev_minor",
  "downcase!",
  "each_byte",
  "each_char",
  "each_cons",
  "each_line",
  "each_node",
  "each_pair",
  "end_with?",
  "exception",
  "exclusive",
  "feedvalue",
  "group_sub",
  "grpowned?",
  "infinite?",
  "inherited",
  "iterator?",
  "localtime",
  "minmax_by",
  "negative?",
  "object_id",
  "old_index",
  "old_slice",
  "orig_flag",
  "partition",
  "positive?",
  "pp_object",
  "pre_match",
  "prepended",
  "protected",
  "readable?",
  "readlines",
  "remainder",
  "required?",
  "satisfied",
  "saturday?",
  "separator",
  "swapcase!",
  "thursday?",
  "transpose",
  "ungetbyte",
  "validated",
  "values_at",
  "writable?",
  "@arguments",
  "@feedvalue",
  "@lookahead",
  "BoolOption",
  "Comparable",
  "EADDRINUSE",
  "ECONNRESET",
  "EOPNOTSUPP",
  "EOWNERDEAD",
  "EPROTOTYPE",
  "Enumerable",
  "Enumerator",
  "FalseClass",
  "FiberError",
  "GroupQueue",
  "IGNORECASE",
  "IndexError",
  "MAX_10_EXP",
  "MIN_10_EXP",
  "NullOption",
  "OnigRegexp",
  "RangeError",
  "SINGLELINE",
  "SingleLine",
  "T_RATIONAL",
  "__callee__",
  "__case_eqq",
  "__method__",
  "__num_to_a",
  "add_option",
  "breakables",
  "byterindex",
  "bytesplice",
  "capitalize",
  "class_eval",
  "codepoints",
  "difference",
  "directory?",
  "drop_while",
  "each_child",
  "each_index",
  "each_slice",
  "each_value",
  "fd_or_path",
  "filter_map",
  "find_index",
  "given_args",
  "has_value?",
  "initialize",
  "intersect?",
  "last_match",
  "make_curry",
  "match_data",
  "minimum_id",
  "open_width",
  "parameters",
  "path_token",
  "post_match",
  "rdev_major",
  "rdev_minor",
  "root_group",
  "rpartition",
  "self_arity",
  "separators",
  "step_ratio",
  "string_sub",
  "superclass",
  "take_while",
  "tsort_each",
  "wednesday?",
  "with_index",
  "yield_self",
  "@breakables",
  "@inspecting",
  "@last_match",
  "@separators",
  "ASCII_RANGE",
  "ArrayOption",
  "BasicObject",
  "DomainError",
  "EINPROGRESS",
  "EKEYEXPIRED",
  "EKEYREVOKED",
  "EMEDIUMTYPE",
  "ENETUNREACH",
  "ENOPROTOOPT",
  "EWOULDBLOCK",
  "Errno2class",
  "FNM_SYSCASE",
  "FloatOption",
  "FrozenError",
  "ObjectMixin",
  "ObjectSpace",
  "PrettyPrint",
  "RUBY_ENGINE",
  "RegexpError",
  "ScriptError",
  "SyntaxError",
  "TRUE_VALUES",
  "T_EXCEPTION",
  "__ary_index",
  "__members__",
  "attr_reader",
  "attr_writer",
  "capitalize!",
  "cleaned_key",
  "close_width",
  "close_write",
  "combination",
  "default_dir",
  "each_object",
  "ensure_call",
  "executable?",
  "expand_path",
  "group_queue",
  "iter_method",
  "last_match=",
  "module_eval",
  "next_values",
  "owned_real?",
  "peek_values",
  "permutation",
  "respond_to?",
  "start_with?",
  "step_ratio=",
  "string_gsub",
  "string_scan",
  "try_process",
  "with_object",
  "@group_queue",
  "@group_stack",
  "EAFNOSUPPORT",
  "ECONNABORTED",
  "ECONNREFUSED",
  "EDESTADDRREQ",
  "EHOSTUNREACH",
  "EKEYREJECTED",
  "ENAMETOOLONG",
  "EPFNOSUPPORT",
  "EPROCUNAVAIL",
  "EPROGUNAVAIL",
  "ERPCMISMATCH",
  "ETOOMANYREFS",
  "FALSE_VALUES",
  "FIND_LONGEST",
  "FNM_CASEFOLD",
  "FNM_DOTMATCH",
  "FNM_NOESCAPE",
  "FNM_PATHNAME",
  "NEWLINE_CRLF",
  "RUBY_VERSION",
  "RegexpOption",
  "RuntimeError",
  "SHARE_DELETE",
  "StringOption",
  "SymbolOption",
  "VALID_VALUES",
  "__ENCODING__",
  "__attached__",
  "__codepoints",
  "_concat_path",
  "alias_method",
  "block_given?",
  "column_count",
  "column_index",
  "default_proc",
  "drive_prefix",
  "exclude_end?",
  "fetch_values",
  "force_false?",
  "ignored_args",
  "instance_of?",
  "intersection",
  "method_added",
  "mruby_Random",
  "object_group",
  "output_width",
  "pretty_flags",
  "pretty_print",
  "remove_const",
  "reverse_each",
  "stack_length",
  "string_split",
  "undef_method",
  "used_options",
  "@buffer_width",
  "@output_width",
  "ALT_SEPARATOR",
  "ArgumentError",
  "BooleanOption",
  "CAPTURE_GROUP",
  "EADDRNOTAVAIL",
  "EPROGMISMATCH",
  "IntegerOption",
  "MRUBY_VERSION",
  "NoMemoryError",
  "NoMethodError",
  "OnigMatchData",
  "StandardError",
  "StopIteration",
  "UnknownOption",
  "__classname__",
  "__sub_replace",
  "__update_hash",
  "attr_accessor",
  "bsearch_index",
  "const_missing",
  "count_objects",
  "current_group",
  "default_proc=",
  "default_value",
  "define_method",
  "delete_prefix",
  "delete_suffix",
  "expanded_path",
  "extend_object",
  "in_lower_half",
  "instance_eval",
  "instance_exec",
  "partition_idx",
  "remove_method",
  "set_backtrace",
  "singleline_pp",
  "splitted_path",
  "validate_type",
  "DEFAULT_CONFIG",
  "FIND_NOT_EMPTY",
  "LocalJumpError",
  "PATH_SEPARATOR",
  "StandardOutput",
  "__upto_endless",
  "close_on_exec=",
  "close_on_exec?",
  "collect_concat",
  "const_defined?",
  "delete_prefix!",
  "delete_suffix!",
  "each_codepoint",
  "explicit_value",
  "fill_breakable",
  "interval_ratio",
  "longest_option",
  "method_missing",
  "method_removed",
  "named_captures",
  "paragraph_mode",
  "pretty_inspect",
  "public_methods",
  "readable_real?",
  "sub_minimum_id",
  "transform_keys",
  "unused_options",
  "validate_type?",
  "validate_types",
  "writable_real?",
  "ENOTRECOVERABLE",
  "EPROTONOSUPPORT",
  "ESOCKTNOSUPPORT",
  "MRUBY_COPYRIGHT",
  "MissingArgument",
  "SystemCallError",
  "append_features",
  "class_variables",
  "comma_breakable",
  "each_with_index",
  "explicit_value=",
  "include_private",
  "initialize_copy",
  "interval_ratio=",
  "local_variables",
  "matching_option",
  "method_defined?",
  "module_function",
  "onig_regexp_sub",
  "option_defined?",
  "pad_repetitions",
  "pop_inspect_key",
  "private_methods",
  "singleton_class",
  "source_location",
  "suppress_errors",
  "transform_keys!",
  "tsort_each_node",
  "world_readable?",
  "world_writable?",
  "FloatDomainError",
  "MRUBY_RELEASE_NO",
  "SystemStackError",
  "each_with_object",
  "executable_real?",
  "global_variables",
  "included_modules",
  "instance_methods",
  "onig_regexp_gsub",
  "onig_regexp_scan",
  "prepend_features",
  "push_inspect_key",
  "string_to_option",
  "suppress_errors?",
  "transform_values",
  "tsort_each_child",
  "underscore_flags",
  "INT_STRING_REGEXP",
  "MRUBY_DESCRIPTION",
  "NEGATE_SINGLELINE",
  "ZeroDivisionError",
  "check_inspect_key",
  "expand_path_array",
  "expects_argument?",
  "generational_mode",
  "guard_inspect_key",
  "onig_regexp_split",
  "protected_methods",
  "sharing_detection",
  "singleline_format",
  "singleton_methods",
  "transform_values!",
  "underscore_flags?",
  "@sharing_detection",
  "DONT_CAPTURE_GROUP",
  "InvalidOptionValue",
  "MRUBY_RELEASE_DATE",
  "class_variable_get",
  "class_variable_set",
  "generational_mode=",
  "instance_variables",
  "onig_regexp_match?",
  "pretty_print_cycle",
  "FLOAT_STRING_REGEXP",
  "NotImplementedError",
  "RUBY_ENGINE_VERSION",
  "longest_flag_length",
  "old_square_brancket",
  "respond_to_missing?",
  "WORD_BOUND_ALL_RANGE",
  "__inspect_recursive?",
  "break_outmost_groups",
  "object_address_group",
  "pretty_print_inspect",
  "repeated_combination",
  "repeated_permutation",
  "@set_global_variables",
  "MissingRequiredOption",
  "__coerce_step_counter",
  "enumerator_block_call",
  "instance_variable_get",
  "instance_variable_set",
  "remove_class_variable",
  "set_global_variables=",
  "set_global_variables?",
  "__repeated_combination",
  "clear_global_variables",
  "consume_next_argument?",
  "singleton_method_added",
  "string_to_option_class",
  "POSIX_BRACKET_ALL_RANGE",
  "class_variable_defined?",
  "define_singleton_method",
  "try_process_smashed_arg",
  "remove_instance_variable",
  "old_square_brancket_equal",
  "try_process_grouped_flags",
  "instance_variable_defined?",
  "undefined_instance_methods",
  "strongly_connected_components",
  "pretty_print_instance_variables",
  "each_strongly_connected_component",
  "each_strongly_connected_component_from",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/enum.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/hash.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/array.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/range.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/compar.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/kernel.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/string.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/symbol.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/00class.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/10error.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/numeric.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrblib/00kernel.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-dir/mrblib/dir.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-env/mrblib/env.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-pp/mrblib/1_pp.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-tsort/mrblib/tsort.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-io/mrblib/io.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/99_slop.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-file-stat/mrblib/ext.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-io/mrblib/file.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-io/mrblib/kernel.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-pp/mrblib/0_prettyprint.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-print/mrblib/print.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/04_slop_types.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/05_slop_error.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-file-stat/mrblib/file-stat.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/00_slop_option.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/02_slop_parser.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/03_slop_result.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-enum-ext/mrblib/enum.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-hash-ext/mrblib/hash.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-proc-ext/mrblib/proc.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-struct/mrblib/struct.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-slop/mrblib/01_slop_options.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-enum-lazy/mrblib/lazy.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-sprintf/mrblib/string.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-array-ext/mrblib/array.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-range-ext/mrblib/range.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby/mrbgems/mruby-onig-regexp/mrblib/onig_regexp.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-io/mrblib/file_constants.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-object-ext/mrblib/object.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-string-ext/mrblib/string.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-symbol-ext/mrblib/symbol.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-enumerator/mrblib/enumerator.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-toplevel-ext/mrblib/toplevel.rb",
  "/home/runner/work/groonga/groonga/vendor/mruby-source/mrbgems/mruby-numeric-ext/mrblib/numeric_ext.rb",
};
